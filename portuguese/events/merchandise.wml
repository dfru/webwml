#use wml::debian::template title="Merchandise do Debian" GEN_TIME="yes" BARETITLE="true"
#use wml::debian::translation-check translation="3c06310c5d261bd2102e562e1b811d587413a5e4"

# TRANSLATORS: We moved the original english file from /misc/awards to the /News folder, so we lost the git history.
# This translation-check header is set to an arbitrary commit just to allow this file to be built.
# Please compare manually with the english version to update your translation (which is older than the commit referenced).

<p>
A medida que cresce a popularidade do Debian, temos recebido muitos pedidos
de <q>merchandise</q>. Sendo uma organização sem fins lucrativos, não produzimos
e nem vendemos nada. Felizmente, várias empresas viram um mercado potencial e estão
tentando preenchê-lo. As informações abaixo são fornecidas como um favor para
nossos(as) usuários(as) fiéis. A ordem é puramente alfabética e não há
classificação ou endosso de um(a) fornecedor(a) específico(a).
</p>

<p>
Alguns(mas) fornecedores(as) contribuem com uma parte das vendas com merchandise
do Debian para o Debian. Isso é indicado no tópico de cada fornecedor(a).
Esperamos que os(as) fornecedores(as) considerem fazer doações para o Debian.
</p>

<p>
Além disso, alguns(mas) colaboradores(as) do Debian ou grupos locais tem
produzido mercadorias com a marca Debian e podem ter algum estoque disponível,
informações sobre isso estão disponíveis na
<a href="https://wiki.debian.org/Merchandise">wiki</a>.
</p>

#include "$(ENGLISHDIR)/events/merchandise.data"

<p>Se você vende <q>merchandise</q> com um tema Debian e gostaria de ser listado
nesta página, envie um e-mail <strong>em inglês</strong> para
&lt;<a href="mailto:events@debian.org">events@debian.org</a>&gt;.
<br>
Por favor, forneça as seguintes informações:
<ul>
  <li>o nome da empresa,
  <li>a URL para a sua página principal,
  <li>a URL onde o <q>merchandise</q> Debian pode ser encontrado,
  <li>a lista de <q>merchandise</q> Debian vendida,
  <li>a lista de idioma disponíveis no site,
  <li>o país de origem,
  <li>informações sobre a entrega (internacional ou não).
</ul>
Só estamos interessados em divulgar material relacionado ao Debian.

<p>Como nós protegemos nossa reputação, levamos o serviço ao consumidor a sério.
Se recebermos reclamações sobre seus serviços, você será removido desta página.
</p>
