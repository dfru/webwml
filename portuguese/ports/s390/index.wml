#use wml::debian::template title="Porte S/390"
#use wml::debian::toc
#use wml::debian::translation-check translation="0d2ff40f3ac99634f3bb83bb7589af752e6783c5"

<toc-display/>

<toc-add-entry name="status">Situação atual</toc-add-entry>

<p>O S/390 é uma arquitetura do Debian oficialmente suportada desde o
lançamento do Debian 3.0 (woody).</p>

<p>Para instruções de como instalar o Debian, veja o
<a href="$(HOME)/releases/stable/s390x/">guia de instalação</a>.</p>

<toc-add-entry name="team">Equipe de portabilidade S/390</toc-add-entry>

<p>
As seguintes pessoas contribuíram para o porte s390 do Debian:
</p>

<ul>
  <li>Aurélien Jarno</li>
  <li>Bastian Blank</li>
  <li>Chu-yeon Park and Jae-hwa Park</li>
  <li>Frank Kirschner</li>
  <li>Frans Pop</li>
  <li>Gerhard Tonn</li>
  <li>Jochen Röhrig</li>
  <li>Matt Zimmerman</li>
  <li>Philipp Kern</li>
  <li>Richard Higson</li>
  <li>Stefan Gybas</li>
</ul>

<toc-add-entry name="development">Desenvolvimento</toc-add-entry>

<p>Um servidor de construção e a máquina de porte para a arquitetura
<em>s390x</em> estão atualmente hospedados em
<a href="https://www.itzbund.de/">Informationstechnikzentrum Bund (ITZBund)</a>.
Outros servidores de construção são fornecidos pelo
<a href="http://www.iic.kit.edu">Informatics Innovation Center,
Karlsruhe Institute of Technology (KIT)</a> e
<a href="https://www.marist.edu/">Marist College</a>. Nós agradecemos
aos(às) apoiadores(as) por seu suporte.</p>

<p>No passado a <a href="http://www.millenux.de/">Millenux</a> e a <a
href="https://www.ibm.com/it-infrastructure/z/os/linux-support">Comunidade
de Desenvolvimento do Sistema Linux</a> hospedavam as máquinas de
construção.</p>

<toc-add-entry name="contact">Informações de contato</toc-add-entry>

<p>Se você gostaria de ajudar, inscreva-se na lista de discussão debian-s390.
Para isso, envie uma mensagem contendo a palavra "subscribe"
no campo assunto para <email "debian-s390-request@lists.debian.org">, ou ue a
<a href="https://lists.debian.org/debian-s390/">página web da lista de discussão</a>.
Você também pode navegar e pesquisar nos
<a href="https://lists.debian.org/debian-s390/">arquivos da lista de discussão</a>.</p>
