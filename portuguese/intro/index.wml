#use wml::debian::template title="Introdução ao Debian" MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="community">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="community"></a>
      <h2>O Debian é uma comunidade</h2>
      <p>Milhares de voluntários(as) ao redor do mundo trabalham juntos(as) no
sistema operacional Debian, priorizando o Software Livre e de Código Aberto.
Conheça o projeto Debian.</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="people">As pessoas:</a>
          Quem somos e o que fazemos
        </li>
        <li>
          <a href="philosophy">Nossa filosofia:</a>
          Por que fazemos e como fazemos
        </li>
        <li>
          <a href="../devel/join/">Envolva-se:</a>
          Como se tornar um(a) contribuidor(a) Debian
        </li>
        <li>
          <a href="help">Contribuir:</a>
          Como você pode ajudar o Debian
        </li>
        <li>
          <a href="../social_contract">Contrato social Debian:</a>
          Nosso compromisso ético
        </li>
        <li>
          <a href="diversity">Todos(as) são bem-vindos(as)</a>
          Declaração de diversidade
        </li>
        <li>
          <a href="../code_of_conduct">Para participantes</a>
          Código de conduta Debian
        </li>
        <li>
          <a href="../partners/">Parceiros(as):</a>
          Empresas e organizações que auxiliam o projeto Debian
        </li>
        <li>
          <a href="../donations">Doações</a>
          Como patrocinar o projeto Debian
        </li>
        <li>
          <a href="../legal/">Questões legais</a>
          Licenças, marcas registradas, política de privacidade, política de
          patentes, etc.
        </li>
        <li>
          <a href="../contact">Contato</a>
          Como entrar em contato conosco
        </li>
      </ul>
    </div>

  </div>

<hr>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-desktop fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <a id="software"></a>
      <h2>O Debian é um sistema operacional livre</h2>
      <p>O Debian é um sistema operacional livre, desenvolvido e mantido pelo
      projeto Debian. Uma distribuição Linux livre com milhares de aplicativos
      para atender às necessidades de nossos(as) usuários(as).</p>
    </div>

    <div style="text-align: left">
      <ul>
        <li>
          <a href="../distrib">Download:</a>
          Onde obter o Debian
        </li>
        <li>
          <a href="why_debian">Por que Debian</a>
          Razões para escolher o Debian
        </li>
        <li>
          <a href="../support">Suporte:</a>
          Onde encontrar ajuda
        </li>
        <li>
          <a href="../security">Segurança:</a>
          Última atualização<br>
          <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
              @MYLIST = split(/\n/, $MYLIST);
              $MYLIST[0] =~ s#security#../security#;
              print $MYLIST[0]; }:>
        </li>
        <li>
          <a href="../distrib/packages">Software:</a>
          Pesquise e navegue pela longa lista dos pacotes Debian
        </li>
        <li>
          <a href="../doc"> Documentação</a>
          Guia de instalação, FAQ, HOWTOs, Wiki, e mais
        </li>
        <li>
          <a href="../bugs">Sistema de rastreamenteo de bugs (BTS - Bug Tracking System)</a>
          Como relatar um bug, documentação do BTS
        </li>
        <li>
          <a href="https://lists.debian.org/">Listas de discussão:</a>
          Coleção de listas do Debian para usuários(as), desenvolvedores(as), etc.
          <a href="https://wiki.debian.org/Brasil/Listas">Listas em português</a>
        </li>
        <li>
          <a href="../blends">Pure Blends:</a>
          Metapacotes para necessidades específicas
        </li>
        <li>
          <a href="../devel"> Canto dos(as) desenvolvedores(as):</a>
          Informações principalmente de interesse de desenvolvedores(as) Debian
        </li>
        <li>
          <a href="../ports"> Portes/Arquiteturas:</a>
          Suporte Debian para várias arquiteturas de CPU
        </li>
        <li>
          <a href="search">Pesquisa:</a>
          Informações como sobre como usar o mecanismo de busca Debian
        </li>
        <li>
          <a href="cn">Idiomar:</a>
          Configuração de idioma para o site web do Debian
        </li>
      </ul>
    </div>
  </div>

</div>

