#use wml::debian::translation-check translation="f878955e279294523a604a4e9bbd3d93dd0f3cc7" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 11: выпуск 11.6</define-tag>
<define-tag release_date>2022-12-17</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о шестом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction awstats "Исправление межсайтового скриптинга [CVE-2022-46391]">
<correction base-files "Обновление /etc/debian_version для редакции 11.6">
<correction binfmt-support "Запуск binfmt-support.service после systemd-binfmt.service">
<correction clickhouse "Исправление чтения за пределами выделенного буфера памяти [CVE-2021-42387 CVE-2021-42388], переполнений буфера [CVE-2021-43304 CVE-2021-43305]">
<correction containerd "Дополнение CRI: исправление утечки goroutine во время Exec [CVE-2022-23471]">
<correction core-async-clojure "Исправление ошибок сборок в тестовом наборе">
<correction dcfldd "Исправление вывода SHA1 на архитектурах с порядком байтов от старшего к младшему">
<correction debian-installer "Повторная сборка с учётом proposed-updates; увеличение версии ABI ядра Linux до 5.10.0-20">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction debmirror "Добавление non-free-firmware в список разделов по умолчанию">
<correction distro-info-data "Добавление Ubuntu 23.04, Lunar Lobster; обновление дат прекращения сопровождения Debian ELTS; исправление даты выпуска Debian 8 (jessie)">
<correction dojo "Исправление загрязнения прототипа [CVE-2021-23450]">
<correction dovecot-fts-xapian "Порождение зависимости от dovecot с используемой версией ABI во время сборки">
<correction efitools "Исправление периодической ошибки сборки из-за неправильной зависимости в Makefile">
<correction evolution "Перемешение адресных книг Google Contacts в CalDAV, так как API Google Contacts был отключён">
<correction evolution-data-server "Перемешение адресных книг Google Contacts в CalDAV, так как API Google Contacts был отключён; исправление совместимости с Gmail OAuth">
<correction evolution-ews "Исправление получения пользовательских сертификатов, принадлежащих контактам">
<correction g810-led "Управление доступом к устройству с помощью uaccess вместо того, чтобы открывать всё с правами для записи для всех пользователей [CVE-2022-46338]">
<correction glibc "Исправление регрессии в wmemchr и wcslen на ЦП, имеющих AVX2, но не имеющих BMI2 (напр., Intel Haswell)">
<correction golang-github-go-chef-chef "Исправление периодической ошибки тестирования">
<correction grub-efi-amd64-signed "Не удалять двоичные файлы Xen, теперь они снова работают; включать шрифты в сборку memdisk для образов EFI; исправление ошибки главного кода файла, чтобы ошибки обрабатывались лучше; увеличение уровня Debian SBAT до 4">
<correction grub-efi-arm64-signed "Не удалять двоичные файлы Xen, теперь они снова работают; включать шрифты в сборку memdisk для образов EFI; исправление ошибки главного кода файла, чтобы ошибки обрабатывались лучше; увеличение уровня Debian SBAT до 4">
<correction grub-efi-ia32-signed "Не удалять двоичные файлы Xen, теперь они снова работают; включать шрифты в сборку memdisk для образов EFI; исправление ошибки главного кода файла, чтобы ошибки обрабатывались лучше; увеличение уровня Debian SBAT до 4">
<correction grub2 "Не удалять двоичные файлы Xen, теперь они снова работают; включать шрифты в сборку memdisk для образов EFI; исправление ошибки главного кода файла, чтобы ошибки обрабатывались лучше; увеличение уровня Debian SBAT до 4">
<correction hydrapaper "Добавление отсутствующей зависимости от python3-pil">
<correction isoquery "Исправление ошибки тестирования, вызванной изменением в переводе на французский язык в пакете iso-codes">
<correction jtreg6 "Новый пакет, требуется для сборки новых версий openjdk-11">
<correction lemonldap-ng "Улучшение распространения завершения сессии [CVE-2022-37186]">
<correction leptonlib "Исправление деления на нуль [CVE-2022-38266]">
<correction libapache2-mod-auth-mellon "Исправление открытого перенаправления [CVE-2021-3639]">
<correction libbluray "Исправление поддержки BD-J в новых обновлениях Oracle Java">
<correction libconfuse "Исправление чтения за пределами выделенного буфера динамической памяти в cfg_tilde_expand [CVE-2022-40320]">
<correction libdatetime-timezone-perl "Обновление поставляемых данных">
<correction libtasn1-6 "Исправление чтения за пределами выделенного буфера памяти [CVE-2021-46848]">
<correction libvncserver "Исправление утечки памяти [CVE-2020-29260]; поддержка экранов с большими размерами">
<correction linux "Новый стабильный выпуск основной ветки разработки; увеличение версии ABI до 20; [rt] обновление до версии 5.10.158-rt77">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки; увеличение версии ABI до 20; [rt] обновление до версии 5.10.158-rt77">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки; увеличение версии ABI до 20; [rt] обновление до версии 5.10.158-rt77">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки; увеличение версии ABI до 20; [rt] обновление до версии 5.10.158-rt77">
<correction mariadb-10.5 "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2018-25032 CVE-2021-46669 CVE-2022-27376 CVE-2022-27377 CVE-2022-27378 CVE-2022-27379 CVE-2022-27380 CVE-2022-27381 CVE-2022-27382 CVE-2022-27383 CVE-2022-27384 CVE-2022-27386 CVE-2022-27387 CVE-2022-27444 CVE-2022-27445 CVE-2022-27446 CVE-2022-27447 CVE-2022-27448 CVE-2022-27449 CVE-2022-27451 CVE-2022-27452 CVE-2022-27455 CVE-2022-27456 CVE-2022-27457 CVE-2022-27458 CVE-2022-32081 CVE-2022-32082 CVE-2022-32083 CVE-2022-32084 CVE-2022-32085 CVE-2022-32086 CVE-2022-32087 CVE-2022-32088 CVE-2022-32089 CVE-2022-32091]">
<correction mod-wsgi "Сброс заголовка X-Client-IP, когда он не является доверенным заголовком [CVE-2022-2255]">
<correction mplayer "Исправление нескольких проблем безопасности [CVE-2022-38850 CVE-2022-38851 CVE-2022-38855 CVE-2022-38858 CVE-2022-38860 CVE-2022-38861 CVE-2022-38863 CVE-2022-38864 CVE-2022-38865 CVE-2022-38866]">
<correction mutt "Исправление аварийной остановки gpgme при выведении списка ключей в блоке открытых ключей, а также при выведении списка блока открытых ключей для старых версий gpgme">
<correction nano "Исправление аварийных остановок и потенциальной потери данных">
<correction nftables "Исправление ошибки на единицу / двойного освобождения памяти">
<correction node-hawk "Грамматической разбор URL с помощью stdlib [CVE-2022-29167]">
<correction node-loader-utils "Исправление загрязнения прототипа [CVE-2022-37599 CVE-2022-37601], отказа в обслуживании из-за ошибки регулярного выражения [CVE-2022-37603]">
<correction node-minimatch "Улучшении защиты от отказа в обслуживании из-за ошибки регулярного выражения [CVE-2022-3517]; исправление регрессии в заплате для CVE-2022-3517">
<correction node-qs "Исправление загрязнения прототипа [CVE-2022-24999]">
<correction node-xmldom "Исправление загрязнения прототипа [CVE-2022-37616]; предотвращение вставки неправильно сформированных нод [CVE-2022-39353]">
<correction nvidia-graphics-drivers "Новый выпуск основной ветки разработки; исправление безопасности [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-legacy-390xx "Новый выпуск основной ветки разработки; исправление безопасности [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34680 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259]">
<correction nvidia-graphics-drivers-tesla-450 "Новый выпуск основной ветки разработки; исправление безопасности [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-tesla-470 "Новый выпуск основной ветки разработки; исправление безопасности [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction omnievents "Добавление отсутствующей зависимости от libjs-jquery к пакету omnievents-doc">
<correction onionshare "Исправление отказа в обслуживании [CVE-2022-21689], HTML-инъекции [CVE-2022-21690]">
<correction openvpn-auth-radius "Поддержка директивы verify-client-cert">
<correction postfix "Новый стабильный выпуск основной ветки разработки">
<correction postgresql-13 "Новый стабильный выпуск основной ветки разработки">
<correction powerline-gitstatus "Исправление введения команд через вредоносные настройки репозитория [CVE-2022-42906]">
<correction pysubnettree "Исправление сборки модуля">
<correction speech-dispatcher "Уменьшение размера буфера espeak с целью избежать появления артефактов при синтезе речи">
<correction spf-engine "Исправление ошибки запуска pyspf-milter из-за неправильного утверждения import">
<correction tinyexr "Исправление переполнения динамической памяти [CVE-2022-34300 CVE-2022-38529]">
<correction tinyxml "Исправление бесконечного цикла [CVE-2021-42260]">
<correction tzdata "Обновление данных для Фиджи, Мексики и Палестины; обновление списка корректировочных секунд">
<correction virglrenderer "Исправление записи за пределами выделенного буфера памяти issue [CVE-2022-0135]">
<correction x2gothinclient "Пакет x2gothinclient-minidesktop предоставляет виртуальный пакет lightdm-greeter">
<correction xfig "Исправление переполнения буфера [CVE-2021-40241]">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2022 5212 chromium>
<dsa 2022 5223 chromium>
<dsa 2022 5224 poppler>
<dsa 2022 5225 chromium>
<dsa 2022 5226 pcs>
<dsa 2022 5227 libgoogle-gson-java>
<dsa 2022 5228 gdk-pixbuf>
<dsa 2022 5229 freecad>
<dsa 2022 5230 chromium>
<dsa 2022 5231 connman>
<dsa 2022 5232 tinygltf>
<dsa 2022 5233 e17>
<dsa 2022 5234 fish>
<dsa 2022 5235 bind9>
<dsa 2022 5236 expat>
<dsa 2022 5239 gdal>
<dsa 2022 5240 webkit2gtk>
<dsa 2022 5241 wpewebkit>
<dsa 2022 5242 maven-shared-utils>
<dsa 2022 5243 lighttpd>
<dsa 2022 5244 chromium>
<dsa 2022 5245 chromium>
<dsa 2022 5246 mediawiki>
<dsa 2022 5247 barbican>
<dsa 2022 5248 php-twig>
<dsa 2022 5249 strongswan>
<dsa 2022 5250 dbus>
<dsa 2022 5251 isc-dhcp>
<dsa 2022 5252 libreoffice>
<dsa 2022 5253 chromium>
<dsa 2022 5254 python-django>
<dsa 2022 5255 libksba>
<dsa 2022 5256 bcel>
<dsa 2022 5257 linux-signed-arm64>
<dsa 2022 5257 linux-signed-amd64>
<dsa 2022 5257 linux-signed-i386>
<dsa 2022 5257 linux>
<dsa 2022 5258 squid>
<dsa 2022 5260 lava>
<dsa 2022 5261 chromium>
<dsa 2022 5263 chromium>
<dsa 2022 5264 batik>
<dsa 2022 5265 tomcat9>
<dsa 2022 5266 expat>
<dsa 2022 5267 pysha3>
<dsa 2022 5268 ffmpeg>
<dsa 2022 5269 pypy3>
<dsa 2022 5270 ntfs-3g>
<dsa 2022 5271 libxml2>
<dsa 2022 5272 xen>
<dsa 2022 5273 webkit2gtk>
<dsa 2022 5274 wpewebkit>
<dsa 2022 5275 chromium>
<dsa 2022 5276 pixman>
<dsa 2022 5277 php7.4>
<dsa 2022 5278 xorg-server>
<dsa 2022 5279 wordpress>
<dsa 2022 5280 grub-efi-amd64-signed>
<dsa 2022 5280 grub-efi-arm64-signed>
<dsa 2022 5280 grub-efi-ia32-signed>
<dsa 2022 5280 grub2>
<dsa 2022 5281 nginx>
<dsa 2022 5283 jackson-databind>
<dsa 2022 5285 asterisk>
<dsa 2022 5286 krb5>
<dsa 2022 5287 heimdal>
<dsa 2022 5288 graphicsmagick>
<dsa 2022 5289 chromium>
<dsa 2022 5290 commons-configuration2>
<dsa 2022 5291 mujs>
<dsa 2022 5292 snapd>
<dsa 2022 5293 chromium>
<dsa 2022 5294 jhead>
<dsa 2022 5295 chromium>
<dsa 2022 5296 xfce4-settings>
<dsa 2022 5297 vlc>
<dsa 2022 5298 cacti>
<dsa 2022 5299 openexr>
</table>



<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
