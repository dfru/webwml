#use wml::debian::template title="Debians norske hjørne"
#use wml::debian::translation-check translation="7ff1e58fbad65b9683e41dcd4c9d35fe7c22ecef" maintainer="Hans F. Nordhaug"
# Oppdatert av Hans F. Nordhaug <hansfn@gmail.com>, 2008-2021.

  <p>
    På denne siden vil du finne informasjon for norske brukere av
    Debian.  Om du syns noe spesielt hører til her, skriv gjerne til
    en av de norske <a href="#translators">oversetterne</a>.</p>

  <h2>E-postlister</h2>

  <p>
    Debian har for tiden ingen offisielle e-postlister på norsk.  Dersom
    det er interesse for det, kan vi starte en eller flere, for
    brukere og/eller utviklere. </p>

  <h2>Pekere</h2>

  <p>
    Noen pekere som vil være av interesse for norske Debian-brukere:
  </p>

  <ul>
    <!-- 
      Foreningen Linux Norge ser ikke ut til å eksistere lenger.
      Domenet linux.no fungerer, men er totalt utdatert.
    -->
    <!-- li><a href="http://www.linux.no/">Linux Norge</a><br>
	<em>"En frivillig organisasjon som skal spre informasjon om
	operativsystemet Linux, i Norge."</em>
    </li -->

    <li><a href="https://nuug.no/">Norwegian UNIX User Group</a>. Sjekk spesielt deres Wiki-side med 
        <a href="https://wiki.nuug.no/likesinnede-oversikt">oversikt over likesinnede foreninger</a>
        som lister mange/alle aktive Linux-brukergrupper.
    </li>

  </ul>

  <h2>Norske bidragsytere til Debian-prosjektet</h2>

  <h3>Nåværende aktive norske Debian-utviklere:</h3>

  <ul>
    <li>Lars Bahner &lt;<email bahner@debian.org>&gt;</li>
    <li>Morten Werner Forsbring &lt;<email werner@debian.org>&gt;</li>
    <li>Ove Kåven &lt;<email ovek@debian.org>&gt;</li>
    <li>Petter Reinholdtsen &lt;<email pere@debian.org>&gt;</li>
    <li>Ruben Undheim &lt;<email rubund@debian.org>&gt;</li>
    <li>Steinar H. Gunderson &lt;<email sesse@debian.org>&gt;</li>
    <li>Stein Magnus Jodal &lt;<email jodal@debian.org>&gt;</li>
    <li>Stig Sandbeck Mathisen &lt;<email ssm@debian.org>&gt;</li>
    <li>Tollef Fog Heen &lt;<email tfheen@debian.org>&gt;</li>
    <li>Øystein Gisnås &lt;<email shaka@debian.org>&gt;</li>
  </ul>

  <p>Denne listen oppdateres uregelmessig så du vil kanskje også sjekke
    <a href="https://db.debian.org/">Debian Developers Database</a> - velg
    &quot;Norway&quot; som &quot;country&quot;.</p>

  <h3>Tidligere norske Debian-utviklere:</h3>

  <ul>
    <li>Morten Hustvei</li>
    <li>Ole J. Tetlie</li>
    <li>Peter Krefting</li>
    <li>Tom Cato Amundsen</li>
    <li>Tore Anderson</li>
    <li>Tor Slettnes</li>
  </ul>

  <a id="translators"></a>
  <h3>Oversettere:</h3>
 
  <p>Debians nettsider oversettes for øyeblikket til norsk av:</p>

  <ul>
    <li>Hans F. Nordhaug &lt;<email hansfn@gmail.com>&gt;</li>
  </ul>

  <p>
    <!-- Debian mangler per idag noen som kan oversette nettsidene til norsk.  -->
    Hvis du er interessert bør du begynne med å lese
    <a href="$(DEVEL)/website/translating">informasjonen for oversettere</a>.
  </p>

  <p>Debians nettsider ble tidligere oversatt av:</p>

  <ul>
    <li>Cato Auestad</li>
    <li>Stig Sandbeck Mathisen</li>
    <li>Tollef Fog Heen</li>
    <li>Tor Slettnes</li>
  </ul>

  <p>
    Om du vil hjelpe til, eller om du vet om flere som på en eller
    annen måte er involvert i Debian-prosjektet, ta kontakt med en av
    oss.</p>

# Local variables:
# mode: sgml
# sgml-indent-data:t
# sgml-doctype:"../.doctype"
# End:
