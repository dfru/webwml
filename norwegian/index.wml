#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"
#use wml::debian::translation-check translation="fe2d0a6290a91814fb8d336efc57e158b56b319e" maintainer="Hans F. Nordhaug"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div id="splash">
  <h1>Debian</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>Fellesskapet</h1>
      <h2>Debian er et fellesskap av folk!</h2>

#include "$(ENGLISHDIR)/index.inc"

    <div class="row">
      <div class="community column">
        <a href="intro/people" aria-hidden="true">
          <img src="Pics/users.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/people">Folk</a></h2>
        <p>Hvem vi er og hva vi gjør</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/philosophy" aria-hidden="true">
          <img src="Pics/heartbeat.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/philosophy">Vår filosofi</a></h2>
        <p>Hvorfor vi gjør det, og hvordan vi gjør det</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/" aria-hidden="true">
          <img src="Pics/user-plus.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">Bli involvert, bidra</a></h2>
        <p>Hvordan du kan bli med!</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#community" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#community">Mer ...</a></h2>
        <p>Ytterligere informasjon om Debian-fellesskapet</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>Operativsystemet</h1>
      <h2>Debian er et fullstendig fritt operativsystem!</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
        <a href="$(HOME)/download" class="os-dl-btn"><download></a>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">Hvorfor Debian</a></h2>
        <p>Det som gjør Debian annerledes</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">Brukerstøtte</a></h2>
        <p>Få hjelp og dokumentasjon</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">Sikkerhetsoppdateringer</a></h2>
        <p>Debians sikkerhetsmeldinger (DSA)</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#software" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#software">Mer ...</a></h2>
        <p>Flere lenker til nedlastinger og programvare</p>
      </div>
    </div>
  </div>
</div>

<hr>
# <!-- An optional row highlighting events happening now, such as releases, point releases, debconf and minidebconfs, and elections (dpl, GRs...). -->
# <div class="row">
#  <div class="column styled-href-blue column-left">
#    <div style="text-align: center">
#      <h2><a href="https://debconf21.debconf.org/">DebConf21</a> er på vei!</h2>
#      <p>Debian-konferansen holdes på nett 24.-28. august, 2021.</p>
#    </div>
#  </div>
# </div>


<!-- The next row of columns on the site. -->
<!-- The News will be selected by the press team. -->

<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>Nyheter og kunngjøringer om Debian</h2>
    </div>

    <:= get_top_news() :>

    <!-- No more News entries behind this line! -->
    <div class="project-news">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">Alle nyhetene</a> &emsp;&emsp;
	<a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Nyheter om Debian" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Nyheter om Debian-prosjektet" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Debians sikkerhetsmeldinger (kun titler)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debians sikkerhetsmeldinger (sammendrag)" href="security/dsa-long">
:#rss#}

