msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-03-13 16:15+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "Nėra reikalavimų įsisūnijimui "

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "Nėra likę paketų našlaičių"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "Nėra paketų laukiančių būti įsisūnytais"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "Nėra paketų laukiančiųjų supakavimo"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "Nėra reikalautų paketų"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "Pagalba nėra prašoma"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "įsūnytas nuo šiandien."

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "įsūnytas nuo vakar"

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "%s dienų yra įsūnytas "

#: ../../english/template/debian/wnpp.wml:46
#, fuzzy
msgid "%s days in adoption, last activity today."
msgstr "%s dienų yra įsūnytas "

#: ../../english/template/debian/wnpp.wml:50
#, fuzzy
msgid "%s days in adoption, last activity yesterday."
msgstr "įsūnytas nuo vakar"

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr ""

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "ruošiami nuo šiandien."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "ruošiami nuo vakar."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "ruošiami %s dienų."

#: ../../english/template/debian/wnpp.wml:72
#, fuzzy
msgid "%s days in preparation, last activity today."
msgstr "ruošiami %s dienų."

#: ../../english/template/debian/wnpp.wml:76
#, fuzzy
msgid "%s days in preparation, last activity yesterday."
msgstr "ruošiami nuo vakar."

#: ../../english/template/debian/wnpp.wml:80
#, fuzzy
msgid "%s days in preparation, last activity %s days ago."
msgstr "ruošiami %s dienų."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
#, fuzzy
msgid "adoption requested since today."
msgstr "įsūnytas nuo šiandien."

#: ../../english/template/debian/wnpp.wml:89
#, fuzzy
msgid "adoption requested since yesterday."
msgstr "įsūnytas nuo vakar"

#: ../../english/template/debian/wnpp.wml:93
#, fuzzy
msgid "adoption requested since %s days."
msgstr "įsūnytas nuo šiandien."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
#, fuzzy
msgid "orphaned since today."
msgstr "įsūnytas nuo šiandien."

#: ../../english/template/debian/wnpp.wml:102
#, fuzzy
msgid "orphaned since yesterday."
msgstr "įsūnytas nuo vakar"

#: ../../english/template/debian/wnpp.wml:106
#, fuzzy
msgid "orphaned since %s days."
msgstr "ruošiami nuo šiandien."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "reikalautas šiandien."

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "reikalautas vakar."

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "Reikalautas %s prieš dienų."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "paketo informacija"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr ""
