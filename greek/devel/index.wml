#use wml::debian::template title="Γωνιά προγραμματιστών του Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="efa5e923d43c06826f515b6647608c5f73fdfc8d" maintainer="galaxico"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Αν και όλες οι πληροφορίες σε αυτή τη σελίδα και όλοι οι σύνδεσμοι
σε άλλες σελίδες είναι δημόσια διαθέσιμοι, ο παρών ιστότοπος απευθύνεται πρωτίστως σε προγραμματιστές του Debian.</p>
</aside>

<ul class="toc">
<li><a href="#basic">Βασικά</a></li>
<li><a href="#packaging">Πακετάρισμα</a></li>
<li><a href="#workinprogress">Δουλειά σε εξέλιξη</a></li>
<li><a href="#projects">Έργα</a></li>
<li><a href="#miscellaneous">Διάφορα</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">Γενικές Πληροφορίες</a></h2>
      <p>Μια λίστα των τωρινών προγραμματιστών και συντηρητών, πώς να μπείτε στο Σχέδιο και σύνδεσμοι στην βάση δεδομένων των προγραμματιστών, το Καταστατικό, τη διαδικασία ψηφοφορίας, τις κυκλοφορίες και τις αρχιτεκτονικές.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Οργάνωση του Debian</a></dt>
        <dd>Πάνω από χίλιες εθελόντριες και εθελοντές είναι μέλη του Σχεδίου Debian. Η παρούσα σελίδα εξηγεί την οργανωτική δομή του Debian, καταγράφει τις ομάδες και τα μέλη τους καθώς και διευθύνσεις επικοινωνίας.</dd>
        <dt><a href="$(HOME)/intro/people">Οι άνθρωποι πίσω από το Debian</a></dt>
        <dd><a href="https://wiki.debian.org/DebianDeveloper">Προγραμματιστές του Debian (DD)</a> (πλήρη μέλη του Σχεδίου Debian) και <a href="https://wiki.debian.org/DebianMaintainer">Συντηρητές του Debian (DM)</a>, συνεισφέρουν στο Σχέδιο. Παρακαλούμε ρίξτε μια ματιά στην <a href="https://nm.debian.org/public/people/dd_all">λίστα των Προγραμματιστών του Debian</a> και στη  <a href="https://nm.debian.org/public/people/dm_all/">λίστα των Συντηρητών του Debian</a> για να βρείτε περισσότερα για τους ανθρώπους που εμπλέκονται, καθώς και για τα πακέτα που συντηρούν. Έχουμε επίσης έναν <a href="developers.loc">παγκόσμιο χάρτη των προγραμματιστών του Debian</a> και μια  <a href="https://gallery.debconf.org/">γκαλερί</a> με εικόνες από προηγούμενες εκδηλώσεις του Debian.</dd>
        <dt><a href="join/">Πώς να μπείτε στο Debian</a></dt>
        <dd>Θα θέλατε να συνεισφέρετε και να μπείτε στο Σχέδιο; Πάντα ψάχνουμε για καινούριους προγραμματιστές ή άτομα με ενθουσιασμό για το ελεύθερο λογισμικό με τεχνικές δεξιότητες. Για περισσότερες πληροφορίες παρακαλούμε επισκεφθείτε την ακόλουθη ιστοσελίδα.</dd>
        <dt><a href="https://db.debian.org/">Βάση δεδομένων των Προγραμματιστών</a></dt>
        <dd>Μερικές πληροφορίες σε αυτή τη βάση δεδομένων είναι προσβάσιμες για όλους, κάποιες άλλες είναι προσβάσιμες μόνο στους προγραμματιστές που έχουν συνδεθεί. Η βάση δεδομένων περιέχει πληροφορίες όπως <a href="https://db.debian.org/machines.cgi">μηχανήματα του Σχεδίου</a> και <a href="extract_key">GnuPG κλειδιά των προγραμματιστών</a>. Προγραμματιστές με λογαριασμό μπορούν να <a href="https://db.debian.org/password.html">αλλάξουν τον κωδικό τους</a> και να μάθουν πώς μπορούν να ρυθμίσουν την <a href="https://db.debian.org/forward.html">προώθηση της αλληλογραφίας τους</a> για τον λογαριασμό τους στο Debian. Αν σχεδιάζετε να χρησιμοποιήσετε κάποιο από τα μηχανήματα του Debian, παρακαλούμε βεβαιωθείτε ότι διαβάσατε τις <a href="dmup">Πολιτικές χρήσης των μηχανημάτων του Debian</a>.</dd>
        <dt><a href="constitution">Το Καταστατικό</a></dt>
        <dd>Το κείμενο αυτό περιγράφει την οργανωτική δομή για την τυπική διαδικασία λήψης αποφάσεων στο Σχέδιο.
        </dd>
        <dt><a href="$(HOME)/vote/">Πληροφορίες για τις ψηφοφορίες</a></dt>
        <dd>Πώς εκλέγουμε τους επικεφαλής μας, πώς επιλέγουμε τα λογότυπά μας και πώς ψηφίζουμε γενικά.</dd>
        <dt><a href="$(HOME)/releases/">Εκδόσεις σε κυκλοφορία</a></dt>
        <dd>Αυτή η σελίδα καταγράφει τις τρέχουσες κυκλοφορίες (<a href="$(HOME)/releases/stable/">σταθερή</a>, <a href="$(HOME)/releases/testing/">δοκιμαστική</a>, και <a href="$(HOME)/releases/unstable/">ασταθή</a>) και περιέχει ένα ευρετήριο των παλιών εκδόσεων και των κωδικών ονομασιών τουςε.</dd>
        <dt><a href="$(HOME)/ports/">Διαφορετικές αρχιτεκτονικές</a></dt>
        <dd>Το Debian τρέχει σε πολλές διαφορετικές αρχιτεκτονικές. Αυτή η σελίδα συλλέγει πληροφορίες σχετικά με διάφορες υλοποιήσεις του Debian, μερικές βασισμένες στον πυρήνα του Linux, άλλες βασισμένες στους πυρήνες FreeBSD, NetBSD και Hurd.</dd>

     </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Πακετάρισμα</a></h2>
      <p>Σύνδεσμοι προς το εγχειρίδιο της πολιτικής μας και άλλα κείμενα σχετικά με την πολιτική του Debian, διαδικασίες και άλλους πόρους για τους προγραμματιστές του Debian, και τον οδηγό των καινούριων συντηρητών.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Εγχειρίδιο της Πολιτικής του Debian</a></dt>
        <dd>Το εγχειρίδιο αυτό περιγράφει τις απαιτήσεις πολιτικής για τη διανομή του Debian. Αυτό περιλαμβάνει τη δομή και το περιεχόμενο της αρχειοθήκης του Debian, αρκετά ζητήματα σχεδιασμού του λειτουργικού συστήματος καθώς και τις τεχνικές απαιτήσεις που το κάθε πακέτο πρέπει να ικανοποιεί για να συμπεριληφθεί στη διανομή.
        <p>Με λίγα λόγια, <strong>πρέπει</strong> να το διαβάσετε.</p>
        </dd>
      </dl>

      <p>Υπάρχουν αρκετά άλλα κείμενα που σχετίζονται με την πολιτική και τα οποία μπορεί να σας ενδιααφέρουν:</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />Το Πρότυπο Ιεραρχίας Συστημάτων Αρχείων (FHS) ορίζει την δομή των καταλόγων
        και τα περιεχόμενά τους (τοποθεσία αρχείων)· συμμόρφωση με την έκδοση 3.0 είναι υποχρεωτική (δείτε το <a
        href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">κεφάλαιο
        9</a> του Εγχειριδίου της Πολιτικής του Debian).</li>
        <li>Λίστα των <a href="$(DOC)/packaging-manuals/build-essential"> build-essential πακέτων</a>
        <br />Θα πρέπει να έχετε στη διάθεσή σας αυτά τα πακέτα αν θέλετε να μεταγλωττίσετε λογισμικό, να φτιάξετε ένα πακέτο ή ένα σύνολο πακέτων. Δεν χρειάζεται να τα συμπεριλάβετε στη γραμμή <code>Build-Depends</code> όταν <a
        href="https://www.debian.org/doc/debian-policy/ch-relationships.html">δηλώνετε σχέσεις
        </a> μεταξύ πακέτων.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Μενού συστήματος</a>
        <br />Η δομή του Debian για τις επιλογές του μενού· παρακαλούμε ελέγξτε επίσης την τεκμηρίωση του
        <a href="$(DOC)/packaging-manuals/menu.html/">μενού συστήματος</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Πολιτική Emacs </a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Πολιτική Java</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Πολιτική Perl</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Πολιτική Python</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Debconf Specification</a></li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Πολιτική εφαρμογών Βάσεων Δεδομένων</a> (προσχέδιο)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Πολιτική Tcl/Tk</a> (προσχέδιο)</li>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Πολιτική του Debian για την Ada</a></li>
      </ul>

      <p>Παρακαλούμε ρίξτε επίσης μια ματιά στις <a
      href="https://bugs.debian.org/debian-policy">προτεινόμενες επικαιροποιήσεις στην
      Πολιτική του Debian</a>.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">Αναφορά των Προγραμματιστών</a></dt>

        <dd>
        Επισκόπηση των συνειστώμενων διαδικασιών και των διαθέσιμων πόρων για τους προγραμματιστές του
        Debian -- άλλο ένα κείμενο που <strong>πρέπει να διαβαστεί</strong>.
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">Οδηγός των Καινούριων Συντηρητών</a></dt>

        <dd>
        Πώς να φτιάξετε ένα πακέτο του Debian (σε απλή γλώσσα), συμπεριλαμβανομένων πολλών
        παραδειγμάτων. Αν σχεδιάζετε να γίνετε προγραμματιστής του Debian ή συντηρητής,
        αυτό είναι ένα καλό σημείο αφετηρίας.
        </dd>
      </dl>


    </div>

  </div>

</div>


<h2><a id="workinprogress">Εργασία σε εξέλιξη: σύνδεσμοι για τους ενεργούς προγραμματιστές και συντηρητές του Debian</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">&lsquo;Δοκιμαστική&rsquo; διανομή του Debian</a></dt>
  <dd>
    Παράγεται αυτόματα από την &lsquo;ασταθή&rsquo; διανομή:
    από εδώ θα πρέπει να πάρετε τα πακέτα σας ώστε αυτά να ληφθούν υπόψιν για την επόμενη κυκλοφορία του Debian.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Σφάλματα κρίσιμα για την κυκλοφορία</a></dt>
  <dd>
    Μια λίστα σφαλμάτων που μπορεί να προκαλέσουν την αφαίρεση ενός πακέτου από τη
    &lsquo;δοκιμαστική&rsquo; διανομή ή να προκαλέσουν μια καθυστέρηση για την κυκλοφορία
    της επόμενης έκδοσης. Αναφορές σφαλμάτων με κρισιμότητα μεγαλύτερη ή ίση της &lsquo;σοβαρής&rsquo; πληρούν τις προϋποθέσεις
    για τη λίστα αυτή, συνεπώς παρακαλούμε βεβαιωθείτε ότι τα έχετε διορθώσει, για τα πακέτα σας, όσο πιο γρήγορα γίνεται.
  </dd>

  <dt><a href="$(HOME)/Bugs/">Σύστημα Παρακολούθησης Σφαλμάτων του Debian (BTS)</a></dt>
    <dd>
    Για την αναφορά, συζήτηση και διόρθωση των σφαλμάτων. Το BTS είναι χρήσιμο τόσο για τους χρήστες όσο και για τους προγραμματιστές.
    </dd>

  <dt>Πληροφορίες για τα πακέτα του Debian</dt>
    <dd>
      Οι ιστοσελίδες <a href="https://qa.debian.org/developer.php">πληροφορίες πακέτων</a>
      και <a href="https://tracker.debian.org/">ιχνηλάτης πακέτων</a> παρέχουν συλλογές πολύτιμων
      πληροφοριών για τους συντηρητές. Προγραμματιστές που θέλουν να παρακολουθούν
      την πορεία άλλων πακέτων, μπορούν να εγγραφούν (μέσω αλληλογραφίας) σε μια υπηρεσία που
      στέλνει αντίγραφα των μηνυμάτων από το σύστημα BTS και ειδοποιήσεις για μεταφορτώσεις
      και εγκαταστάσεις. Παρακαλούμε δείτε το <a
      href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">εγχειρίδιο του ιχνηλάτη
      πακέτων</a> για περισσότερες πληροφορίες.
    </dd>

    <dt><a href="wnpp/">Πακέτα που χρειάζονται βοήθεια</a></dt>
      <dd>
      Τα Πακέτα που χρειάζονται δουλειά και τα Υποψήφια πακέτα (Work-Needing and Prospective Packages), για συντομία WNPP,
      είναι μια λίστα πακέτων του Debian που χρειάζονται καινούριο συντηρητή και πακέτα που δεν έχουν συμπεριληφθεί
      ακόμα στο Debian.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">Σύστημα Εισερχομένων</a></dt>
      <dd>
      Εσωτερικοί εξυπηρετητές της αρχειοθήκης: εδώ είναι που ανεβαίνουν τα καινούρια πακέτα. Τα πακέτα που γίνονται δεκτά είναι
      σχεδόν αμέσως διαθέσιμα μέσω ενός περιηγητή και μεταδίδονται στους <a href="$(HOME)/mirror/">καθρέφτες</a>
      τέσσερις φορές την ημέρα.
      <br />
      <strong>Σημείωση:</strong> Εξαιτίας της φύσης της υποδομής των &lsquo;εισερχομένων&rsquo;,
      δεν συνιστούμε το καθρέφτισμά της.
      </dd>

    <dt><a href="https://lintian.debian.org/">Αναφορές του Lintian</a></dt>
      <dd>
      Το <a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a>
      είναι ένα πρόγραμμα που ελέγχει αν ένα πακέτο συμμορφώνεται με την πολιτική του Debian.
      Οι προγραμματίστριες θα πρέπει να το χρησιμοποιούν πριν από κάθε μεταφόρτωση.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">&lsquo;Πειραματική&rsquo; διανομή του Debian</a></dt>
      <dd>
      Η &lsquo;πειραματική&rsquo; διανομή χρησιμοποιείται ως ένα προσωρινό δοκιμαστικό στάδιο για ιδιαίτερα
      πειραματικό λογισμικό. Παρακαλούμε εγκαταστήστε τα
      <a href="https://packages.debian.org/experimental/">πειραματικά
      πακέτα</a> μόνο αν ξέρετε πώς να χρησιμοποιήσετε την
      &lsquo;ασταθή&rsquo; διανομή.
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">Wiki του Debian</a></dt>
      <dd>
      Το Wiki του Debian με συμβουλές για προγραμματιστές και άλλα άτομα που συνεισφέρουν.
      </dd>
</dl>

<h2><a id="projects">Πρότζεκτ: εσωτερικές ομάδες και πρότζεκτ</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Ιστοσελίδες του Debian</a></li>
<li><a href="https://ftp-master.debian.org/">Αρχειοθήκη του Debian</a></li>
<li><a href="$(DOC)/ddp">Σχέδιο Τεκμηρίωσης του Debian (DDP)</a></li>
<li><a href="https://qa.debian.org/">Ομάδα Διασφάλισης Ποιότητας του Debian (QA Team)</a></li>
<li><a href="$(HOME)/CD/">Εικόνες CD/DVD</a></li>
<li><a href="https://wiki.debian.org/Keysigning">Υπογραφή κλειδιών</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">Συντονισμός υπογραφής κλειδιών</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Πρότζεκτ του Debian για το IPv6 </a></li>
<li><a href="buildd/">Autobuilder Network</a> and their <a href="https://buildd.debian.org/">Build Logs</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Debian Description Translation Project (DDTP)</a></li>
<li><a href="debian-installer/">Ο Εγκαταστάτης του Debian</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debian Women</a></li>
<li><a href="$(HOME)/blends/">Καθαρά Μείγματα του Debian</a></li>

</ul>


<h2><a id="miscellaneous">Διάφοροι σύνδεσμοι</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://debconf-video-team.pages.debian.net/videoplayer/">Καταγραφές</a> των ομιλιών στα συνέδριά μας</li>
<li><a href="passwordlessssh">Ρύθμιση του SSH</a> ώστε να μην σας ζητά κωδικό</li>
<li>Πώς να <a href="$(HOME)/MailingLists/HOWTO_start_list">αιτηθείτε μια καινούρια λίστα αλληλογραφίας</a></li>
<li>Πληροφορίες για τον <a href="$(HOME)/mirror/">καθρεφτισμό του Debian</a></li>
<li>Το <a href="https://qa.debian.org/data/bts/graphs/all.png">γράφημα όλων των σφαλμάτων</a></li>
<li><a href="https://ftp-master.debian.org/new.html">Καινούρια πακέτα</a> στην αναμονή να συμπεριληφθούν στο Debian (NEW Queue)</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">Καινούρια πακέτα του Debian</a> για τις τελευταίες 7 μέρες</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Πακέτα που έχουν αφαιρεθεί από το Debian</a></li>
</ul>
