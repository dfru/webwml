#use wml::debian::translation-check translation="165625c3a669960bb5e1c766db812564b5fd665e" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sårbarheder blev opdaget i implementeringen af 
programmeringssproget Perl.  Projektet Common Vulnerabilities and Exposures har 
registreret følgende problemer:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18311">CVE-2018-18311</a>

    <p>Jayakrishna Menon og Christophe Hauser opdagede en 
    heltalsoverløbssårbarhed i Perl_my_setenv, førende til et heapbaseret 
    bufferoverløb med angriberkontrollerede inddata.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18312">CVE-2018-18312</a>

    <p>Eiichi Tsukata opdagede at et fabrikeret regulært udtryk, kunne medføre 
    en heapbaseret bufferoverløbsskrivning under kompilering, potentielt førende 
    til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18313">CVE-2018-18313</a>

    <p>Eiichi Tsukata opdagede at et fabrikeret regulært udtryk kunne medføre en 
    heapbaseret bufferoverløbslæsning under kompilering, hvilket førte til 
    informationslækage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18314">CVE-2018-18314</a>

    <p>Jakub Wilk opdagede at et særlig fremstillet regulært udtryk kunne føre 
    til et heapbaseret bufferoverløb.</p></li>

</ul>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 5.24.1-3+deb9u5.</p>

<p>Vi anbefaler at du opgraderer dine perl-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende perl, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/perl">https://security-tracker.debian.org/tracker/perl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4347.data"
