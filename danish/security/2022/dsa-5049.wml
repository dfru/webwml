#use wml::debian::translation-check translation="f95c4fb5cb2a56eaf77f41eba98c0ac87fd478ec" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i Flatpak, et applikationsudrulningsframework 
til skrivebordsapplikationer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43860">CVE-2021-43860</a>

    <p>Ryan Gonzalez opdagede at Flatpak ikke på korrekt vis validerede om 
    rettighederne, der vises for en bruger vedrørende en applikation på 
    installeringstidspunktet, svarer til de faktiske rettigheder, som er tildelt 
    applikationen på kørselstidspunktet.  Ondsindede applikationer kunne derfor 
    tildele sig selv rettigheder, uden brugerens samtykke.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21682">CVE-2022-21682</a>

    <p>Flatpak forhindrede ikke altid en ondsindet bruger af flatpak-builder i 
    at skrive til det lokale filsystem.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet
i version 1.10.7-0+deb11u1.</p>

<p>Bemærk at flatpak-builder af kompabilitetshensyn også skulle opdateres, og er 
nu tilgængelig som version 1.0.12-1+deb11u1 i bullseye.</p>

<p>Vi anbefaler at du opgraderer dine flatpak- og flatpak-builder-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende flatpak, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/flatpak">\
https://security-tracker.debian.org/tracker/flatpak</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5049.data"
