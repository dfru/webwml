<define-tag pagetitle>Debian 10 <q>Buster</q> utgiven</define-tag>
<define-tag release_date>2019-07-06</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="cbfcd50b343ab08499a29ae770a5528eda687986" maintainer="Andreas Rönnquist"

<p>Efter 25 månaders utveckling presenterar Debian-projektet stolt en ny stabil
utgåva 10 (med kodnamnet <q>Buster</q>), som kommer att stödjas de kommande
5 åren tack vare det kombinerade arbetet mellan
<a href="https://security-team.debian.org/">Debians säkerhetsgrupp</a> och
gruppen för <a href="https://wiki.debian.org/LTS">Debians långtidsstöd</a>.
</p>

<p>
Debian 10 <q>Buster</q> släpps med flera program och skrivbordsmiljöer.
Bland annat inkluderas nu skrivbordsmiljöerna:
</p>
<ul>
<li>Cinnamon 3.8,</li>
<li>GNOME 3.30,</li>
<li>KDE Plasma 5.14,</li>
<li>LXDE 0.99.2,</li>
<li>LXQt 0.14,</li>
<li>MATE 1.20,</li>
<li>Xfce 4.12.</li>
</ul>

<p>
I denna utgåva använder GNOME displayservern Wayland som standard istället
för Xorg. Wayland har en enklare och mer modern design, vilket medför fördelar
när det gäller säkerhet. Dock så installeras Xorg fortfarande som standard och
displayhanteraren tillåter fortfarande användare att välja Xorg som
displayserver för sin nästa session.
</p>

<p>
Tack vare projektet Reproducible Builds kommer mer än 91% av källkodspaketen
som inkluderas i Debian 10 bygga bit-för-bit identiska binära paket.
Detta är en viktig verifikationsfunktion som skyddar användare mot
illasinnade försök att mixtra med kompilatorer och byggnätverk. Framtida
Debianutgåvor kommer att inkludera verktyg och metadata så att slutanvändare
kan validera ursprunget för paket i arkivet.
</p>

<p>
För dem i säkerhetskänsliga miljöer installeras och aktiveras AppArmor som
standard, ett obligatoriskt åtkomstkontrollramverk för att begränsa ett
programs rättigheter.
Utöver detta så kan alla metoder som tillhandahålls av APT (förutom cdrom, gpgv
och rsh) valfritt använda skyddsmekanismen <q>seccomp-BPF</q>. https-metoden
för APT inkluderas i apt-paketet och behöver inte installeras separat.
</p>

<p>
Nätverksfiltrering baseras på ramverket nftables som standard i
Debian 10 <q>Buster</q>. Med start i iptables v1.8.2 inkluderar det binära
paketet både iptables-nft och iptables-legacy, två varianter på kommandoradsgränssnittet
iptables. Den nftables-baserade varianten använder Linuxkärnans undersystem
nf_tables. Systemet <q>alternatives</q> kan användas för att välja mellan
de olika varianterna.
</p>

<p>
Stödet för UEFI (<q>Unified Extensible Firmware Interface</q>) som först
introducerades i Debian 7 (med kodnamnet <q>Wheezy</q>) fortsätter att
förbättras i Debian 10 <q>Buster</q>. Stöd för Secure Boot inkluderas i denna
utgåva för amd64-, i386- och arm64-arkitekturerna och skall fungera direkt på
de flesta maskiner med Secure Boot-stöd. Detta betyder att användare inte
skall behöva inaktivera Secure Boot-stöd i fastprogramvarukonfigurationen.
</p>

<p>
Paketen cups och cups-filters installeras som standard i Debian 10
<q>Buster</q> vilket ger användare allt som behövs för att dra fördel av
utskrifter utan drivrutiner. Nätverksutskriftsköer eller IPP-skrivare kommer att
ställas in automatiskt och hanteras av cups-browsed och användningen av
icke-fria leverantörsskrivardrivrutiner och insticksmoduler kan undvikas.
</p>

<p>
Debian 10 <q>Buster</q> inkluderar en mängd uppdaterade mjukvarupaket, (över
62% av alla paket i <q>Stretch</q>), så som:
</p>
<ul>
<li>Apache 2.4.38</li>
<li>BIND DNS Server 9.11</li>
<li>Chromium 73.0</li>
<li>Emacs 26.1</li>
<li>Firefox 60.7 (i paketet firefox-esr)</li>
<li>GIMP 2.10.8</li>
<li>GNU Compiler Collection 7.4 och 8.3</li>
<li>GnuPG 2.2</li>
<li>Golang 1.11</li>
<li>Inkscape 0.92.4</li>
<li>LibreOffice 6.1</li>
<li>Linux 4.19 series</li>
<li>MariaDB 10.3</li>
<li>OpenJDK 11</li>
<li>Perl 5.28</li>
<li>PHP 7.3</li>
<li>PostgreSQL 11</li>
<li>Python 3 3.7.2</li>
<li>Ruby 2.5.1</li>
<li>Rustc 1.34</li>
<li>Samba 4.9</li>
<li>systemd 241</li>
<li>Thunderbird 60.7.2</li>
<li>Vim 8.1</li>
<li>mer än 59 000 andra mjukvarupaket klara att använda, byggda från
nästan 29 000 källkodspaket.</li>
</ul>

<p>
Med detta breda urval av paket och dess traditionellt breda
arkitektursstöd, håller sig Debian till sitt mål att vara det
universella operativsystemet. Det passar många olika användningsområden:
från skrivbordssystem till laptops; från utvecklingsservrar till
klustersystem; och för databaser, webb, eller lagringsservrar. På samma
gång säkerställer ytterligare kvalitetssäkringsinsatser, så som automatiska
installations- och uppgraderingstester för alla paket i Debians arkiv, att
<q>Buster</q> uppfyller de höga förväntningarna som användare har på en
stabil Debianutgåva.
</p>

<p>
Totalt tio arkitekturer stöds:
64-bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bit PC / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
för ARM, <code>armel</code>
och <code>armhf</code> för äldre och senare 32-bitars hårdvara,
samt <code>arm64</code> för 64-bitars <q>AArch64</q>-arkitekturen,
och för MIPS, <code>mips</code> (big-endian)
och <code>mipsel</code> (little-endian)-arkitekturerna för 32-bitars hårdvara och <code>mips64el</code>-arkitektur
för 64-bitars little-endian-hårdvara.
</p>

<h3>Vill du prova?</h3>
<p>
Om du helt enkelt vill testa Debian 10 <q>Buster</q> utan att installera det,
kan du använda en av de tillgängliga <a href="$(HOME)/CD/live/">live-avbildningarna</a> som laddar och kör
det fullständiga operativsystemet i ett skrivskyddat läge i din dators minne.
</p>

<p>
Dessa live-avbildningar tillhandahålls för arkitekturerna <code>amd64</code> och
<code>i386</code> och finns tillgängliga för DVD-skivor, USB-minnen och
nätverksstart-setups. Användaren kan välja mellan olika skrivbordsmiljöer att
testa:
Cinnamon, GNOME, KDE Plasma, LXDE, MATE, Xfce och, nytt i Buster, LXQt. Debian Live
Buster återintroducerar standard-Live-avbildningen, så det är även möjligt att
prova ett bassystem av Debian utan något grafiskt användargränssnitt.
</p>

<p>
Om du trivs med operativsystemet har du alternativet att installera från
live-avbildningen till din dators hårddisk. Live-avbildningen inkluderar
den oberoende Calamares-installeraren så väl som den vanliga Debian-installeraren.
Ytterligare information finns tillgänglig i
<a href="$(HOME)/releases/buster/releasenotes">versionsfakta</a> samt
<a href="$(HOME)/CD/live/">avsnittet för liveinstallationsavbildningar
på Debians webbplats</a>.
</p>

<p>
Om du väljer att installera Debian 10 <q>Buster</q> direkt till din
dators hårddisk kan du välja bland en mängd olika installationsmedia som
Blu-ray Disc, DVD, CD, USB-minne, eller via en nätverksanslutning.
Flera skrivbordsmiljöer &mdash; GNOME, KDE Plasma-skrivbord och
program, LXDE och Xfce &mdash; kan installeras genom dessa avbildningar.
Utöver detta finns <q>multi-arkitekturs</q>-CDs tillgängliga som ger
stöd för att installera från ett val av arkitekturer från en disk. Eller så
kan du skapa bootbar USB-installationsmedia
(Se <a href="$(HOME)/releases/buster/installmanual">Installationsguiden</a>
för ytterligare detaljer).
</p>

<p>För molnanvändare erbjuder Debian direkt stöd för många av de
mest kända molnlösningarna. Officella Debianavbildningar är enkelt valbara
genom varje marknadsplats för avbildningar. Debian publicerar
även
<a href="https://cdimage.debian.org/cdimage/openstack/current/">färdigbyggda
OpenStack-avbildningar</a> för arkitekturerna <code>amd64</code> och
<code>arm64</code>, redo att hämtas och användas i lokala molnlösningar.
</p>

<p>
Debian kan nu installeras på 76 olika språk, med de flesta av dessa tillgängliga
både i text-baserat och grafiskt användargränssnitt.
</p>

<p>
Installationsavbildningarna kan hämtas redan nu via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (den rekommenderade metoden),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, eller
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; se
<a href="$(HOME)/CD/">Debian på CD</a> för ytterligare information. <q>Buster</q> kommer
även snart att finnas tillgängligt på fysisk DVD, CD-ROM och Blu-ray Disc från
ett antal <a href="$(HOME)/CD/vendors">försäljare</a>.
</p>



<h3>Uppgradera Debian</h3>
<p>
Uppgraderingar till Debian 10 från den föregående utgåvan, Debian 9
(med kodnamn <q>Stretch</q>), hanteras automatiskt med hjälp av
pakethanteringssystemet apt för de flesta konfigurationer.
Som alltid kan Debiansystem uppgraderas smärtfritt, på plats,
utan någon påtvingat stillestånd, men det rekommenderas starkt att läsa
<a href="$(HOME)/releases/buster/releasenotes">versionsfakta</a> så väl
som <a href="$(HOME)/releases/buster/installmanual">installationsmanualen</a>
för möjliga problem, och för detaljerad information om installation och
uppgradering. Versionsfakta kommer att förbättras och översättas till
ytterligare språk under veckorna efter utgåvan.
</p>


<h2>Om Debian</h2>

<p>
Debian är ett fritt operativsystem, utvecklat av
tusentals frivilliga från hela världen som samarbetar via Internet.
Debian-projektets styrkor är dess volontärbas, dess hängivenhet till
Debians Sociala kontrakt och fri mjukvara, och dess åtagande att
tillhandahålla det bästa operativsystemet möjligt. Denna nya
utgåva är ytterligare ett viktigt steg i denna riktning.
</p>


<h2>Kontaktinformation</h2>

<p>
För ytterligare information, var vänlig besök Debians webbsidor på
<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post
(på engelska) till &lt;press@debian.org&gt;.
</p>
