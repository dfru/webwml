<define-tag pagetitle>Uppdaterad Debian 11; 11.1 utgiven</define-tag>
<define-tag release_date>2021-10-09</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a" maintainer="Andreas Rönnquist"

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin första uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling.</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction apr "Prevent out-of-bounds array dereference">
<correction atftp "Fix buffer overflow [CVE-2021-41054]">
<correction automysqlbackup "Fix crash when using <q>LATEST=yes</q>">
<correction base-files "Update for the 11.1 point release">
<correction clamav "New upstream stable release; fix clamdscan segfaults when --fdpass and --multipass are used together with ExcludePath">
<correction cloud-init "Avoid duplicate includedir in /etc/sudoers">
<correction cyrus-imapd "Fix denial-of-service issue [CVE-2021-33582]">
<correction dazzdb "Fix a use-after-free in DBstats">
<correction debian-edu-config "debian-edu-ltsp-install: extend main server related exclude list; add slapd and xrdp-sesman to the list of masked services">
<correction debian-installer "Rebuild against proposed updates; update Linux ABI to 5.10.0-9; use udebs from proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates; use udebs from proposed-updates and stable; use xz-compressed Packages files">
<correction detox "Fix handling of large files">
<correction devscripts "Make the --bpo option target bullseye-backports">
<correction dlt-viewer "Add missing qdlt/qdlt*.h header files to dev package">
<correction dpdk "New upstream stable release">
<correction fetchmail "Fix segmentation fault and security regression">
<correction flatpak "New upstream stable release; don't inherit an unusual $XDG_RUNTIME_DIR setting into the sandbox">
<correction freeradius "Fix thread crash and sample configuration">
<correction galera-3 "New upstream stable release">
<correction galera-4 "New upstream stable release; solve circular Conflicts with galera-3 by no longer providing a virtual <q>galera</q> package">
<correction glewlwyd "Fix possible buffer overflow during FIDO2 signature validation in webauthn registration [CVE-2021-40818]">
<correction glibc "Restart openssh-server even if it has been deconfigured during the upgrade; fix text fallback when debconf is unusable">
<correction gnome-maps "New upstream stable release; fix a crash when starting up with last-used map type being aerial, and no aerial tile definition is found; don't sometimes write broken last view position on exit; fix hang when dragging around route markers">
<correction gnome-shell "New upstream stable release; fix freeze after cancelling (some) system-modal dialogs; fix word suggestions in on-screen keyboard; fix crashes">
<correction hdf5 "Adjust package dependencies to improve upgrade paths from older releases">
<correction iotop-c "Properly handle UTF-8 process names">
<correction jailkit "Fix creation of jails that need to use /dev; fix library presence check">
<correction java-atk-wrapper "Also use dbus to detect accessibility being enabled">
<correction krb5 "Fix KDC null dereference crash on FAST request with no server field [CVE-2021-37750]; fix memory leak in krb5_gss_inquire_cred">
<correction libavif "Use correct libdir in libavif.pc pkgconfig file">
<correction libbluray "Switch to embedded libasm; the version from libasm-java is too new">
<correction libdatetime-timezone-perl "New upstream stable release; update DST rules for Samoa and Jordon; confirmation of no leap andra on 2021-12-31">
<correction libslirp "Fix multiple buffer overflow issues [CVE-2021-3592 CVE-2021-3593 CVE-2021-3594 CVE-2021-3595]">
<correction linux "New upstream stable release; increase ABI to 9; [rt] Update to 5.10.65-rt53; [mipsel] bpf, mips: Validate conditional branch offsets [CVE-2021-38300]">
<correction linux-signed-amd64 "New upstream stable release; increase ABI to 9; [rt] Update to 5.10.65-rt53; [mipsel] bpf, mips: Validate conditional branch offsets [CVE-2021-38300]">
<correction linux-signed-arm64 "New upstream stable release; increase ABI to 9; [rt] Update to 5.10.65-rt53; [mipsel] bpf, mips: Validate conditional branch offsets [CVE-2021-38300]">
<correction linux-signed-i386 "New upstream stable release; increase ABI to 9; [rt] Update to 5.10.65-rt53; [mipsel] bpf, mips: Validate conditional branch offsets [CVE-2021-38300]">
<correction mariadb-10.5 "New upstream stable release; security fixes [CVE-2021-2372 CVE-2021-2389]">
<correction mbrola "Fix end of file detection">
<correction modsecurity-crs "Fix request body bypass issue [CVE-2021-35368]">
<correction mtr "Fix regression in JSON output">
<correction mutter "New upstream stable release; kms: Improve handling of common video modes that might exceed the possible bandwidth; ensure valid window texture size after viewport changes">
<correction nautilus "Avoid opening multiple selected files in multiple application instances; don't save window size and position when tiled; fix some memory leaks; update translations">
<correction node-ansi-regex "Fix regular expression-based denial of service issue [CVE-2021-3807]">
<correction node-axios "Fix regular expression-based denial of service issue [CVE-2021-3749]">
<correction node-object-path "Fix prototype pollution issues [CVE-2021-23434 CVE-2021-3805]">
<correction node-prismjs "Fix regular expression-based denial of service issue [CVE-2021-3801]">
<correction node-set-value "Fix prototype pollution [CVE-2021-23440]">
<correction node-tar "Remove non-directory paths from the directory cache [CVE-2021-32803]; strip absolute paths more comprehensively [CVE-2021-32804]">
<correction osmcoastline "Fix projections other than WGS84">
<correction osmpbf "Rebuild against protobuf 3.12.4">
<correction pam "Fix syntax error in libpam0g.postinst when a systemd unit fails">
<correction perl "Security update; fix a regular expression memory leak">
<correction pglogical "Update for PostgreSQL 13.4 snapshot handling fixes">
<correction pmdk "Fix missing barriers after non-temporal memcpy">
<correction postgresql-13 "New upstream stable release; fix mis-planning of repeated application of a projection step [CVE-2021-3677]; disallow SSL renegotiation more completely">
<correction proftpd-dfsg "Fix <q>mod_radius leaks memory contents to radius server</q> and <q>sftp connection aborts with </q>Corrupted MAC on input""; skip escaping of already-escaped SQL text">
<correction pyx3 "Fix horizontal font alignment issue with texlive 2020">
<correction reportbug "Update suite names following bullseye release">
<correction request-tracker4 "Fix login timing side-channel attack issue [CVE-2021-38562]">
<correction rhonabwy "Fix JWE CBC tag computation and JWS alg:none signature verification">
<correction rpki-trust-anchors "Add HTTPS URL to the LACNIC TAL">
<correction rsync "Re-add --copy-devices; fix regression in --delay-updates; fix edge case in --mkpath; fix rsync-ssl; fix --sparce and --inplace; update options available to rrsync; documentation fixes">
<correction ruby-rqrcode-rails3 "Fix for ruby-rqrcode 1.0 compatibility">
<correction sabnzbdplus "Prevent directory escape in renamer function [CVE-2021-29488]">
<correction shellcheck "Fix rendering of long options in manpage">
<correction shiro "Fix authentication bypass issues [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510]; update Spring Framework compatibility patch; support Guice 4">
<correction speech-dispatcher "Fix setting of voice name for the generic module">
<correction telegram-desktop "Avoid crash when auto-delete is enabled">
<correction termshark "Include themes in package">
<correction tmux "Fix a race condition which results in the config not being loaded if several clients are interacting with the server while it's initializing">
<correction txt2man "Fix regression in handling display blocks">
<correction tzdata "Update DST rules for Samoa and Jordan; confirm the absence of a leap andra on 2021-12-31">
<correction ublock-origin "New upstream stable release; fix denial of service issue [CVE-2021-36773]">
<correction ulfius "Ensure memory is initialised before use [CVE-2021-40540]">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2021 4959 thunderbird>
<dsa 2021 4960 haproxy>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4965 libssh>
<dsa 2021 4966 gpac>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4968 haproxy>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4972 ghostscript>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4976 wpewebkit>
<dsa 2021 4977 xen>
<dsa 2021 4978 linux-signed-amd64>
<dsa 2021 4978 linux-signed-arm64>
<dsa 2021 4978 linux-signed-i386>
<dsa 2021 4978 linux>
<dsa 2021 4979 mediawiki>
</table>

<p>
Under de sista stegen i Bullseyes frysning släpptes några uppdateringar via
<a href="https://security.debian.org/">säkerhetsarkivet</a> men utan
en medföljande DSA. Ni finner detaljerna för dessa uppdateringar nedan.
</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction apache2 "Fix mod_proxy HTTP2 request line injection [CVE-2021-33193]">
<correction btrbk "Fix arbitrary code execution issue [CVE-2021-38173]">
<correction c-ares "Fix missing input validation on hostnames returned by DNS servers [CVE-2021-3672]">
<correction exiv2 "Fix overflow issues [CVE-2021-29457 CVE-2021-31292]">
<correction firefox-esr "New upstream stable release [CVE-2021-29980 CVE-2021-29984 CVE-2021-29985 CVE-2021-29986 CVE-2021-29988 CVE-2021-29989]">
<correction libencode-perl "Encode: mitigate @INC pollution when loading ConfigLocal [CVE-2021-36770]">
<correction libspf2 "spf_compile.c: Correct size of ds_avail [CVE-2021-20314]; fix <q>reverse</q> macro modifier">
<correction lynx "Fix leakage of credentials if SNI was used together with a URL containing credentials [CVE-2021-38165]">
<correction nodejs "New upstream stable release; fix use after free issue [CVE-2021-22930]">
<correction tomcat9 "Fix authentication bypass issue [CVE-2021-30640] and request smuggling issue [CVE-2021-33037]">
<correction xmlgraphics-commons "Fix server side request forgery issue [CVE-2020-11988]">
</table>


<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>
