#use wml::debian::template title="Bidra: Hur du kan hjälpa Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="3a5c68f115956c48aa60c6aa793eb4d802665b23"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#coding">Utveckling och paketunderhåll</a></li>
    <li><a href="#testing">Testning och felsökande</a></li>
    <li><a href="#documenting">Författa dokumentation och tagga paket</a></li>
    <li><a href="#translating">Översättning och lokalisering</a></li>
    <li><a href="#usersupport">Hjälpa andra användare</a></li>
    <li><a href="#events">Organisera evenemang</a></li>
    <li><a href="#donations">Donera pengar, hårdvara eller bandbredd</a></li>
    <li><a href="#usedebian">Använda Debian</a></li>
    <li><a href="#organizations">Hur din organisation kan stödja Debian</a></li>
  </ul>
</div>

<p>Debian är inte bara ett operativsystem, det är en gemenskap. En
stor mängd folk med många olika kunskaper bidrar till projektet: vår
mjukvara, artwork, wikin och annan dokumentation är resultatet av en
gemensam ansträngning av en stor grupp individer. Inte alla är
utvecklare, och du behöver verkligen inte veta hur man kodar för att
kunna delta. Det finns många olika sätt som du kan hjälpa till för att
göra Debian bättre än det är. Om du skulle vilja delta finns här några
föreslag för både erfarana och oerfarna användare.</p>

<h2><a id="coding">Utveckling och paketunderhåll</a></h2>

<aside class="light">
  <span class="fa fa-code-branch fa-5x"></span>
</aside>

<p>Kanske du vill skriva en ny applikation från början, kanske du vill
implementera en ny funktion i ett redan existerande program. Om du är
en utvecklare och vill bidra till Debian kan du också hjälpa oss att
preparera mjukvaran i Debian för enkel installation, vilket vi kallar
"paketering". Ta en titt på den här listan för några idéer om hur du kan
komma igång:</p>

<ul>
  <li>Paketera applikationer, exempelvis dom som du har erfarenhet av eller
      anser vara värdefulla för Debian. För mer information om hur du blir
      paketansvarig, se <a href="$(HOME)/devel/">Utvecklarhörnan</a>.</li>
  <li>Hjälp till att underhålla existerande applikationer, exempelvis genom
      att bidra felrättningar (patchar) eller ytterligare information i
      <a href="https://bugs.debian.org/">felrapporteringssystemet</a>. Alternativt
      kan gå med i en underhållsgrupp eller gå med ett mjukvaruprojekt på
      <a href="https://salsa.debian.org/">Salsa</a> (vår egen Gitlabinstans).</li>
  <li>Assistera oss genom att <a
      href="https://security-tracker.debian.org/tracker/data/report">spåra</a> och
      <a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">rätta</a>
      <a href="$(HOME)/security/">säkerhetsproblem</a> i Debian.</li>
  <li>Du kan även hjälpa oss med härdning av <a
  href="https://wiki.debian.org/Hardening">paket</a>, <a
  href="https://wiki.debian.org/Hardening/RepoAndImages">förråd och
  avbildningar</a>, och <a href="https://wiki.debian.org/Hardening/Goals">andra
  komponenter</a>.</li>
  <li>Intresserad av att <a href="$(HOME)/ports/">anpassa</a> Debian till
  någon arkitektur som du har erfarenhet av? Du kan starta en ny anpassning
  eller bidra till en redan existerande.</li>
  <li>Hjälp oss att förbättra Debian-relaterade <a
  href="https://wiki.debian.org/Services">tjänster</a> eller skapa och underhåll
  nya, <a href="https://wiki.debian.org/Services#wishlist">föreslagna eller
  efterfrågade</a> av gemenskapen.</li>
</ul>

<h2><a id="testing">Testning och felsökande</a></h2>

<aside class="light">
  <span class="fa fa-bug fa-5x"></span>
</aside>

<p>Precis som alla andra mjukvaruprojekt behöver Debian användare som testar
operativsystemet och dess applikationer. Ett sätt att bidra är att installera
den senaste versionen och rapportera tillbaks till utvecklarna om något inte
fungerar som det skall. Vi behöver också folk för att testa vår
installationsmedia, secure boot och bootloadern U-Boot på olika hårdvara.
</p>
 
<ul>
   <li>Du kan använda vårt <a href="https://bugs.debian.org/">felrapporteringssystem</a>
      för att rapportera alla problem du hittar i Debian. Före du gör detta, var
      god säkerställ att problemet inte redan har rapporterats.</li>
   <li>Besök felspårningssystemet och försök att söka igenom felen som
      associeras med paket som du använder. Se om du kan bistå med ytterligare
      information och reproducera problemen som beskrivs.</li>
   <li>Testa <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">Debianinstalleraren
   och Live-ISO-avbildningar</a>, stöd för <a href="https://wiki.debian.org/SecureBoot/Testing">secure
   boot</a>, <a href="https://wiki.debian.org/LTS/TestSuites">LTS-uppdateringar</a>,
   och bootloadern <a href="https://wiki.debian.org/U-boot/Status">U-Boot</a>.</li>
</ul>

<h2><a id="documenting">Författa dokumentation och tagga paket</a></h2>
 
<aside class="light">
  <span class="fa fa-file-alt fa-5x"></span>
</aside>

<p>Om du upplever några problem i Debian och inte kan skriva kod för att
lösa problemet, kanske att ta anteckningar och skriva ner din lösning är
ett alternativ. På det sättet kan du hjälpa andra användare som kan ha liknande
problem. All Debiandokumentation är skriven av medlemmar i gemenskapen och
det finns flera olika sätt som du kan hjälpa till.</p>

<ul>
   <li>Gå med <a href="$(HOME)/doc/ddp">Debians dokumentationsprojekt</a> och
      hjälp till med den officiella Debiandokumentationen.</li>
   <li>Bidra till <a href="https://wiki.debian.org/">Debian Wikin</a></li>
   <li>Tagga och kategorisera paket på <a
   href="https://debtags.debian.org/">Debtags</a>-webbplatsen, så att
   Debiananvändare lätt kan hitta mjukvaran som dom söker efter.</li>
</ul>


<h2><a id="translating">Översättning och lokalisering</a></h2>

<aside class="light">
  <span class="fa fa-language fa-5x"></span>
</aside>

<p>
Om ditt modersmål inte är engelska, men du har tillräckligt bra kunskaper i
engelska för att förstå och översätta mjukvara eller Debianrelaterad
information så som webbplatser, dokumentation, osv?  Varför inte gå med i
en översättningsgrupp och konvertera Debianapplikationer till ditt modersmål.
Vi söker även efter folk som kan kontrollera existerande översättningar och
göra felrapporter om nödvändigt.
</p>


# Translators, link directly to your group's pages
<ul>
   <li>Allt som har med Debians internationalisering diskuteras på
   <a href="https://lists.debian.org/debian-i18n/">i18n-sändlistan</a>.</li>
   <li>För frågor rörande svenska översättningar, kontakta sändlistan
   <a href="https://lists.debian.org/debian-l10n-swedish/">debian-l10n-swedish</a>.</li>
   <li>Om du talar ett språk som modersmål som inte stöds i Debian ännu?
   Kontakta oss via webbplatsen <a href="$(HOME)/international/">Debian Internationellt</a>.</li>
</ul>


<h2><a id="usersupport">Hjälpa andra användare</a></h2>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>

<p>Du kan även bidra till projektet genom att hjälpa andra Debiananvändare.
Projektet använder olika supportkanaler, exempelvis sändlistor i
olika språk, och IRC-kanaler. För mer information, se vår <a
href="$(HOME)/support">sida för support</a>, <a
href="https://lists.debian.org/debian-user-swedish/">sändlistan för svensk
support</a> och den svenska IRC-kanalen <code>#debian.se</code> på
servern <var>irc.debian.org</var>.

# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language

<h2><a id="events">Organisera evenemang</a></h2>

<aside class="light">
  <span class="fas fa-calendar-check fa-5x"></span>
</aside>

<p>Utöver den årliga Debiankonfensen (DebConf) finns det flera
mindre möten och get-togethers i olika länder varje år. Att ta del i
eller hjälpa att organisera ett <a href="$(HOME)/events/">evenemang</a> är
en god möjlighet att möta andra Debiananvändare och utvecklare.
</p>

<ul>
   <li>Hjälp till under den årliga <a href="https://debconf.org/">Debiankonferensen</a>,
       exempelvis genom att spela in <a href="https://video.debconf.org/">videos</a> av
       föreläsningar och presentationer, välkomna besökare och hjälpa talarna,
       organisera speciella evenemang under DebConf (som den årliga ost- och
       vinfesten), hjälpa till med förberedelse och uppstädning, osv.</li>
   <li>Utöver detta finns det flera <a
       href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a>-evenemang,
       lokala möten som organiseras av Debians projektmedlemmar.</li>
   <li>Du kan också skapa eller gå med en <a href="https://wiki.debian.org/LocalGroups">lokal
       Debiangrupp</a> med regelbundna möten och andra aktiviteter.</li>
   <li>Kolla även på andra evenemang som <a
       href="https://wiki.debian.org/DebianDay">Debian Day-evenemang
       </a>, <a href="https://wiki.debian.org/ReleaseParty">utgivningsfester</a>,
       <a href="https://wiki.debian.org/BSP">felrättarfester</a>,
       <a href="https://wiki.debian.org/Sprints">utvecklingssprints</a>, or
       <a href="https://wiki.debian.org/DebianEvents">andra evenemang</a> över hela världen.</li>
</ul>

<h2><a id="donations">Donera pengar, hårdvara eller bandbredd</a></h2>

<aside class="light">
  <span class="fas fa-gift fa-5x"></span>
</aside>

<p>Alla donationer till Debianprojektet hanteras av Debians projektledare
(DPL). Med ditt stöd kan vi köpa hårdvara, domäner,
kryptografiska certifikat, osv. Vi använder även medel för att sponsra
DebConf- och MiniDebConf-evenemang, utvecklingssprints, närvaro vid andra
evenemang och andra saker.</p>

<ul>
  <li>Du kan <a href="$(HOME)/donations">donera</a> pengar, utrustning och
  tjänster till Debianprojektet.</li>
  <li>Vi letar konstant efter <a href="$(HOME)/mirror/">speglingar</a> runt
  hela världen.</li>
  <li>För våra Debiananpassningar är vi beroende av våra <a
  href="$(HOME)/devel/buildd/">automatkompileringsnätverk</a>.</li>
</ul>

<h2><a id="usedebian">Använd Debian och tala om det</a></h2>

<aside class="light">
  <span class="fas fa-bullhorn fa-5x"></span>
</aside>

<p>Sprid budskapet och berätta för andra om Debian och Debiangemenskapen.
Rekommendera operativsystemet till andra användare och visa dom hur man
installerar det. Använd det och njut, helt enkelt -- Det är antagligen det
lättaste sättet att ge tillbaks till Debianprojektet.</p>

<ul>
  <li>Hjälp till att marknadsföra Debian genom att ge föreläsningar och demonstrera
      det för andra användare.</li>
  <li>Bidra till vår <a href="https://www.debian.org/devel/website/">webbplats</a>
      och hjälp oss att förbättra Debians publika front.</li>
  <li>Ta <a href="https://wiki.debian.org/ScreenShots">skärmdumpar</a> och
      <a href="https://screenshots.debian.net/upload">ladda upp</a> dem till
      <a href="https://screenshots.debian.net/">screenshots.debian.net</a> så
      att våra användare kan se hur mjukvara i Debian ser ut innan dom använder
      den.</li>
  <li>Du kan aktivera bidrag till <a
      href="https://packages.debian.org/popularity-contest">popularity-contest</a>
      så vi vet vilka paket som är populära och mest användbara för alla.</li>
</ul>
 
 
<h2><a id="organizations">Hur kan din organisation stödja Debian</a></h2>
 
<aside class="light">
  <span class="fa fa-check-circle fa-5x"></span>
</aside>

<p>
   Oberoende på om du jobbar inom utbildning, kommersiellt, icke-vinst-drivande,
   eller på en statlig myndighet så finns det flera sätt som du kan stödja oss
   med era resurser.
</p>
 
<ul>
  <li>Exempelvis kan din organisation helt enkelt <a
      href="$(HOME)/donations">donera</a> pengar eller hårdvara.</li>
  <li>Kanske skulle du vilja <a
      href="https://www.debconf.org/sponsors/">sponsra</a> våra konferenser.</li>
  <li>Din organisation kunde tillhandahålla <a
      href="https://wiki.debian.org/MemberBenefits">produkter eller tjänster
      till Debians bidragslämnare</a>.</li>
  <li>Vi söker även efter <a
      href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">gratis hosting</a>. </li>
  <li>Självklart, att sätta upp speglingar för vår <a href="https://www.debian.org/mirror/ftpmirror">mjukvara</a>, <a
      href="https://www.debian.org/CD/mirroring/">installationsmedia</a>,
      eller <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">konferensvideos</a> uppskattas också stort.</li>
  <li>Kanske du skulle överväga att sälja <a href="https://www.debian.org/events/merchandise">Debianprylar</a>,
      <a href="https://www.debian.org/CD/vendors/">installationsmedia</a>, eller <a href="https://www.debian.org/distrib/pre-installed">förinstallerade system</a>.</li>
  <li>Om din organisation erbjuder <a
      href="https://www.debian.org/consultants/">konsulting</a> eller <a
      href="https://wiki.debian.org/DebianHosting">hosting</a>, vänligen
      berätta det för oss.</li>
</ul>


<p>
Vi är också intresserade av att forma <a href="https://www.debian.org/partners/">partnerskap</a>. Om du kan
marknadsföra Debian genom att <a href="https://www.debian.org/users/">tillhandahålla
en rekommendation</a>, köra det på din organisations servrar eller
skrivbordsmaskiner, eller till och med uppmuntra personalen att delta i vårt
projekt, så är det fantastiskt. Kanske du överväger att utbilda inom
operativsystemet Debian samt gemenskapen, och styra ditt team till att
bidra under arbetstid eller skicka dom på ett av våra <a
href="$(HOME)/events/">evenemang</a>.</p>


