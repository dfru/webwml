#use wml::debian::translation-check translation="ed1b2e9d8349f3e0333668208c8830371bdc453c"

<define-tag pagetitle>تحديث دبيان 10: الإصدار 10.10</define-tag>
<define-tag release_date>2021-06-19</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.10</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
يسعد مشروع دبيان الإعلان عن التحديث العاشر لتوزيعته المستقرة دبيان <release> (الاسم الرمزي <q><codename></q>).
بالإضافة إلى تسوية بعض المشكلات الحرجة يصلح هذا التحديث بالأساس مشاكلات الأمان. تنبيهات الأمان أعلنت بشكل منفصِل وفقط مُشار إليها في هذا الإعلان.
</p>

<p>
يرجى ملاحظة أن هذا التحديث لا يشكّل إصدار جديد لدبيان <release> بل فقط تحديثات لبعض الحزم المضمّنة
وبالتالي ليس بالضرورة رمي الوسائط القديمة للإصدار <q><codename></q>، يمكن تحديث الحزم باستخدام مرآة دبيان محدّثة.
</p>

<p>
الذين يثبّتون التحديثات من security.debian.org باستمرار لن يكون عليهم تحديث العديد من الحزم،
أغلب التحديثات مضمّنة في هذا التحديث.
</p>

<p>
صور جديدة لأقراص التثبيت ستكون متوفرة في موضعها المعتاد.
</p>

<p>
يمكن الترقية من  تثبيت آنيّ إلى هذه المراجعة بتوجيه نظام إدارة الحزم إلى إحدى مرايا HTTP الخاصة بدبيان.
قائمة شاملة لمرايا دبيان على المسار:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>إصلاح العديد من العلاّت</h2>

<p>أضاف هذا التحديث للإصدار المستقر بعض الإصلاحات المهمة للحزم التالية:</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السبب</th></tr>
<correction apt "Accept suite name changes for repositories by default (e.g. stable -&gt; oldstable)">
<correction awstats "Fix remote file access issues [CVE-2020-29600 CVE-2020-35176]">
<correction base-files "Update /etc/debian_version for the 10.10 point release">
<correction berusky2 "Fix segfault at startup">
<correction clamav "New upstream stable release; fix denial of security issue [CVE-2021-1405]">
<correction clevis "Fix support for TPMs that only support SHA256">
<correction connman "dnsproxy: Check the length of buffers before memcpy [CVE-2021-33833]">
<correction crmsh "Fix code execution issue [CVE-2020-35459]">
<correction debian-installer "Use 4.19.0-17 Linux kernel ABI">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction dnspython "XFR: do not attempt to compare to a non-existent <q>expiration</q> value">
<correction dput-ng "Fix crash in the sftp uploader in case of EACCES from the server; update codenames; make <q>dcut dm</q> work for non-uploading DDs; fix a TypeError in http upload exception handling; don't try and construct uploader email from system hostname in .dak-commands files">
<correction eterm "Fix code execution issue [CVE-2021-33477]">
<correction exactimage "Fix build with C++11 and OpenEXR 2.5.x">
<correction fig2dev "Fix buffer overflow [CVE-2021-3561]; several output fixes; rebuild testsuite during build and in autopkgtest">
<correction fluidsynth "Fix use-after-free issue [CVE-2021-28421]">
<correction freediameter "Fix denial of service issue [CVE-2020-6098]">
<correction fwupd "Fix generation of the vendor SBAT string; stop using dpkg-dev in fwupd.preinst; new upstream stable version">
<correction fwupd-amd64-signed "Sync with fwupd">
<correction fwupd-arm64-signed "Sync with fwupd">
<correction fwupd-armhf-signed "Sync with fwupd">
<correction fwupd-i386-signed "Sync with fwupd">
<correction fwupdate "Improve SBAT support">
<correction fwupdate-amd64-signed "Sync with fwupdate">
<correction fwupdate-arm64-signed "Sync with fwupdate">
<correction fwupdate-armhf-signed "Sync with fwupdate">
<correction fwupdate-i386-signed "Sync with fwupdate">
<correction glib2.0 "Fix several integer overflow issues [CVE-2021-27218 CVE-2021-27219]; fix a symlink attack affecting file-roller [CVE-2021-28153]">
<correction gnutls28 "Fix null-pointer dereference issue [CVE-2020-24659]; add several improvements to memory reallocation">
<correction golang-github-docker-docker-credential-helpers "Fix double free issue [CVE-2019-1020014]">
<correction htmldoc "Fix buffer overflow issues [CVE-2019-19630 CVE-2021-20308]">
<correction ipmitool "Fix buffer overflow issues [CVE-2020-5208]">
<correction ircii "Fix denial of service issue [CVE-2021-29376]">
<correction isc-dhcp "Fix buffer overrun issue [CVE-2021-25217]">
<correction isync "Reject <q>funny</q> mailbox names from IMAP LIST/LSUB [CVE-2021-20247]; fix handling of unexpected APPENDUID response code [CVE-2021-3578]">
<correction jackson-databind "Fix external entity expansion issue [CVE-2020-25649] and several serialization-related issues [CVE-2020-24616 CVE-2020-24750 CVE-2020-35490 CVE-2020-35491 CVE-2020-35728 CVE-2020-36179 CVE-2020-36180 CVE-2020-36181 CVE-2020-36182 CVE-2020-36183 CVE-2020-36184 CVE-2020-36185 CVE-2020-36186 CVE-2020-36187 CVE-2020-36188 CVE-2020-36189 CVE-2021-20190]">
<correction klibc "malloc: Set errno on failure; fix several overflow issues [CVE-2021-31873 CVE-2021-31870 CVE-2021-31872]; cpio: Fix possible crash on 64-bit systems [CVE-2021-31871]; {set,long}jmp [s390x]: save/restore the correct FPU registers">
<correction libbusiness-us-usps-webtools-perl "Update to new US-USPS API">
<correction libgcrypt20 "Fix weak ElGamal encryption with keys not generated by GnuPG/libgcrypt [CVE-2021-40528]">
<correction libgetdata "Fix use after free issue [CVE-2021-20204]">
<correction libmateweather "Adapt to renaming of America/Godthab to America/Nuuk in tzdata">
<correction libxml2 "Fix out-of-bounds read in xmllint [CVE-2020-24977]; fix use-after-free issues in xmllint [CVE-2021-3516 CVE-2021-3518]; validate UTF8 in xmlEncodeEntities [CVE-2021-3517]; propagate error in xmlParseElementChildrenContentDeclPriv; fix exponential entity expansion attack [CVE-2021-3541]">
<correction liferea "Fix compatibility with webkit2gtk &gt;= 2.32">
<correction linux "New upstream stable release; increase ABI to 17; [rt] Update to 4.19.193-rt81">
<correction linux-latest "Update to 4.19.0-17 ABI">
<correction linux-signed-amd64 "New upstream stable release; increase ABI to 17; [rt] Update to 4.19.193-rt81">
<correction linux-signed-arm64 "New upstream stable release; increase ABI to 17; [rt] Update to 4.19.193-rt81">
<correction linux-signed-i386 "New upstream stable release; increase ABI to 17; [rt] Update to 4.19.193-rt81">
<correction mariadb-10.3 "New upstream release; security fixes [CVE-2021-2154 CVE-2021-2166 CVE-2021-27928]; fix Innotop support; ship caching_sha2_password.so">
<correction mqtt-client "Fix denial of service issue [CVE-2019-0222]">
<correction mumble "Fix remote code execution issue [CVE-2021-27229]">
<correction mupdf "Fix use-after-free issue [CVE-2020-16600] and double free issue [CVE-2021-3407]">
<correction nmap "Update included MAC prefix list">
<correction node-glob-parent "Fix regular expression denial of service issue [CVE-2020-28469]">
<correction node-handlebars "Fix code execution issues [CVE-2019-20920 CVE-2021-23369]">
<correction node-hosted-git-info "Fix regular expression denial of service issue [CVE-2021-23362]">
<correction node-redis "Fix regular expression denial of service issue [CVE-2021-29469]">
<correction node-ws "Fix regular expression-related denial of service issue [CVE-2021-32640]">
<correction nvidia-graphics-drivers "Fix improper access control vulnerability [CVE-2021-1076]">
<correction nvidia-graphics-drivers-legacy-390xx "Fix improper access control vulnerability [CVE-2021-1076]; fix installation failure on Linux 5.11 release candidates">
<correction opendmarc "Fix heap overflow issue [CVE-2020-12460]">
<correction openvpn "Fix <q>illegal client float</q> issue [CVE-2020-11810]; ensure key state is authenticated before sending push reply [CVE-2020-15078]; increase listen() backlog queue to 32">
<correction php-horde-text-filter "Fix cross-site scripting issue [CVE-2021-26929]">
<correction plinth "Use session to verify first boot welcome step">
<correction ruby-websocket-extensions "Fix denial of service issue [CVE-2020-7663]">
<correction rust-rustyline "Fix build with newer rustc">
<correction rxvt-unicode "Disable ESC G Q escape sequence [CVE-2021-33477]">
<correction sabnzbdplus "Fix code execution vulnerability [CVE-2020-13124]">
<correction scrollz "Fix denial of service issue [CVE-2021-29376]">
<correction shim "New upstream release; add SBAT support; fix i386 binary relocations; don't call QueryVariableInfo() on EFI 1.10 machines (e.g. older Intel Macs); fix handling of ignore_db and user_insecure_mode; add maintainer scripts to the template packages to manage installing and removing fbXXX.efi and mmXXX.efi when we install/remove the shim-helpers-$arch-signed packages; exit cleanly if installed on a non-EFI system; don't fail if debconf calls return errors">
<correction shim-helpers-amd64-signed "Sync with shim">
<correction shim-helpers-arm64-signed "Sync with shim">
<correction shim-helpers-i386-signed "Sync with shim">
<correction shim-signed "Update for new shim; multiple bugfixes in postinst and postrm handling; provide unsigned binaries for arm64 (see NEWS.Debian); exit cleanly if installed on a non-EFI system; don't fail if debconf calls return errors; fix documentation links; build against shim-unsigned 15.4-5~deb10u1; add explicit dependency from shim-signed to shim-signed-common">
<correction speedtest-cli "Handle case where <q>ignoreids</q> is empty or contains empty ids">
<correction tnef "Fix buffer over-read issue [CVE-2019-18849]">
<correction uim "libuim-data: Copy <q>Breaks</q> from uim-data, fixing some upgrade scenarios">
<correction user-mode-linux "Rebuild against Linux kernel 4.19.194-1">
<correction velocity "Fix potential arbitrary code execution issue [CVE-2020-13936]">
<correction wml "Fix regression in Unicode handling">
<correction xfce4-weather-plugin "Move to version 2.0 met.no API">
</table>


<h2>تحديثات الأمان</h2>


<p>
أضافت هذه المراجعة تحديثات الأمان التالية للإصدار المستقر.
سبق لفريق الأمان نشر تنبيه لكل تحديث:
</p>

<table border=0>
<tr><th>معرَّف التنبيه</th>  <th>الحزمة</th></tr>
<dsa 2021 4848 golang-1.11>
<dsa 2021 4865 docker.io>
<dsa 2021 4873 squid>
<dsa 2021 4874 firefox-esr>
<dsa 2021 4875 openssl>
<dsa 2021 4877 webkit2gtk>
<dsa 2021 4878 pygments>
<dsa 2021 4879 spamassassin>
<dsa 2021 4880 lxml>
<dsa 2021 4881 curl>
<dsa 2021 4882 openjpeg2>
<dsa 2021 4883 underscore>
<dsa 2021 4884 ldb>
<dsa 2021 4885 netty>
<dsa 2021 4886 chromium>
<dsa 2021 4887 lib3mf>
<dsa 2021 4888 xen>
<dsa 2021 4889 mediawiki>
<dsa 2021 4890 ruby-kramdown>
<dsa 2021 4891 tomcat9>
<dsa 2021 4892 python-bleach>
<dsa 2021 4893 xorg-server>
<dsa 2021 4894 php-pear>
<dsa 2021 4895 firefox-esr>
<dsa 2021 4896 wordpress>
<dsa 2021 4898 wpa>
<dsa 2021 4899 openjdk-11-jre-dcevm>
<dsa 2021 4899 openjdk-11>
<dsa 2021 4900 gst-plugins-good1.0>
<dsa 2021 4901 gst-libav1.0>
<dsa 2021 4902 gst-plugins-bad1.0>
<dsa 2021 4903 gst-plugins-base1.0>
<dsa 2021 4904 gst-plugins-ugly1.0>
<dsa 2021 4905 shibboleth-sp>
<dsa 2021 4907 composer>
<dsa 2021 4908 libhibernate3-java>
<dsa 2021 4909 bind9>
<dsa 2021 4910 libimage-exiftool-perl>
<dsa 2021 4912 exim4>
<dsa 2021 4913 hivex>
<dsa 2021 4914 graphviz>
<dsa 2021 4915 postgresql-11>
<dsa 2021 4916 prosody>
<dsa 2021 4918 ruby-rack-cors>
<dsa 2021 4919 lz4>
<dsa 2021 4920 libx11>
<dsa 2021 4921 nginx>
<dsa 2021 4922 hyperkitty>
<dsa 2021 4923 webkit2gtk>
<dsa 2021 4924 squid>
<dsa 2021 4925 firefox-esr>
<dsa 2021 4926 lasso>
<dsa 2021 4928 htmldoc>
<dsa 2021 4929 rails>
<dsa 2021 4930 libwebp>
</table>


<h2>الحزم المزالة</h2>

<p>
الحزم التالية أزيلت لأسباب خارجة عن سيطرتنا:
</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السبب</th></tr>
<correction sogo-connector "Incompatible with current Thunderbird versions">

</table>

<h2>مُثبِّت دبيان</h2>
<p>
حدِّث المُثبِّت ليتضمن الإصلاحات المندرجة في هذا الإصدار المستقر.
</p>

<h2>المسارات</h2>

<p>
القائمة الكاملة للحزم المغيّرة في هذه المراجعة:
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>التوزيعة المستقرة الحالية:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>التحديثات المقترحة للتوزيعة المستقرة:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>معلومات حول التوزيعة المستقرة (ملاحظات الإصدار والأخطاء إلخ):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>معلومات وإعلانات الأمان:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>حول دبيان</h2>

<p>
مشروع دبيان هو اتحاد لمطوري البرمجيات الحرة تطوعوا بالوقت والمجهود لإنتاج نظام تشعيل دبيان حر بالكامل.
</p>

<h2>معلومات الاتصال</h2>

<p>
لمزيد من المعلومات يرجى زيارة موقع دبيان
<a href="$(HOME)/">https://www.debian.org/</a>
أو إرسال بريد إلكتروني إلى &lt;press@debian.org&gt;
أو الاتصال بفريق إصدار المستقرة على
&lt;debian-release@lists.debian.org&gt;.
</p>
