#use wml::debian::template title="사용자 지원" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="c7a64739b495b3bd084b972d820ef78fc30a3f8a" maintainer="Sebul"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#irc">IRC (실시간 지원)</a></li>
  <li><a href="#mail_lists">메일링 리스트</a></li>
  <li><a href="#usenet">유즈넷 뉴스그룹</a></li>
  <li><a href="#forums">데비안 사용자 포럼</a></li>
  <li><a href="#maintainers">패키지 관리자에게 연락</a></li>
  <li><a href="#bts">버그 추적 시스템</a></li>
  <li><a href="#release">알려진 문제</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 데비안 지원은 봉사자 그룹이 합니다.
이 커뮤니티 기반 지원이 여러분의 요구를 만족시키지 않고 우리 <a href="doc/">문서</a>에서 답을 못 찾으면, 
<a href="consultants/">컨설턴트</a>를 고용해서 질문에 답하거나
데비안 시스템에 추가 기능을 유지 또는 추가할 수 있습니다.</p>
</aside>

<h2><a id="irc">IRC (실시간 지원)</a></h2>

<p>
<a href="http://www.irchelp.org/">IRC</a>는 
전 세계 사람들과 실시간으로 채팅할 수 있는 좋은 방법입니다. 
인스턴트 메시징을 위한 텍스트 기반 채팅 시스템입니다. 
IRC에서 채팅방(채널이라 불림)에 들어가거나 
개인 메시지를 통해 개인과 직접 채팅할 수 있습니다.
</p>

<p>
데비안 전용 IRC 채널은 <a href="https://www.oftc.net/">OFTC</a>에 있습니다. 
데비안 채널 전체 목록은
<a href="https://wiki.debian.org/IRC">위키</a> 참조. 
 
<a href="https://netsplit.de/channels/index.en.php?net=oftc&chat=debian">검색 엔진</a>으로 데비안 관련 채널을 확인할 수 있습니다.
</p>

<h3>IRC 클라이언트</h3>

<p>
IRC 네트워크에 연결하려면 선호하는 웹 브라우저에서 OFTC의 <a 
href="https://www.oftc.net/WebChat/">WebChat</a>을 사용하거나 
컴퓨터에 클라이언트를 설치할 수 있습니다. 
세상에는 다양한 클라이언트가 있으며 일부는 그래픽 인터페이스가 있고 일부는 콘솔용입니다. 
일부 유명한 IRC 클라이언트는 데비안용으로 패키징되었습니다. 예를 들면:
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/irssi">irssi</a> (텍스트 모드)</li>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a> (텍스트 모드)</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat (GTK)</a></li>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a> (KDE)</li>
</ul> 

<p>
데비안 위키는 데비안 패키지로 사용할 수 있는 더 포괄적인 <a href="https://wiki.debian.org/IrcClients">IRC 클라이언트 목록</a>을 제공합니다.
</p>

<h3>네트워크에 연결</h3>

<p>
클라이언트를 설치했으면 서버에 연결하도록 클라이언트에 말해야 합니다. 
대부분의 클라이언트에서 수행하려면 칠 것은:
</p>

<pre>
/server irc.debian.org
</pre>

<p>호스트명 irc.debian.org 은 irc.oftc.net 의 별칭. 어떤 클라이언트(예를 들어 irssi)는 대신 아래 입력:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<h3>채널 가입</h3>

<p>
연결되면, <code>#debian</code> 채널에 참여하기 위해 칠 명령:</p>

<pre>
/join #debian
</pre>

<p>HexChat 또는 Konversation과 같은 그래픽 클라이언트에는 서버에 연결하고 채널에 참여하기 위한 버튼이나 메뉴 항목이 종종 있습니다.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/DebianIRC">IRC FAQ</a></button></p>

<h2><a id="mail_lists">메일링 리스트</a></h2>

<p>
천 명 이상의 활동적인 <a href="intro/people.en.html#devcont">개발자</a>가
전세계에 퍼져서 자기 시간대에서 남는 시간에 데비안으로 일합니다. 
그래서 우리는 주로 이메일을 통해 소통합니다. 
비슷하게, 데비안 개발자와 사용자 사이의 대부분 대화는 서로 다른
<a href="MailingLists/">메일링 리스트</a>에서 생깁니다:
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
# Added korean.
<ul>
  <li>영어로 사용자 지원을 바라면, <a href="https://lists.debian.org/debian-user/">debian-user</a> 메일링 리스트에 연락하세요.</li>
  <li>다른 언어로 사용자 지원을 바라면, 다른 사용자 메일링 리스트 <a href="https://lists.debian.org/users.html">인덱스</a> 보세요.
  한국어 메일링 리스트 <a href='https://lists.debian.org/debian-l10n-korean/'>https://lists.debian.org/debian-l10n-korean</a></li>
</ul>

<p>
구독하지 않아도 <a href="https://lists.debian.org/">메일링 리스트 아카이브</a>를 훑거나 <a href="https://lists.debian.org/search.html">검색</a>할 수 있습니다.
</p>

<p>
물론 데비안 전용이 아닌 리눅스 생태계의 일부 측면에 전념하는 다른 메일링 리스트가 많이 있습니다. 
원하는 검색 엔진을 사용하여 목적에 가장 적합한 목록을 찾으십시오.
</p>

<h2><a id="usenet">유즈넷 뉴스그룹</a></h2>

<p>
많은 <a href="#mail_lists">메일링 리스트</a>를 뉴스그룹처럼, 
 <kbd>linux.debian.*</kbd> 계층에서 찾아볼 수 있습니다.
</p>

<h2><a id="forums">데비안 사용자 포럼</h2>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
# Added korean.

<p>
<a href="https://forums.debian.net">데비안 사용자 포럼</a>은 
은 수천 명의 다른 사용자가 데비안 관련 주제에 대해 토론하고, 질문하고, 
답변을 통해 서로 돕는 웹 포털입니다. 대한민국에는 <a 
href='https://debianusers.or.kr/'>https://debianusers.or.kr</a>이 있습니다.
등록하지 않고도 모든 게시판을 읽을 수 있습니다. 
토론에 참여하고 자신의 게시물을 게시하려면 등록하고 로그인하십시오.
</p>

<h2><a id="maintainers">패키지 관리자에게 연락</a></h2>

<p>
기본적으로, 데비안 패키지와 연락하는 두 가지 방법이 있습니다:
</p>

<ul>
  <li>
버그 보고 하려면,
단순히 <a href="bts">버그 보고</a> 하세요.
관리자는 버그 보고 사본을 자동으로 받습니다.
</li>
  <li>
단순히 관리자에게 메일 보내려면, 각 패키지에 설정된 특별한 메일 별칭을 쓰세요:<br>
      &lt;<em>package_name</em>&gt;@packages.debian.org</li>
</ul>

<h2><a id="bts">버그 추적 시스템</a></h2>

<p>
데비안 배포판에는 자체 사용자와 개발자가 보고한 버그가 들어있는 <a href="Bugs/">버그 추적기</a>가 있습니다.
모든 버그는 고유 번호가 있으며 해결된 것으로 표시될 때까지 파일에 보관됩니다.
버그 보고하는 두 가지 방법:
</p>

<ul>
  <li>
권장하는 방법은 데비안 패키지 <em>reportbug</em>을 쓰는 겁니다.
</li>
  <li>
또는, 이 <a href="Bugs/Reporting">페이지</a>에 이메일 보낼 수 있습니다.
</li>
</ul>

<h2><a id="release">알려진 문제</a></h2>

<p>
현재 안정 배포판의 한계 및 심각한 문제는 (있다면) <a href="releases/stable/">릴리스 페이지</a>에 설명이 있습니다.</p>

<p><a href="releases/stable/releasenotes">릴리스 노트</a> 및 <a href="releases/stable/errata">정오표</a>에 특별한 주의를 하세요.</p>

