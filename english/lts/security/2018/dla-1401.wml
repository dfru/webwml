<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Various security issues were discovered in Graphicsmagick, a collection
of image processing tools. Heap-based buffer overflows or overreads may
lead to a denial of service or disclosure of in-memory information or
other unspecified impact by processing a malformed image file.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.20-3+deb8u3.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1401.data"
# $Id: $
