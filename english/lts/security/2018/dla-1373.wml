<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in PHP (recursive acronym for PHP:
Hypertext Preprocessor), a widely-used open source general-purpose
scripting language that is especially suited for web development and can
be embedded into HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10545">CVE-2018-10545</a>

  <p>Dumpable FPM child processes allow bypassing opcache access
  controls because fpm_unix.c makes a PR_SET_DUMPABLE prctl call,
  allowing one user (in a multiuser environment) to obtain sensitive
  information from the process memory of a second user's PHP
  applications by running gcore on the PID of the PHP-FPM worker
  process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10547">CVE-2018-10547</a>

  <p>There is a reflected XSS on the PHAR 403 and 404 error pages via
  request data of a request for a .phar file. NOTE: this vulnerability
  exists because of an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-5712">CVE-2018-5712</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10548">CVE-2018-10548</a>

  <p>ext/ldap/ldap.c allows remote LDAP servers to cause a denial of
  service (NULL pointer dereference and application crash) because of
  mishandling of the ldap_get_dn return value.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.4.45-0+deb7u14.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1373.data"
# $Id: $
