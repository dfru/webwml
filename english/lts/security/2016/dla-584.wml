<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>John Lightsey and Todd Rinaldo reported that the opportunistic loading
of optional modules can make many programs unintentionally load code
from the current working directory (which might be changed to another
directory without the user realising) and potentially leading to
privilege escalation, as demonstrated in Debian with certain
combinations of installed packages.</p>

<p>The problem relates to Perl loading modules from the includes directory
array ("@INC") in which the last element is the current directory (".").
That means that, when <q>perl</q> wants to load a module (during first
compilation or during lazy loading of a module in run time), perl will
look for the module in the current directory at the end, since '.' is
the last include directory in its array of include directories to seek.
The issue is with requiring libraries that are in "." but are not
otherwise installed.</p>

<p>With this update the Sys::Syslog Perl module is updated to not load
modules from current directory.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.29-1+deb7u1.</p>

<p>We recommend that you upgrade your libsys-syslog-perl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-584.data"
# $Id: $
