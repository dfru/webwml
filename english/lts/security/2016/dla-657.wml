<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that libarchive mishandled hardlink archive entries of
non-zero data size, possibly allowing remote attackers to to write to
arbitrary files via especially crafted archives.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.0.4-3+wheezy4.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-657.data"
# $Id: $
