<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in qemu-kvm, a full
virtualization solution on x86 hardware based on Quick
Emulator(Qemu). The Common Vulnerabilities and Exposures project
identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7909">CVE-2016-7909</a>

  <p>Quick Emulator(Qemu) built with the AMD PC-Net II emulator support is
  vulnerable to an infinite loop issue. It could occur while receiving
  packets via pcnet_receive().</p>

  <p>A privileged user/process inside guest could use this issue to crash
  the Qemu process on the host leading to DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8909">CVE-2016-8909</a>

  <p>Quick Emulator(Qemu) built with the Intel HDA controller emulation support
  is vulnerable to an infinite loop issue. It could occur while processing the
  DMA buffer stream while doing data transfer in <q>intel_hda_xfer</q>.</p>

  <p>A privileged user inside guest could use this flaw to consume excessive CPU
  cycles on the host, resulting in DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8910">CVE-2016-8910</a>

  <p>Quick Emulator(Qemu) built with the RTL8139 ethernet controller emulation
  support is vulnerable to an infinite loop issue. It could occur while
  transmitting packets in C+ mode of operation.</p>

  <p>A privileged user inside guest could use this flaw to consume
  excessive CPU cycles on the host, resulting in DoS situation.</p></li>

</ul>

<p>Further issues fixed where the CVE requests are pending:</p>

<ul>

<li>Quick Emulator(Qemu) built with the i8255x (PRO100) NIC emulation
  support is vulnerable to a memory leakage issue. It could occur while
  unplugging the device, and doing so repeatedly would result in leaking
  host memory affecting, other services on the host.

  <p>A privileged user inside guest could use this flaw to cause a DoS on the host
  and/or potentially crash the Qemu process on the host.</p></li>

<li>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via
  Plan 9 File System(9pfs) support, is vulnerable to a several memory
  leakage issues.

  <p>A privileged user inside guest could use this flaw to leak the host
  memory bytes resulting in DoS for other services.</p></li>

<li>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via
  Plan 9 File System(9pfs) support, is vulnerable to an integer overflow
  issue. It could occur by accessing xattributes values.

  <p>A privileged user inside guest could use this flaw to crash the Qemu
  process instance resulting in DoS.</p></li>

<li>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via
  Plan 9 File System(9pfs) support, is vulnerable to memory leakage
  issue. It could occur while creating extended attribute via
  <q>Txattrcreate</q> message.

  <p>A privileged user inside guest could use this flaw to leak host
  memory, thus affecting other services on the host and/or potentially
  crash the Qemu process on the host.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u18.</p>

<p>We recommend that you upgrade your qemu-kvm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-689.data"
# $Id: $
