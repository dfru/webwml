<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Viktor Szakats reported that libcurl, an URL transfer library, does
not strip off user credentials from the URL when automatically
populating the Referer HTTP request header field in outgoing HTTP
requests. Sensitive authentication data may leak to the server that is
the target of the second HTTP request.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
7.52.1-5+deb9u14.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2664.data"
# $Id: $
