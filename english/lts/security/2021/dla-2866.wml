<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Access to IMAP mailboxes through running imapd over rsh and ssh is
now disabled by default in uw-imap, the University of Washington IMAP
Toolkit. Code using the library can enable it with tcp_parameters()
after making sure that the IMAP server name is sanitized.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
8:2007f~dfsg-5+deb9u1.</p>

<p>We recommend that you upgrade your uw-imap packages.</p>

<p>For the detailed security status of uw-imap please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/uw-imap">https://security-tracker.debian.org/tracker/uw-imap</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2866.data"
# $Id: $
