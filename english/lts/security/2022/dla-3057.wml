<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in request-tracker4, a extensible
ticket/issue tracking system. Sensitive information could have been revealed by
way of a timing attack on the authentication system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38562">CVE-2021-38562</a>

    <p>Best Practical Request Tracker (RT) 4.2 before 4.2.17, 4.4 before 4.4.5,
    and 5.0 before 5.0.2 allows sensitive information disclosure via a timing
    attack against lib/RT/REST2/Middleware/Auth.pm.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
4.4.1-3+deb9u4.</p>

<p>We recommend that you upgrade your request-tracker4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3057.data"
# $Id: $
