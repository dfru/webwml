<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the Commandline class in maven-shared-utils, a
collection of various utility classes for the Maven build system, can emit
double-quoted strings without proper escaping, allowing shell injection
attacks.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.0.0-1+deb9u1.</p>

<p>We recommend that you upgrade your maven-shared-utils packages.</p>

<p>For the detailed security status of maven-shared-utils please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/maven-shared-utils">https://security-tracker.debian.org/tracker/maven-shared-utils</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3059.data"
# $Id: $
