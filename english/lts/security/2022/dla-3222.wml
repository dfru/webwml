<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>ranjit-git discovered an information leak vulnerability in node-fetch, a
Node.js module exposing a window.fetch compatible API on Node.js
runtime: the module was not honoring the same-origin-policy and upon
following a redirect would leak cookies to the the target URL.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.7.3-1+deb10u1.</p>

<p>We recommend that you upgrade your node-fetch packages.</p>

<p>For the detailed security status of node-fetch please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-fetch">https://security-tracker.debian.org/tracker/node-fetch</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3222.data"
# $Id: $
