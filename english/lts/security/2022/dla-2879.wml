<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in Ghostscript, the GPL
PostScript/PDF interpreter, which could result in denial of service and
potentially the execution of arbitrary code if malformed document files
are processed.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
9.26a~dfsg-0+deb9u8.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>For the detailed security status of ghostscript please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ghostscript">https://security-tracker.debian.org/tracker/ghostscript</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2879.data"
# $Id: $
