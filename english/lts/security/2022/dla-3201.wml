<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Yuchen Zeng and Eduardo Vela discovered a buffer overflow in NTFS-3G, a
read-write NTFS driver for FUSE, due to incorrect validation of some of
the NTFS metadata. A local user can take advantage of this flaw for local
root privilege escalation.</p>


<p>For Debian 10 Buster, this problem has been fixed in version
1:2017.3.23AR.3-3+deb10u3.</p>

<p>We recommend that you upgrade your ntfs-3g packages.</p>

<p>For the detailed security status of ntfs-3g please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ntfs-3g">https://security-tracker.debian.org/tracker/ntfs-3g</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3201.data"
# $Id: $
