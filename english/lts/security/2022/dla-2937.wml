<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Three issues have been discovered in gif2apng: tool for converting animated GIF images to APNG format.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45909">CVE-2021-45909</a>

    <p>heap-based buffer overflow vulnerability in the DecodeLZW function.
    It allows an attacker to write a large amount of arbitrary data outside the
    boundaries of a buffer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45910">CVE-2021-45910</a>:

    <p>heap-based buffer overflow within the main function. It allows an attacker
    to write data outside of the allocated buffer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45911">CVE-2021-45911</a>:

    <p>heap based buffer overflow in processing of delays in the main function.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.9+srconly-2+deb9u2.</p>

<p>We recommend that you upgrade your gif2apng packages.</p>

<p>For the detailed security status of gif2apng please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gif2apng">https://security-tracker.debian.org/tracker/gif2apng</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2937.data"
# $Id: $
