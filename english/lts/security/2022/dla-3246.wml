<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in Hawk, an HTTP authentication
scheme. Hawk used a regular expression to parse `Host` HTTP headers which was
subject to regular expression DoS attack. Each added character in the
attacker's input increased the computation time exponentially.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29167">CVE-2022-29167</a>

    <p>Hawk is an HTTP authentication scheme providing mechanisms for making
    authenticated HTTP requests with partial cryptographic verification of the
    request and response, covering the HTTP method, request URI, host, and
    optionally the request payload. Hawk used a regular expression to parse
    `Host` HTTP header (`Hawk.utils.parseHost()`), which was subject to regular
    expression DoS attack - meaning each added character in the attacker's
    input increases the computation time exponentially. `parseHost()` was
    patched in `9.0.1` to use built-in `URL` class to parse hostname instead.
    `Hawk.authenticate()` accepts `options` argument. If that contains `host`
    and `port`, those would be used instead of a call to
    `utils.parseHost()`.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
6.0.1+dfsg-1+deb10u1.</p>

<p>We recommend that you upgrade your node-hawk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3246.data"
# $Id: $
