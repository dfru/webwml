<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7482">CVE-2017-7482</a>

    <p>Shi Lei discovered that RxRPC Kerberos 5 ticket handling code does
    not properly verify metadata, leading to information disclosure,
    denial of service or potentially execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7542">CVE-2017-7542</a>

    <p>An integer overflow vulnerability in the ip6_find_1stfragopt()
    function was found allowing a local attacker with privileges to open
    raw sockets to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7889">CVE-2017-7889</a>

    <p>Tommi Rantala and Brad Spengler reported that the mm subsystem does
    not properly enforce the CONFIG_STRICT_DEVMEM protection mechanism,
    allowing a local attacker with access to /dev/mem to obtain
    sensitive information or potentially execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10661">CVE-2017-10661</a>

    <p>Dmitry Vyukov of Google reported that the timerfd facility does
    not properly handle certain concurrent operations on a single file
    descriptor.  This allows a local attacker to cause a denial of
    service or potentially to execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10911">CVE-2017-10911</a> / XSA-216

    <p>Anthony Perard of Citrix discovered an information leak flaw in Xen
    blkif response handling, allowing a malicious unprivileged guest to
    obtain sensitive information from the host or other guests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11176">CVE-2017-11176</a>

    <p>It was discovered that the mq_notify() function does not set the
    sock pointer to NULL upon entry into the retry logic. An attacker
    can take advantage of this flaw during a userspace close of a
    Netlink socket to cause a denial of service or potentially cause
    other impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11600">CVE-2017-11600</a>

    <p>bo Zhang reported that the xfrm subsystem does not properly
    validate one of the parameters to a netlink message. Local users
    with the CAP_NET_ADMIN capability can use this to cause a denial
    of service or potentially to execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12134">CVE-2017-12134</a> / #866511 / XSA-229

    <p>Jan H. Schönherr of Amazon discovered that when Linux is running
    in a Xen PV domain on an x86 system, it may incorrectly merge
    block I/O requests.  A buggy or malicious guest may trigger this
    bug in dom0 or a PV driver domain, causing a denial of service or
    potentially execution of arbitrary code.</p>

    <p>This issue can be mitigated by disabling merges on the underlying
    back-end block devices, e.g.:
        echo 2 > /sys/block/nvme0n1/queue/nomerges</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12153">CVE-2017-12153</a>

    <p>bo Zhang reported that the cfg80211 (wifi) subsystem does not
    properly validate the parameters to a netlink message. Local users
    with the CAP_NET_ADMIN capability on a system with a wifi device
    can use this to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12154">CVE-2017-12154</a>

    <p>Jim Mattson of Google reported that the KVM implementation for
    Intel x86 processors did not correctly handle certain nested
    hypervisor configurations. A malicious guest (or nested guest in a
    suitable L1 hypervisor) could use this for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14106">CVE-2017-14106</a>

    <p>Andrey Konovalov of Google reported that a specific sequence of
    operations on a TCP socket could lead to division by zero.  A
    local user could use this for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14140">CVE-2017-14140</a>

    <p>Otto Ebeling reported that the move_pages() system call permitted
    users to discover the memory layout of a set-UID process running
    under their real user-ID. This made it easier for local users to
    exploit vulnerabilities in programs installed with the set-UID
    permission bit set.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14156">CVE-2017-14156</a>

    <p><q>sohu0106</q> reported an information leak in the atyfb video driver.
    A local user with access to a framebuffer device handled by this
    driver could use this to obtain sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14340">CVE-2017-14340</a>

    <p>Richard Wareing discovered that the XFS implementation allows the
    creation of files with the <q>realtime</q> flag on a filesystem with no
    realtime device, which can result in a crash (oops). A local user
    with access to an XFS filesystem that does not have a realtime
    device can use this for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14489">CVE-2017-14489</a>

    <p>ChunYu of Red Hat discovered that the iSCSI subsystem does not
    properly validate the length of a netlink message, leading to
    memory corruption. A local user with permission to manage iSCSI
    devices can use this for denial of service or possibly to
    execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000111">CVE-2017-1000111</a>

    <p>Andrey Konovalov of Google reported that a race condition in the
    raw packet (af_packet) feature. Local users with the CAP_NET_RAW
    capability can use this to cause a denial of service or possibly to
    execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000251">CVE-2017-1000251</a> / #875881

    <p>Armis Labs discovered that the Bluetooth subsystem does not
    properly validate L2CAP configuration responses, leading to a
    stack buffer overflow. This is one of several vulnerabilities
    dubbed <q>Blueborne</q>. A nearby attacker can use this to cause a
    denial of service or possibly to execute arbitrary code on a
    system with Bluetooth enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000363">CVE-2017-1000363</a>

    <p>Roee Hay reported that the lp driver does not properly bounds-check
    passed arguments. This has no security impact in Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000365">CVE-2017-1000365</a>

    <p>It was discovered that argument and environment pointers are not
    properly taken into account by the size restrictions on arguments
    and environmental strings passed through execve(). A local
    attacker can take advantage of this flaw in conjunction with other
    flaws to execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000380">CVE-2017-1000380</a>

    <p>Alexander Potapenko of Google reported a race condition in the ALSA
    (sound) timer driver, leading to an information leak. A local user
    with permission to access sound devices could use this to obtain
    sensitive information.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.93-1. This version also includes bug fixes from upstream versions
up to and including 3.2.93.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.16.43-2+deb8u4 or were fixed in an earlier version.</p>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
4.9.30-2+deb9u4 or were fixed in an earlier version.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1099.data"
# $Id: $
