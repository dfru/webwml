<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in kde4libs, the core libraries
for all KDE 4 applications. The Common Vulnerabilities and Exposures
project identifies the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6410">CVE-2017-6410</a>

    <p>Itzik Kotler, Yonatan Fridburg and Amit Klein of Safebreach Labs
    reported that URLs are not sanitized before passing them to
    FindProxyForURL, potentially allowing a remote attacker to obtain
    sensitive information via a crafted PAC file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8422">CVE-2017-8422</a>

    <p>Sebastian Krahmer from SUSE discovered that the KAuth framework
    contains a logic flaw in which the service invoking dbus is not
    properly checked. This flaw allows spoofing the identity of the
    caller and gaining root privileges from an unprivileged account.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-2074">CVE-2013-2074</a>

    <p>It was discovered that KIO would show web authentication
    credentials in some error cases.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4:4.8.4-4+deb7u3.</p>

<p>We recommend that you upgrade your kde4libs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-952.data"
# $Id: $
