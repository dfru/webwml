<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>libffi requests an executable stack allowing attackers to more easily trigger
arbitrary code execution by overwriting the stack. Please note that libffi is
used by a number of other libraries.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.0.10-3+deb7u1.</p>

<p>We recommend that you upgrade your libffi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-997.data"
# $Id: $
