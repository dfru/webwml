<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in tiff, a library and tools
providing support for the Tag Image File Format (TIFF), leading to
denial of service (DoS) and possibly local code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1354">CVE-2022-1354</a>

    <p>A heap buffer overflow flaw was found in Libtiffs' tiffinfo.c in
    TIFFReadRawDataStriped() function. This flaw allows an attacker to
    pass a crafted TIFF file to the tiffinfo tool, triggering a heap
    buffer overflow issue and causing a crash that leads to a denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1355">CVE-2022-1355</a>

    <p>A stack buffer overflow flaw was found in Libtiffs' tiffcp.c in
    main() function. This flaw allows an attacker to pass a crafted
    TIFF file to the tiffcp tool, triggering a stack buffer overflow
    issue, possibly corrupting the memory, and causing a crash that
    leads to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2056">CVE-2022-2056</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-2057">CVE-2022-2057</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-2058">CVE-2022-2058</a>

    <p>Divide By Zero error in tiffcrop allows attackers to cause a
    denial-of-service via a crafted tiff file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2867">CVE-2022-2867</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-2868">CVE-2022-2868</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-2869">CVE-2022-2869</a>

    <p>libtiff's tiffcrop utility has underflow and input validation flaw
    that can lead to out of bounds read and write. An attacker who
    supplies a crafted file to tiffcrop (likely via tricking a user to
    run tiffcrop on it with certain parameters) could cause a crash or
    in some cases, further exploitation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3570">CVE-2022-3570</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-3598">CVE-2022-3598</a>

    <p>Multiple heap buffer overflows in tiffcrop.c utility in libtiff
    allows attacker to trigger unsafe or out of bounds memory access
    via crafted TIFF image file which could result into application
    crash, potential information disclosure or any other
    context-dependent impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3597">CVE-2022-3597</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-3626">CVE-2022-3626</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-3627">CVE-2022-3627</a>

    <p>Out-of-bounds write, allowing attackers to cause a
    denial-of-service via a crafted tiff file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3599">CVE-2022-3599</a>

    <p>Out-of-bounds read in writeSingleSection in tools/tiffcrop.c,
    allowing attackers to cause a denial-of-service via a crafted tiff
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3970">CVE-2022-3970</a>

    <p>Affects the function TIFFReadRGBATileExt of the file
    libtiff/tif_getimage.c. The manipulation leads to integer
    overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34526">CVE-2022-34526</a>

    <p>A stack overflow was discovered in the _TIFFVGetField function of
    Tiffsplit. This vulnerability allows attackers to cause a Denial
    of Service (DoS) via a crafted TIFF file parsed by the <q>tiffsplit</q>
    or <q>tiffcrop</q> utilities.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
4.1.0+git191117-2~deb10u5.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>For the detailed security status of tiff please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tiff">https://security-tracker.debian.org/tracker/tiff</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3278.data"
# $Id: $
