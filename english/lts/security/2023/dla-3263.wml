<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an off-by-one array size issue in
libtasn1-6, a library to manage the generic ASN.1 data structure.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46848">CVE-2021-46848</a>

    <p>GNU Libtasn1 before 4.19.0 has an ETYPE_OK off-by-one array size check that affects asn1_encode_simple_der.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
4.13-3+deb10u1.</p>

<p>We recommend that you upgrade your libtasn1-6 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3263.data"
# $Id: $
