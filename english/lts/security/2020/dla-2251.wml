<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities were found in Ruby on Rails, a MVC ruby-based
framework geared for web application development, which could lead to
remote code execution and untrusted user input usage, depending on the
application.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8164">CVE-2020-8164</a>

    <p>Strong parameters bypass vector in ActionPack.  In some cases user
    supplied information can be inadvertently leaked from Strong
    Parameters.  Specifically the return value of `each`, or
    `each_value`, or `each_pair` will return the underlying
    <q>untrusted</q> hash of data that was read from the parameters.
    Applications that use this return value may be inadvertently use
    untrusted user input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8165">CVE-2020-8165</a>

    <p>Potentially unintended unmarshalling of user-provided objects in
    MemCacheStore.  There is potentially unexpected behaviour in the
    MemCacheStore where, when untrusted user input is written to the
    cache store using the `raw: true` parameter, re-reading the result
    from the cache can evaluate the user input as a Marshalled object
    instead of plain text.  Unmarshalling of untrusted user input can
    have impact up to and including RCE. At a minimum, this
    vulnerability allows an attacker to inject untrusted Ruby objects
    into a web application.</p>

    <p>In addition to upgrading to the latest versions of Rails,
    developers should ensure that whenever they are calling
    `Rails.cache.fetch` they are using consistent values of the `raw`
    parameter for both reading and writing.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:4.1.8-1+deb8u7.</p>

<p>We recommend that you upgrade your rails packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2251.data"
# $Id: $
