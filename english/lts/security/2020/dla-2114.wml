<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13093">CVE-2018-13093</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13094">CVE-2018-13094</a>

    <p>Wen Xu from SSLab at Gatech reported several NULL pointer
    dereference flaws that may be triggered when mounting and
    operating a crafted XFS volume.  An attacker able to mount
    arbitrary XFS volumes could use this to cause a denial of service
    (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20976">CVE-2018-20976</a>

    <p>It was discovered that the XFS file-system implementation did not
    correctly handle some mount failure conditions, which could lead
    to a use-after-free.  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21008">CVE-2018-21008</a>

    <p>It was discovered that the rsi wifi driver did not correctly
    handle some failure conditions, which could lead to a use-after    free.  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0136">CVE-2019-0136</a>

    <p>It was discovered that the wifi soft-MAC implementation (mac80211)
    did not properly authenticate Tunneled Direct Link Setup (TDLS)
    messages.  A nearby attacker could use this for denial of service
    (loss of wifi connectivity).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2215">CVE-2019-2215</a>

    <p>The syzkaller tool discovered a use-after-free vulnerability in
    the Android binder driver.  A local user on a system with this
    driver enabled could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.
    However, this driver is not enabled on Debian packaged kernels.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10220">CVE-2019-10220</a>

    <p>Various developers and researchers found that if a crafted file    system or malicious file server presented a directory with
    filenames including a '/' character, this could confuse and
    possibly defeat security checks in applications that read the
    directory.</p>

    <p>The kernel will now return an error when reading such a directory,
    rather than passing the invalid filenames on to user-space.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14615">CVE-2019-14615</a>

    <p>It was discovered that Intel 9th and 10th generation GPUs did not
    clear user-visible state during a context switch, which resulted
    in information leaks between GPU tasks.  This has been mitigated
    in the i915 driver.</p>

    <p>The affected chips (gen9 and gen10) are listed at
    <url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen9">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14814">CVE-2019-14814</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14815">CVE-2019-14815</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14816">CVE-2019-14816</a>

    <p>Multiple bugs were discovered in the mwifiex wifi driver, which
    could lead to heap buffer overflows.  A local user permitted to
    configure a device handled by this driver could probably use this
    for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14895">CVE-2019-14895</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14901">CVE-2019-14901</a>

    <p>ADLab of Venustech discovered potential heap buffer overflows in
    the mwifiex wifi driver.  On systems using this driver, a
    malicious Wireless Access Point or adhoc/P2P peer could use these
    to cause a denial of service (memory corruption or crash) or
    possibly for remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14896">CVE-2019-14896</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14897">CVE-2019-14897</a>

    <p>ADLab of Venustech discovered potential heap and stack buffer
    overflows in the libertas wifi driver.  On systems using this
    driver, a malicious Wireless Access Point or adhoc/P2P peer could
    use these to cause a denial of service (memory corruption or
    crash) or possibly for remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15098">CVE-2019-15098</a>

    <p>Hui Peng and Mathias Payer reported that the ath6kl wifi driver
    did not properly validate USB descriptors, which could lead to a
    null pointer derefernce.  An attacker able to add USB devices
    could use this to cause a denial of service (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15217">CVE-2019-15217</a>

    <p>The syzkaller tool discovered that the zr364xx mdia driver did not
    correctly handle devices without a product name string, which
    could lead to a null pointer dereference.  An attacker able to add
    USB devices could use this to cause a denial of service
    (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15291">CVE-2019-15291</a>

    <p>The syzkaller tool discovered that the b2c2-flexcop-usb media
    driver did not properly validate USB descriptors, which could lead
    to a null pointer dereference.  An attacker able to add USB
    devices could use this to cause a denial of service (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15505">CVE-2019-15505</a>

    <p>The syzkaller tool discovered that the technisat-usb2 media driver
    did not properly validate incoming IR packets, which could lead to
    a heap buffer over-read.  An attacker able to add USB devices
    could use this to cause a denial of service (BUG/oops) or to read
    sensitive information from kernel memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15917">CVE-2019-15917</a>

    <p>The syzkaller tool found a race condition in code supporting
    UART-attached Bluetooth adapters, which could lead to a use    after-free.  A local user with access to a pty device or other
    suitable tty device could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16746">CVE-2019-16746</a>

    <p>It was discovered that the wifi stack did not validate the content
    of beacon heads provided by user-space for use on a wifi interface
    in Access Point mode, which could lead to a heap buffer overflow.
    A local user permitted to configure a wifi interface could use
    this to cause a denial of service (memory corruption or crash) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17052">CVE-2019-17052</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-17053">CVE-2019-17053</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-17054">CVE-2019-17054</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-17055">CVE-2019-17055</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-17056">CVE-2019-17056</a>

    <p>Ori Nimron reported that various network protocol implementations
    - AX.25, IEEE 802.15.4, Appletalk, ISDN, and NFC - allowed all
    users to create raw sockets.  A local user could use this to send
    arbitrary packets on networks using those protocols.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17075">CVE-2019-17075</a>

    <p>It was found that the cxgb4 Infiniband driver requested DMA
    (Direct Memory Access) to a stack-allocated buffer, which is not
    supported and on some systems can result in memory corruption of
    the stack.  A local user might be able to use this for denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17133">CVE-2019-17133</a>

    <p>Nicholas Waisman reported that the wifi stack did not valdiate
    received SSID information before copying it, which could lead to a
    buffer overflow if it is not validated by the driver or firmware.
    A malicious Wireless Access Point might be able to use this to
    cause a denial of service (memory corruption or crash) or for
    remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17666">CVE-2019-17666</a>

    <p>Nicholas Waisman reported that the rtlwifi wifi drivers did not
    properly validate received P2P information, leading to a buffer
    overflow.  A malicious P2P peer could use this to cause a denial
    of service (memory corruption or crash) or for remote code
    execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18282">CVE-2019-18282</a>

    <p>Jonathan Berger, Amit Klein, and Benny Pinkas discovered that the
    generation of UDP/IPv6 flow labels used a weak hash function,
    <q>jhash</q>.  This could enable tracking individual computers as they
    communicate with different remote servers and from different
    networks.  The <q>siphash</q> function is now used instead.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18683">CVE-2019-18683</a>

    <p>Multiple race conditions were discovered in the vivid media
    driver, used for testing Video4Linux2 (V4L2) applications,
    These race conditions could result in a use-after-free.  On a
    system where this driver is loaded, a user with permission
    to access media devices could use this to cause a denial
    of service (memory corruption or crash) or possibly for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18809">CVE-2019-18809</a>

    <p>Navid Emamdoost discovered a potential memory leak in the af9005
    media driver if the device fails to respond to a command.  The
    security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19037">CVE-2019-19037</a>

    <p>It was discovered that the ext4 filesystem driver did not
    correctly handle directories with holes (unallocated regions) in
    them.  An attacker able to mount arbitrary ext4 volumes could use
    this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19051">CVE-2019-19051</a>

    <p>Navid Emamdoost discovered a potential memory leak in the i2400m
    wimax driver if the software rfkill operation fails.  The security
    impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19052">CVE-2019-19052</a>

    <p>Navid Emamdoost discovered a potential memory leak in the gs_usb
    CAN driver if the open (interface-up) operation fails.  The
    security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19056">CVE-2019-19056</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19057">CVE-2019-19057</a>

    <p>Navid Emamdoost discovered potential memory leaks in the mwifiex
    wifi driver if the probe operation fails.  The security impact of
    this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19062">CVE-2019-19062</a>

    <p>Navid Emamdoost discovered a potential memory leak in the AF_ALG
    subsystem if the CRYPTO_MSG_GETALG operation fails.  A local user
    could possibly use this to cause a denial of service (memory
    exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19066">CVE-2019-19066</a>

    <p>Navid Emamdoost discovered a potential memory leak in the bfa SCSI
    driver if the get_fc_host_stats operation fails.  The security
    impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19068">CVE-2019-19068</a>

    <p>Navid Emamdoost discovered a potential memory leak in the rtl8xxxu
    wifi driver, in case it fails to submit an interrupt buffer to the
    device.  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19227">CVE-2019-19227</a>

    <p>Dan Carpenter reported missing error checks in the Appletalk
    protocol implementation that could lead to a null pointer
    dereference.  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19332">CVE-2019-19332</a>

    <p>The syzkaller tool discovered a missing bounds check in the KVM
    implementation for x86, which could lead to a heap buffer overflow.
    A local user permitted to use KVM could use this to cause a denial
    of service (memory corruption or crash) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19447">CVE-2019-19447</a>

    <p>It was discovered that the ext4 filesystem driver did not safely
    handle unlinking of an inode that, due to filesystem corruption,
    already has a link count of 0.  An attacker able to mount
    arbitrary ext4 volumes could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19523">CVE-2019-19523</a>

    <p>The syzkaller tool discovered a use-after-free bug in the adutux
    USB driver.  An attacker able to add and remove USB devices could
    use this to cause a denial of service (memory corruption or crash)
    or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19524">CVE-2019-19524</a>

    <p>The syzkaller tool discovered a race condition in the ff-memless
    library used by input drivers.  An attacker able to add and remove
    USB devices could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19525">CVE-2019-19525</a>

    <p>The syzkaller tool discovered a use-after-free bug in the atusb
    driver for IEEE 802.15.4 networking.  An attacker able to add and
    remove USB devices could possibly use this to cause a denial of
    service (memory corruption or crash) or for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19527">CVE-2019-19527</a>

    <p>The syzkaller tool discovered that the hiddev driver did not
    correctly handle races between a task opening the device and
    disconnection of the underlying hardware.  A local user permitted
    to access hiddev devices, and able to add and remove USB devices,
    could use this to cause a denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19530">CVE-2019-19530</a>

    <p>The syzkaller tool discovered a potential use-after-free in the
    cdc-acm network driver.  An attacker able to add USB devices could
    use this to cause a denial of service (memory corruption or crash)
    or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19531">CVE-2019-19531</a>

    <p>The syzkaller tool discovered a use-after-free bug in the yurex
    USB driver.  An attacker able to add and remove USB devices could
    use this to cause a denial of service (memory corruption or crash)
    or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19532">CVE-2019-19532</a>

    <p>The syzkaller tool discovered a potential heap buffer overflow in
    the hid-gaff input driver, which was also found to exist in many
    other input drivers.  An attacker able to add USB devices could
    use this to cause a denial of service (memory corruption or crash)
    or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19533">CVE-2019-19533</a>

    <p>The syzkaller tool discovered that the ttusb-dec media driver was
    missing initialisation of a structure, which could leak sensitive
    information from kernel memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19534">CVE-2019-19534</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19535">CVE-2019-19535</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19536">CVE-2019-19536</a>

    <p>The syzkaller tool discovered that the peak_usb CAN driver was
    missing initialisation of some structures, which could leak
    sensitive information from kernel memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19537">CVE-2019-19537</a>

    <p>The syzkaller tool discovered race conditions in the USB stack,
    involving character device registration.  An attacker able to add
    USB devices could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19767">CVE-2019-19767</a>

    <p>The syzkaller tool discovered that crafted ext4 volumes could
    trigger a buffer overflow in the ext4 filesystem driver.  An
    attacker able to mount such a volume could use this to cause a
    denial of service (memory corruption or crash) or possibly for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19947">CVE-2019-19947</a>

    <p>It was discovered that the kvaser_usb CAN driver was missing
    initialisation of some structures, which could leak sensitive
    information from kernel memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19965">CVE-2019-19965</a>

    <p>Gao Chuan reported a race condition in the libsas library used by
    SCSI host drivers, which could lead to a null pointer dereference.
    An attacker able to add and remove SCSI devices could use this to
    cause a denial of service (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20096">CVE-2019-20096</a>

    <p>The Hulk Robot tool discovered a potential memory leak in the DCCP
    protocol implementation.  This may be exploitable by local users,
    or by remote attackers if the system uses DCCP, to cause a denial
    of service (out of memory).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in
version 4.9.210-1~deb8u1.  This update additionally fixes Debian bugs
<a href="https://bugs.debian.org/869511">#869511</a> and <a
href="https://bugs.debian.org/945023">#945023</a>; and includes many
more bug fixes from stable updates 4.9.190-4.9.210 inclusive.</p>

<p>We recommend that you upgrade your linux-4.9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2114.data"
# $Id: $
