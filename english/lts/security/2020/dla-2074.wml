<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been found in python-apt, a python interface to libapt-pkg.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15795">CVE-2019-15795</a>

    <p>It was discovered that python-apt would still use MD5 hashes to validate
    certain downloaded packages. If a remote attacker were able to perform a
    man-in-the-middle attack, this flaw could potentially be used to install
    altered packages.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15796">CVE-2019-15796</a>

    <p>It was discovered that python-apt could install packages from untrusted
    repositories, contrary to expectations.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.9.3.13.</p>

<p>We recommend that you upgrade your python-apt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2074.data"
# $Id: $
