<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Kevin Backhouse discovered multiple vulnerabilies in the epson2 and
epsonds backends of SANE, a library for scanners.  A malicious remote
device could exploit these to trigger information disclosure, denial
of service and possibly remote code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12862">CVE-2020-12862</a>

    <p>An out-of-bounds read in SANE Backends before 1.0.30 may allow a
    malicious device connected to the same local network as the victim
    to read important information, such as the ASLR offsets of the
    program, aka GHSL-2020-082.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12863">CVE-2020-12863</a>

    <p>An out-of-bounds read in SANE Backends before 1.0.30 may allow a
    malicious device connected to the same local network as the victim
    to read important information, such as the ASLR offsets of the
    program, aka GHSL-2020-083.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12865">CVE-2020-12865</a>

    <p>A heap buffer overflow in SANE Backends before 1.0.30 may allow a
    malicious device connected to the same local network as the victim
    to execute arbitrary code, aka GHSL-2020-084.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12867">CVE-2020-12867</a>

    <p>A NULL pointer dereference in sanei_epson_net_read in SANE
    Backends before 1.0.30 allows a malicious device connected to the
    same local network as the victim to cause a denial of service, aka
    GHSL-2020-075.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.0.25-4.1+deb9u1.</p>

<p>We recommend that you upgrade your sane-backends packages.</p>

<p>For the detailed security status of sane-backends please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sane-backends">https://security-tracker.debian.org/tracker/sane-backends</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2332.data"
# $Id: $
