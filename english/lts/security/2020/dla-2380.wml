<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a cross-site scripting (XSS) vulnerability
in ruby-gon, a Ruby library to send/convert data to Javascript from a Ruby
application. </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25739">CVE-2020-25739</a>

    <p>An issue was discovered in the gon gem before gon-6.4.0 for Ruby.
    MultiJson does not honor the escape_mode parameter to escape fields as an
    XSS protection mechanism. To mitigate, json_dumper.rb in gon now does
    escaping for XSS by default without relying on MultiJson.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
6.1.0-1+deb9u1.</p>

<p>We recommend that you upgrade your ruby-gon packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2380.data"
# $Id: $
