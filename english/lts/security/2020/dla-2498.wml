<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The UK's National Cyber Security Centre (NCSC) discovered that
Xerces-C, a validating XML parser library for C++, contains a
use-after-free error triggered during the scanning of external
DTDs. An attacker could cause a Denial of Service (DoS) and possibly
achieve remote code execution. This flaw has not been addressed in the
maintained version of the library and has no complete mitigation. The
first is provided by this update which fixes the use-after-free
vulnerability at the expense of a memory leak. The other is to disable
DTD processing, which can be accomplished via the DOM using a standard
parser feature, or via SAX using the XERCES_DISABLE_DTD environment
variable.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.1.4+debian-2+deb9u2.</p>

<p>We recommend that you upgrade your xerces-c packages.</p>

<p>For the detailed security status of xerces-c please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xerces-c">https://security-tracker.debian.org/tracker/xerces-c</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2498.data"
# $Id: $
