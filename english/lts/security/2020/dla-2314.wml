<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the ClamAV antivirus toolkit:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3327">CVE-2020-3327</a>

    <p>An out of bounds read in the ARJ archive-parsing module could cause
    denial of service. The fix in 0.102.3 was incomplete.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3350">CVE-2020-3350</a>

    <p>A malicious user could trick clamscan, clamdscan or clamonacc into
    moving or removing a different file than intended when those are
    used with one of the --move or --remove options. This could be used
    to get rid of special system files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3481">CVE-2020-3481</a>

    <p>The EGG archive module was vulnerable to denial of service via NULL
    pointer dereference due to improper error handling. The official
    signature database avoided this problem because the signatures there
    avoided the use of the EGG archive parser.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.102.4+dfsg-0+deb9u1.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>For the detailed security status of clamav please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/clamav">https://security-tracker.debian.org/tracker/clamav</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2314.data"
# $Id: $
