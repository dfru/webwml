<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Brief introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14464">CVE-2019-14464</a>

    <p>Heap-based buffer overflow in XMFile::read</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14496">CVE-2019-14496</a>

    <p>Stack-based buffer overflow in LoaderXM::load</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14497">CVE-2019-14497</a>

    <p>Heap-based buffer overflow in
    ModuleEditor::convertInstrument</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15569">CVE-2020-15569</a>

    <p>Use-after-free in the PlayerGeneric destructor</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.90.86+dfsg-2+deb9u1.</p>

<p>We recommend that you upgrade your milkytracker packages.</p>

<p>For the detailed security status of milkytracker please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/milkytracker">https://security-tracker.debian.org/tracker/milkytracker</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2292.data"
# $Id: $
