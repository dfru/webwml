<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities have been discovered in mutt, a console email client.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14093">CVE-2020-14093</a>

    <p>Mutt allowed an IMAP fcc/postpone man-in-the-middle attack via a
    PREAUTH response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14954">CVE-2020-14954</a>

    <p>Mutt had a STARTTLS buffering issue that affected IMAP, SMTP, and
    POP3. When a server had sent a <q>begin TLS</q> response, the client read
    additional data (e.g., from a man-in-the-middle attacker) and
    evaluated it in a TLS context, aka "response injection."</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.5.23-3+deb8u2.</p>

<p>We recommend that you upgrade your mutt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2268.data"
# $Id: $
