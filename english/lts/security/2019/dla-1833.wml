<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues in bzip2, a high-quality block-sorting file compressor, have
been fixed. One, <a href="https://security-tracker.debian.org/tracker/CVE-2019-12900">CVE-2019-12900</a>, is a out-of-bounds write when using a
crafted compressed file. The other, <a href="https://security-tracker.debian.org/tracker/CVE-2016-3189">CVE-2016-3189</a>, is a potential
user-after-free.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.0.6-7+deb8u1.</p>

<p>We recommend that you upgrade your bzip2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1833.data"
# $Id: $
