<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have recently been discovered in TightVNC 1.x, an
X11 based VNC server/viewer application for Windows and Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6053">CVE-2014-6053</a>

    <p>The rfbProcessClientNormalMessage function in rfbserver.c in TightVNC
    server did not properly handle attempts to send a large amount of
    ClientCutText data, which allowed remote attackers to cause a denial
    of service (memory consumption or daemon crash) via a crafted message
    that was processed by using a single unchecked malloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7225">CVE-2018-7225</a>

    <p>rfbProcessClientNormalMessage() in rfbserver.c did not sanitize
    msg.cct.length, leading to access to uninitialized and potentially
    sensitive data or possibly unspecified other impact (e.g., an integer
    overflow) via specially crafted VNC packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8287">CVE-2019-8287</a>

    <p>TightVNC code contained global buffer overflow in HandleCoRREBBP
    macro function, which could potentially have result in code
    execution. This attack appeared to be exploitable via network
    connectivity.</p>

    <p>(aka <a href="https://security-tracker.debian.org/tracker/CVE-2018-20020">CVE-2018-20020</a>/libvncserver)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20021">CVE-2018-20021</a>

    <p>TightVNC in vncviewer/rfbproto.c contained a CWE-835: Infinite loop
    vulnerability. The vulnerability allowed an attacker to consume
    an excessive amount of resources like CPU and RAM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20022">CVE-2018-20022</a>

    <p>TightVNC's vncviewer contained multiple weaknesses CWE-665: Improper
    Initialization vulnerability in VNC client code that allowed
    attackers to read stack memory and could be abused for information
    disclosure. Combined with another vulnerability, it could be used to
    leak stack memory layout and in bypassing ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15678">CVE-2019-15678</a>

    <p>TightVNC code version contained heap buffer overflow in
    rfbServerCutText handler, which could have potentially resulted in
    code execution. This attack appeared to be exploitable via network
    connectivity.</p>

    <p>(partially aka <a href="https://security-tracker.debian.org/tracker/CVE-2018-20748">CVE-2018-20748</a>/libvnvserver)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15679">CVE-2019-15679</a>

    <p>TightVNC's vncviewer code contained a heap buffer overflow in
    InitialiseRFBConnection function, which could have potentially
    resulted in code execution. This attack appeared to be exploitable
    via network connectivity.</p>

    <p>(partially aka <a href="https://security-tracker.debian.org/tracker/CVE-2018-20748">CVE-2018-20748</a>/libvnvserver)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15680">CVE-2019-15680</a>

    <p>TightVNC's vncviewer code contained a null pointer dereference in
    HandleZlibBPP function, which could have resulted in Denial of System
    (DoS). This attack appeared to be exploitable via network
    connectivity.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15681">CVE-2019-15681</a>

    <p>TightVNC contained a memory leak (CWE-655) in VNC server code, which
    allowed an attacker to read stack memory and could have been abused
    for information disclosure. Combined with another vulnerability, it
    could have been used to leak stack memory and bypass ASLR. This
    attack appeared to be exploitable via network connectivity.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.9-6.5+deb8u1.</p>

<p>We recommend that you upgrade your tightvnc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2045.data"
# $Id: $
