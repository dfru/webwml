<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were three vulnerabilities in the curl
command-line HTTP (etc.) client:</p>

<ul>

 <li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16890">CVE-2018-16890</a>
    <p>A heap buffer out-of-bounds read vulnerability in
   the handling of NTLM type-2 messages.</p></li>

 <li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3822">CVE-2019-3822</a>
    <p>Stack-based buffer overflow in the handling of
   outgoing NTLM type-3 headers.</p></li>

 <li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3823">CVE-2019-3823</a>
    <p>Heap out-of-bounds read in code handling
   the end of a response in the SMTP protocol.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, this issue has been fixed in curl version
7.38.0-4+deb8u14.</p>

<p>We recommend that you upgrade your curl packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1672.data"
# $Id: $
