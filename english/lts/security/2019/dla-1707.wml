<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security vulnerabilities have been discovered in symfony, a PHP
web application framework.  Numerous symfony components are affected:
Security, bundle readers, session handling, SecurityBundle,
HttpFoundation, Form, and Security\Http.</p>

<p>The corresponding upstream advisories contain further details:</p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2017-16652">CVE-2017-16652</a>]
<a href="https://symfony.com/blog/cve-2017-16652-open-redirect-vulnerability-on-security-handlers">https://symfony.com/blog/cve-2017-16652-open-redirect-vulnerability-on-security-handlers</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2017-16654">CVE-2017-16654</a>]
<a href="https://symfony.com/blog/cve-2017-16654-intl-bundle-readers-breaking-out-of-paths">https://symfony.com/blog/cve-2017-16654-intl-bundle-readers-breaking-out-of-paths</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-11385">CVE-2018-11385</a>]
<a href="https://symfony.com/blog/cve-2018-11385-session-fixation-issue-for-guard-authentication">https://symfony.com/blog/cve-2018-11385-session-fixation-issue-for-guard-authentication</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-11408">CVE-2018-11408</a>]
<a href="https://symfony.com/blog/cve-2018-11408-open-redirect-vulnerability-on-security-handlers">https://symfony.com/blog/cve-2018-11408-open-redirect-vulnerability-on-security-handlers</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-14773">CVE-2018-14773</a>]
<a href="https://symfony.com/blog/cve-2018-14773-remove-support-for-legacy-and-risky-http-headers">https://symfony.com/blog/cve-2018-14773-remove-support-for-legacy-and-risky-http-headers</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-19789">CVE-2018-19789</a>]
<a href="https://symfony.com/blog/cve-2018-19789-disclosure-of-uploaded-files-full-path">https://symfony.com/blog/cve-2018-19789-disclosure-of-uploaded-files-full-path</a></p>

<p>[<a href="https://security-tracker.debian.org/tracker/CVE-2018-19790">CVE-2018-19790</a>]
<a href="https://symfony.com/blog/cve-2018-19790-open-redirect-vulnerability-when-using-security-http">https://symfony.com/blog/cve-2018-19790-open-redirect-vulnerability-when-using-security-http</a></p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.3.21+dfsg-4+deb8u4.</p>

<p>We recommend that you upgrade your symfony packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1707.data"
# $Id: $
