<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The update for rdesktop released as 1.8.6-0+deb8u1 introduced a regression
which broke RDP protocol negotiation. Updated rdesktop packages are now
available to correct this issue.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.8.6-0+deb8u2.</p>

<p>We recommend that you upgrade your rdesktop packages.</p>

<p>For the detailed security status of rdesktop please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rdesktop">https://security-tracker.debian.org/tracker/rdesktop</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1837-2.data"
# $Id: $
