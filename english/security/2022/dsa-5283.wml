<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several flaws were discovered in jackson-databind, a fast and powerful JSON
library for Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36518">CVE-2020-36518</a>

    <p>Java StackOverflow exception and denial of service via a large depth of
    nested objects.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42003">CVE-2022-42003</a>

    <p>In FasterXML jackson-databind resource exhaustion can occur because of a
    lack of a check in primitive value deserializers to avoid deep wrapper
    array nesting, when the UNWRAP_SINGLE_VALUE_ARRAYS feature is enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42004">CVE-2022-42004</a>

    <p>In FasterXML jackson-databind resource exhaustion can occur because of a
    lack of a check in BeanDeserializerBase.deserializeFromArray to prevent use
    of deeply nested arrays. An application is vulnerable only with certain
    customized choices for deserialization.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.12.1-1+deb11u1.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>For the detailed security status of jackson-databind please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jackson-databind">\
https://security-tracker.debian.org/tracker/jackson-databind</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5283.data"
# $Id: $
