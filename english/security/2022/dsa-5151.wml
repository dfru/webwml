<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in smarty3, the compiling
PHP template engine. Template authors are able to run restricted static php
methods or even arbitrary PHP code by crafting a malicious math string or by
choosing an invalid {block} or {include} file name. If a math string was passed
through as user provided data to the math function, remote users were able to
run arbitrary PHP code as well.</p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 3.1.33+20180830.1.3a78a21f+selfpack1-1+deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 3.1.39-2+deb11u1.</p>

<p>We recommend that you upgrade your smarty3 packages.</p>

<p>For the detailed security status of smarty3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/smarty3">\
https://security-tracker.debian.org/tracker/smarty3</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5151.data"
# $Id: $
