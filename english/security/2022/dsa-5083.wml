<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22589">CVE-2022-22589</a>

    <p>Heige and Bo Qu discovered that processing a maliciously crafted
    mail message may lead to running arbitrary javascript.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22590">CVE-2022-22590</a>

    <p>Toan Pham discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22592">CVE-2022-22592</a>

    <p>Prakash discovered that processing maliciously crafted web content
    may prevent Content Security Policy from being enforced.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22620">CVE-2022-22620</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to arbitrary code execution. Apple is
    aware of a report that this issue may have been actively
    exploited.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 2.34.6-1~deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.34.6-1~deb11u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5083.data"
# $Id: $
