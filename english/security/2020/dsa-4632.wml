<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Ilja Van Sprundel reported a logic flaw in the Extensible Authentication
Protocol (EAP) packet parser in the Point-to-Point Protocol Daemon
(pppd). An unauthenticated attacker can take advantage of this flaw to
trigger a stack-based buffer overflow, leading to denial of service
(pppd daemon crash).</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 2.4.7-1+4+deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 2.4.7-2+4.1+deb10u1.</p>

<p>We recommend that you upgrade your ppp packages.</p>

<p>For the detailed security status of ppp please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/ppp">https://security-tracker.debian.org/tracker/ppp</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4632.data"
# $Id: $
