#use wml::debian::language_names
#use wml::debian::common_tags

#include "$(ENGLISHDIR)/devel/leader.data"

<bind-gettext-domain domain="organization" />

# <gettext domain="organization">Delegates</gettext>
# <gettext domain="organization">Installation</gettext>
# <gettext domain="organization">Mailing list</gettext>

<define-tag spaces>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</define-tag>
<define-tag job><li>%attributes —</define-tag>
<define-tag genericemail>&lt;<a href="mailto:%0">%0</a>&gt;</define-tag>
<define-tag delegationmail>— <a href="https://lists.debian.org/%0"><gettext domain="organization">delegation mail</gettext></a></define-tag>
<define-tag appointmentmail>— <a href="https://lists.debian.org/%0"><gettext domain="organization">appointment mail</gettext></a></define-tag>
# One male delegate
<define-tag delegate_male> (<gettext domain="organization"><void id="male"/>delegate</gettext>)</define-tag>
# One female delegate
<define-tag delegate_female> (<gettext domain="organization"><void id="female"/>delegate</gettext>)</define-tag>
# Pronoun tags with delegate combinations
<define-tag delegate_he_him> (<gettext domain="organization"><void id="male"/>delegate</gettext>; <gettext domain="organization"><void id="he_him"/>he/him</gettext>)</define-tag>
<define-tag delegate_she_her> (<gettext domain="organization"><void id="female"/>delegate</gettext>; <gettext domain="organization"><void id="she_her"/>she/her</gettext>)</define-tag>
<define-tag delegate_they_them> (<gettext domain="organization"><void id="gender_neutral"/>delegate</gettext>; <gettext domain="organization"><void id="they_them"/>they/them</gettext>)</define-tag>
<define-tag he_him> (<gettext domain="organization"><void id="he_him"/>he/him</gettext>)</define-tag>
<define-tag she_her> (<gettext domain="organization"><void id="she_her"/>she/her</gettext>)</define-tag>
<define-tag they_them> (<gettext domain="organization"><void id="they_them"/>they/them</gettext>)</define-tag>
# current* is only for positions necessarily transitional, such as the leader
<define-tag current><br>
<spaces><em><gettext domain="organization">current</gettext></em>&nbsp;</define-tag>
<define-tag currentmail><br>
<spaces><em><gettext domain="organization">current</gettext></em> %0 &lt;<a href="mailto:%1">%1</a>&gt;</define-tag>
<define-tag member><br>
<spaces><em><gettext domain="organization">member</gettext></em>&nbsp;</define-tag>
<define-tag membermail><br>
<spaces><em><gettext domain="organization">member</gettext></em> %0 &lt;<a href="mailto:%1">%1</a>&gt;</define-tag>
# for release team we use <manager>, <srm>, <wizard> and <assistant>
<define-tag manager><br>
<spaces><em><gettext domain="organization">manager</gettext></em>&nbsp;</define-tag>
<define-tag srm><br>
<spaces><em><abbr title="<gettext domain="organization">Stable Release Manager</gettext>"><gettext domain="organization">SRM</gettext></abbr></em>&nbsp;</define-tag>
<define-tag wizard><br>
<spaces><em><gettext domain="organization">wizard</gettext></em>&nbsp;</define-tag>
# we only use the chair tag once, for techctte, I wonder why it's here.
<define-tag chair>(<em><gettext domain="organization">chair</gettext></em>)</define-tag>
# assistant tag added for DPL "second in command"
<define-tag assistant><br>
<spaces><em><gettext domain="organization">assistant</gettext></em>&nbsp;</define-tag>
<define-tag secretary><br>
<spaces><em><gettext domain="organization">secretary</gettext></em>&nbsp;</define-tag>
<define-tag representative><br>
<spaces><em><gettext domain="organization">representative</gettext></em>&nbsp;</define-tag>
<define-tag representative-role><br>
<spaces><em><gettext domain="organization">role</gettext></em>&nbsp;</define-tag>
<define-tag note><br>
<em>%attributes</em></define-tag>

# --------------------------------------------------------------------------

<div class="tip">
<p>
<gettext domain="organization">In the following list, <q>current</q> is used for positions that are
transitional (elected or appointed with a certain expiration date).</gettext>
</p>
</div>

# there is no officers page, yet
# <em><strong><a href="$(HOME)/intro/officers"><gettext domain="organization">Officers</gettext></a></em></strong>
<ul class="toc">
<li><a href="#officers"><gettext domain="organization">Officers</gettext></a></li>
<li><a href="#distribution"><gettext domain="organization">Distribution</gettext></a></li>
<li><a href="#commsoutreach"><gettext domain="organization">Communication and Outreach</gettext></a>
<ul>
 <li><a href="#data-protection"><gettext domain="organization">Data Protection team</gettext></a></li>
  <li><a href="#publicity"><gettext domain="organization">Publicity team</gettext></a></li>
</ul></li>
<li><a href="#memberships"><gettext domain="organization">Membership in other organizations</gettext></a></li>
<li><a href="#support"><gettext domain="organization">Support and Infrastructure</gettext></a></li>
</ul>

<a name="officers"></a>
<h3><gettext domain="organization">Officers</gettext></h3>

<ul>
  <job <a name="leader" href="$(HOME)/devel/leader"><gettext domain="organization">Leader</gettext></a>> <genericemail leader@debian.org>
    <current><current_leader>
  <job <a name="tech-ctte" href="$(HOME)/devel/tech-ctte"><gettext domain="organization">Technical Committee</gettext></a>> <genericemail debian-ctte@lists.debian.org>
  <appointmentmail 1498053993.4017640.1016684592.748E3111@webmail.messagingengine.com>
  <appointmentmail 1514922200.4161380.1222079088.19895321@webmail.messagingengine.com>
  <appointmentmail 1521224077.296447.1305789200.53C43961@webmail.messagingengine.com>
  <appointmentmail e236c0a0-3194-0a26-b6d8-545647ec887c@debian.org>
  <appointmentmail 20210311203400.6f222e25@commodore.bluemosh.com>
  <appointmentmail msgid-search/?m=YfaAsRdS/EdTdJbo@bluemosh.com>
    <current>Sean Whitton <chair>
    <current>Niko Tyni
    <current>Gunnar Wolf
    <current>Simon McVittie
    <current>Elana Hashman
    <current>Christoph Berg
    <current>Helmut Grohne
    <current>Matthew Vernon
  <job <a name="secretary" href="$(HOME)/devel/secretary"><gettext domain="organization">Secretary</gettext></a>> <genericemail secretary@debian.org> <appointmentmail Yg1hGdrKntMvjBgr@bluemosh.com>
    <current>Kurt Roeckx
</ul>

<a name="distribution"></a>
<h3><gettext domain="organization">Distribution</gettext></h3>

<ul>
  <job <a name="devel" href="$(HOME)/devel/"><gettext domain="organization">Development Projects</gettext></a>> <genericemail debian-devel@lists.debian.org>
  <job <a name="ftpmaster" href="https://ftp-master.debian.org/"><gettext domain="organization">FTP Archives</gettext></a>> <genericemail ftpmaster@debian.org> <delegationmail 1511172581.3130639.1178259472.01A2D970@webmail.messagingengine.com>
  <ul>
    <job <a name="ftpmasters"><gettext domain="organization">FTP Masters</gettext></a>>
      <member>Thorsten Alteholz <delegate_male>
      <member>Ansgar <delegate_male>
      <member>Joerg Jaspert <delegate_male>
      <member>Luke W. Faraone <delegate_they_them>
      <member>Mark Hymers <delegate_male>
    <job <a name="ftpassistants"><gettext domain="organization">FTP Assistants</gettext></a>>
      <member>Bastian Blank
      <member>Luca Falavigna
      <member>Paul Tagliamonte
      <member>Scott Kitterman
      <member>Sean Whitton
    <job <a name="ftpwizards"><gettext domain="organization">FTP Wizards</gettext></a>>
      <member>Mike O'Connor
  </ul>
<job <a name="backports" href="https://backports.debian.org/"><gettext domain="organization">Backports</gettext></a>> <genericemail backports-team@debian.org> <delegationmail 20110706145100.GA13459@upsilon.cc>
  <ul>
  <job <a name="backports-team"><gettext domain="organization">Backports Team</gettext></a>>
    <member>Alexander Wirt<delegate_male>
    <member>Rhonda D'Vine<delegate_female>
  </ul>
  <job <a name="release"><gettext domain="organization">Release Management</gettext></a>>
  <ul>
    <job <a name="release-team" href="https://release.debian.org/"><gettext domain="organization">Release Team</gettext></a>> <genericemail debian-release@lists.debian.org> <delegationmail b9368017-03f6-ec15-c5ee-56208ba0969f@debian.org>
      <member>Emilio Pozuelo Monfort<delegate_male>
      <member>Adam D. Barratt<delegate_male>
      <member>Julien Cristau<delegate_male>
      <member>Cyril Brulebois<delegate_male>
      <member>Ivo De Decker<delegate_male>
      <member>Jonathan Wiltshire<delegate_male>
      <member>Graham Inggs<delegate_male>
      <member>Paul Gevers<delegate_male>
      <member>Sebastian Ramacher<delegate_male>
    <job <a name="qa" href="https://qa.debian.org/"><gettext domain="organization">Quality Assurance</gettext></a>> <genericemail debian-qa@lists.debian.org>
    <job <a name="installer"><gettext domain="organization">Installation System Team</gettext></a>> <genericemail debian-boot@lists.debian.org>
    <job <a name="live" href="https://wiki.debian.org/DebianLive"><gettext domain="organization">Debian Live Team</gettext></a>> <genericemail debian-live@lists.debian.org>
    <job <a name="cd" href="$(HOME)/CD/"><gettext domain="organization">CD/DVD/USB Images</gettext></a>>
    <ul>
      <job <a name="cd-production"><gettext domain="organization">Production</gettext></a>> <genericemail debian-cd@lists.debian.org>
        <member>Philip Hands
        <member>Rapha&euml;l Hertzog
        <member>Steve McIntyre
        <member>Andy Simpkins
        <member>Mattias Wadenstein
        <member>Andrew Cater
    </ul>
    <job <a name="testing"><gettext domain="organization">Testing</gettext></a>> <genericemail debian-testing@lists.debian.org>
  </ul>
  <job <a name="cloud" href="https://wiki.debian.org/Teams/Cloud"><gettext domain="organization">Cloud Team</gettext></a>> <genericemail debian-cloud@lists.debian.org> <delegationmail tsld08riicq.fsf@suchdamage.org>
      <member>Thomas Lange<delegate_male>
  <job <a name="buildd" href="https://buildd.debian.org/"><gettext domain="organization">Autobuilding infrastructure</gettext></a>>
  <ul>
    <job <a name="wanna-build"><gettext domain="organization">Wanna-build team</gettext></a>> <genericemail debian-wb-team@lists.debian.org>
      <member>Andreas Barth
      <member>Aurelien Jarno
      <member>Joachim Breitner
      <member>Kurt Roeckx
      <member>Mehdi Dogguy
      <member>Philipp Kern
    <job <a name="buildd-adm"><gettext domain="organization">Buildd administration</gettext></a>> <var>&lt;architecture&gt;</var>@buildd.debian.org
      <member>Andreas Barth
      <member>Aurelien Jarno
      <member>Christoph Egger
      <member>Colin Tuckley
      <member>Dann Frazier
      <member>Frederik Sch&uuml;ler
      <member>Hector Oron
      <member>Konstantinos Margaritis
      <member>Kurt Roeckx
      <member>LaMont Jones
      <member>Michael Banck
      <member>Peter De Schrijver
      <member>Philipp Kern
      <member>Riku Voipio
      <member>Samuel Thibault
  </ul>
  <job <a name="doc" href="$(HOME)/doc/"><gettext domain="organization">Documentation</gettext></a>> <genericemail debian-doc@lists.debian.org>
    <member>Javier Fern&aacute;ndez-Sanguino
    <member>Osamu Aoki
    <member>Josip Rodin
  <job <a name="wnpp" href="$(HOME)/devel/wnpp/"><gettext domain="organization">Work-Needing and Prospective Packages list</gettext></a>> <genericemail debian-wnpp@lists.debian.org>
    <member>Matej Vela
</ul>

<a name="commsoutreach"></a>
<h3><gettext domain="organization">Communication and Outreach</gettext></h3>

<ul>
   <job <a name="data-protection"><gettext domain="organization">Data Protection team</gettext></a>> <genericemail data-protection@debian.org> <delegationmail 1528227688.412908.1397547576.57703279@webmail.messagingengine.com>
    <member>Jonathan McDowell<delegate_he_him>
    <member>Tollef Fog Heen<delegate_male>
    <member>Matthew Vernon<delegate_male>

    <job <a name="publicity" href="https://wiki.debian.org/Teams/Publicity"><gettext domain="organization">Publicity team</gettext></a>> <genericemail debian-publicity@lists.debian.org> <delegationmail 1527454813.1668886.1387370912.1CCDF411@webmail.messagingengine.com>
    <member>Laura Arjona Reina<delegate_female>
    <member>Donald Norwood<delegate_male>
    <note <gettext domain="organization">Press Contact</gettext> — <genericemail press@debian.org>>

  <job <a name="website" href="$(HOME)/devel/website/"><gettext domain="organization">Web Pages</gettext></a>> <genericemail debian-www@lists.debian.org>
    <member>Rhonda D'Vine
    <member>K&aring;re Thor Olsen
    <member>Paul Wise
    <member>Laura Arjona Reina
    <member>Carsten Schoenert
    <member>Thomas Lange

  <job <a name="planet" href="https://planet.debian.org/"><gettext domain="organization">Planet Debian</gettext></a>> <genericemail planet@debian.org>
    <member>Benjamin Mako Hill
    <member>Joerg Jaspert
    <member>Jordi Mallach

  <job <a name="outreach" href="https://wiki.debian.org/Teams/Outreach"><gettext domain="organization">Outreach</gettext></a>> <genericemail outreach@debian.org> <delegationmail 785e2f3c-afcb-45f3-a6b2-9af397c48d34@www.fastmail.com>
    <member>Molly de Blanc <delegate_female>
    <member>Pranav Jain <delegate_male>
    <member>Jaminy Prabaharan <delegate_female>

  <job <a name="women" href="https://www.debian.org/women/"><gettext domain="organization">Debian Women Project</gettext></a>> <genericemail debian-women@lists.debian.org>
    <member>Amaya Rodrigo
    <member>Erinn Clark
    <member>Hanna Wallach
    <member>Helen Faulkner
    <member>Margarita Manterola
    <member>Meike Reichle

  <job <a name="community" href="https://wiki.debian.org/Teams/Community"><gettext domain="organization">Community</gettext></a>> <genericemail community@debian.org> <genericemail antiharassment@debian.org> <delegationmail YeG6RzLnjBzS65vx@bluemosh.com>
    <member>Steve McIntyre <delegate_he_him>
    <member>Luke W. Faraone <delegate_they_them>
    <member>Jean-Philippe MENGUAL <delegate_he_him>
    <member>Molly de Blanc <delegate_she_her>
    <member>Sruthi Chandran <delegate_she_her>
    <member>Andrew Cater <delegate_he_him>
    <note <gettext domain="organization">To send a private message to all the members of the Community Team, use the GPG key <a href="community-team-pubkey.txt">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>.</gettext>>

  <job <a name="events" href="$(HOME)/events/"><gettext domain="organization">Events</gettext></a>> <genericemail events@debian.org>
    <member>Alexander Wirt
    <member>Franziska Lichtblau
    <member>Geert Stappers
    <member>Luca Capello
    <member>Paulo Henrique de Lima Santana

  <job <a name="debconf" href="https://www.debconf.org/"><gettext domain="organization">DebConf Committee</gettext></a>> <genericemail debconf-committee@debian.org> <delegationmail tslwobtb8el.fsf@suchdamage.org>
    <member>Daniel Lange<delegate_male>
    <member>Stefano Rivera<delegate_male>
    <member>Antonio Terceiro<delegate_male>
    <member>Gunnar Wolf<delegate_male>
    <member>Bernelle Verster<delegate_female>

  <job <a name="partners" href="$(HOME)/partners/"><gettext domain="organization">Partner Program</gettext></a>> <genericemail partners@debian.org>
    <member>Andre Felipe Machado
    <member>Laura Arjona Reina

  <job <a name="donations-hardware" href="$(HOME)/donations#equipment_donations"><gettext domain="organization">Hardware Donations Coordination</gettext></a>> <genericemail hardware-donations@debian.org>
    <member>Bernd Zeimetz
    <member>Martin Zobel-Helas
    <member>Benjamin Mako Hill
    <member>Rob Bradford

</ul>

<a name="memberships"></a>
<h3><gettext domain="organization">Membership in other organizations</gettext></h3>

<p>Debian is a member of the following organizations, either directly
or through <a href="https://www.spi-inc.org/">SPI</a>.</p>

<ul>
  <job <a name="org-gnome"><gettext domain="organization">GNOME Foundation</gettext></a>>
    <representative>Matthew Garrett
  <job <a name="org-lpi"><gettext domain="organization">Linux Professional Institute</gettext></a>>
  <job <a name="org-linux-mag"><gettext domain="organization">Linux Magazine</gettext></a>>
    <representative-role>Advisory Council
  <job <a name="org-linuxbase"><gettext domain="organization">Linux Standards Base</gettext></a>>
  <job <a name="org-freestandards"><gettext domain="organization">Free Standards Group</gettext></a>>
  <job <a name="org-oasis-open"><gettext domain="organization">OASIS: Organization
      for the Advancement of Structured Information Standards</gettext></a>>
    <representative>Mark Johnson
  <job <a name="org-oval"><gettext domain="organization">OVAL: Open Vulnerability
      Assessment Language</gettext></a>>
    <representative>Javier Fern&aacute;ndez-Sanguino
  <job <a name="org-osi"><gettext domain="organization">Open Source Initiative</gettext></a>>
</ul>

<a name="support"></a>
<h3><gettext domain="organization">Support and Infrastructure</gettext></h3>

<ul>
  <job <a name="bugs" href="https://bugs.debian.org/"><gettext domain="organization">Bug Tracking System</gettext></a>> <genericemail owner@bugs.debian.org>
    <member>Don Armstrong
    <member>Blars Blarson
    <member>Josip Rodin
    <member>Colin Watson
  <job <a name="lists" href="$(HOME)/MailingLists/"><gettext domain="organization">Mailing Lists Administration and Mailing List Archives</gettext></a>> <genericemail listmaster@lists.debian.org>
    <member>Alexander Wirt
    <member>Cord Beermann
    <member>David Moreno Garza
    <member>Don Armstrong
    <member>Martin Zobel-Helas
    <member>Pascal Hakim
    <member>Hanno Wagner
  <job <a name="nmfrontdesk" href="$(HOME)/devel/join/newmaint#FrontDesk"><gettext domain="organization">New Members Front Desk</gettext></a>> <genericemail nm@debian.org> <delegationmail 20210309115941.388475b1@commodore.bluemosh.com>
    <member>Jonathan McDowell<delegate_he_him>
    <member>Mattia Rizzolo<delegate_male>
    <member>Santiago Ruano Rincón<delegate_male>
    <member>Tobias Frost<delegate_male>
    <member>Pierre-Elliott Bécue<delegate_male>
    <member>Sam Hartman<delegate_male>
    <member>Stuart Prescott<delegate_male>
    <member>Nicolas Dandrimont<delegate_male>
  <job <a name="dam" href="$(HOME)/devel/join/newmaint#DAM"><gettext domain="organization">Debian Account Managers</gettext></a>> <genericemail da-manager@debian.org> <delegationmail YhtGLlghl4F45AbI@bluemosh.com>
    <member>Enrico Zini<delegate_male>
    <member>Joerg Jaspert<delegate_male>
    <member>Jonathan Wiltshire<delegate_male>
    <member>Nicolas Dandrimont<delegate_male>
    <member>Stuart Prescott<delegate_male>
    <note <gettext domain="organization">To send a private message to all DAMs, use the GPG key 57731224A9762EA155AB2A530CA8D15BB24D96F2.</gettext>>
  <job <a name="keyring" href="https://keyring.debian.org/"><gettext domain="organization">Keyring Maintainers (PGP and GPG)</gettext></a>> <genericemail keyring-maint@debian.org> <delegationmail 1535701657.2203656.1492256048.477439EA@webmail.messagingengine.com>
    <member>Gunnar Wolf<delegate_male>
    <member>John Sullivan<delegate_male>
    <member>Jonathan McDowell<delegate_he_him>
  <job <a name="security" href="$(HOME)/security/"><gettext domain="organization">Security Team</gettext></a>> <genericemail team@security.debian.org>
    <member>Alberto Garcia
    <member>Alessandro Ghedini
    <member>Aron Xu
    <member>Florian Weimer
    <member>Markus Koschany
    <member>Moritz Muehlenhoff
    <member>Raphael Geissert
    <member>Salvatore Bonaccorso
    <member>Sebastien Delafond
    <member>Thijs Kinkhorst
    <member>Yves-Alexis Perez
  <job <a name="policy" href="$(DOC)/debian-policy/"><gettext domain="organization">Policy</gettext></a>> <genericemail debian-policy@lists.debian.org> <delegationmail 1534924310.3924040.1482221768.47BABED0@webmail.messagingengine.com>
    <member>Russ Allbery<delegate_male>
    <member>Sean Whitton<delegate_male>
  <job <a name="dsa" href="https://dsa.debian.org/"><gettext domain="organization">System Administration</gettext></a>> <genericemail debian-admin@lists.debian.org> <delegationmail tsla75tdtza.fsf@suchdamage.org>
    <note <gettext domain="organization">This is the address to use when encountering problems on one of Debian's machines, including password problems or you need a package installed.</gettext>>
    <member>Adam D. Barratt<delegate_male>
    <member>Aurelien Jarno<delegate_male>
    <member>Héctor Orón Martínez<delegate_male>
    <member>Julien Cristau<delegate_male>
    <member>Luca Filipozzi<delegate_male>
    <member>Paul Wise<delegate_male>
    <member>Peter Palfrader<delegate_male>
    <member>Tollef Fog Heen<delegate_male>
    <note <gettext domain="organization">If you have hardware problems with Debian machines, please see <a href="https://db.debian.org/machines.cgi">Debian Machines</a> page, it should contain per-machine administrator information.</gettext>>
  <job <a name="db" href="https://db.debian.org/"><gettext domain="organization">LDAP Developer Directory Administrator</gettext></a>> <genericemail admin@db.debian.org>
  <job <a name="mirrors" href="$(HOME)/mirror/"><gettext domain="organization">Mirrors</gettext></a>> <genericemail mirrors@debian.org>
    <member>Peter Palfrader
    <member>Julien Cristau
  <job <a name="dns"><gettext domain="organization">DNS Maintainer</gettext></a>> <genericemail hostmaster@debian.org>
  <job <a name="pts" href="https://packages.qa.debian.org/"><gettext domain="organization">Package Tracking System</gettext></a>> <genericemail owner@packages.qa.debian.org>
    <member>Rapha&euml;l Hertzog
  <job <a name="treasurer" href="https://wiki.debian.org/Teams/Treasurer"><gettext domain="organization">Treasurer</gettext></a>> <genericemail treasurer@debian.org> <delegationmail 20160416194934.GD2171@halon.org.uk> <appointmentmail YhvdjOE02CVIasWu@bluemosh.com>
    <member>Brian Gupta<delegate_male>
    <member>Philipp Hug<delegate_male>
    <member>Hector Oron<delegate_male>
    <member>Daniel Lange<delegate_male>
  <job <gettext domain="organization"><a name="trademark" href="m4_HOME/trademark">Trademark</a> use requests</gettext>> <genericemail trademark@debian.org> <delegationmail tsl1rold9al.fsf@suchdamage.org>
    <member>Brian Gupta<delegate_he_him>
    <member>Taowa<delegate_they_them>
  <job <a name="salsa" href="https://salsa.debian.org/"><gettext domain="organization">Salsa administrators</gettext></a>> <genericemail salsa-admin@debian.org> <delegationmail 20210512000735.3cf97a1d@commodore.bluemosh.com>
    <member>Alexander Wirt
    <member>Joerg Jaspert
</ul>
