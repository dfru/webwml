#use wml::debian::translation-check translation="9e2bd6fdfda60b03320893da8be2d9ad75fedff5"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se informó de que las implementaciones de los perfiles HID y HOGP de BlueZ
no requieren específicamente que se vinculen el dispositivo y el anfitrión.
Dispositivos maliciosos pueden aprovecharse de este defecto para conectarse a un
anfitrión y suplantar a un dispositivo HID existente sin seguridad o para provocar
que tenga lugar un descubrimiento de servicio SDP o GATT, lo que permitiría la inyección
de informes HID en el subsistema de entrada desde un origen no vinculado.</p>

<p>Para el caso del perfil HID se ha añadido una nueva opción de configuración
(ClassicBondedOnly) para asegurarse de que las conexiones entrantes procedan exclusivamente de
dispositivos vinculados. La opción toma por omisión el valor <q>false</q> para maximizar la
compatibilidad de los dispositivos.</p>

<p>Para la distribución «antigua estable» (stretch), este problema se ha corregido
en la versión 5.43-2+deb9u2.</p>

<p>Para la distribución «estable» (buster), este problema se ha corregido en
la versión 5.50-1.2~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de bluez.</p>

<p>Para información detallada sobre el estado de seguridad de bluez, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/bluez">\
https://security-tracker.debian.org/tracker/bluez</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4647.data"
