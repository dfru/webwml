#use wml::debian::cdimage title="Instalación por red desde un CD mínimo"
#use wml::debian::release_info
#use wml::debian::installer
#use wml::debian::translation-check translation="e01f641181acd20b6afc24e482e4360c075e4712" 
#include "$(ENGLISHDIR)/releases/images.data"

<p>Un CD de <q>instalación por red</q> o <q>netinst</q> es un único CD que
posibilita que instale el sistema completo. Este único CD contiene
sólo la mínima cantidad de software para instalar el sistema base y
obtener el resto de paquetes a través de Internet.</p>

<p><strong>¿Qué es lo mejor para mí: el CD mínimo arrancable o los CD
completos?</strong> Depende, pero pensamos que en muchos casos la
imagen del CD mínimo es lo mejor: sobre todo, sólo tiene que
descargar los paquetes que seleccionó para instalarlos en su
máquina, lo que ahorra tanto tiempo como ancho de banda. Por otro
lado, los CD completos son más recomendables cuando tiene que
instalar en más de una máquina, o en máquinas sin una conexión libre a
Internet.</p>

<p><strong>¿Qué tipo de conexiones de red se pueden usar durante la instalación?</strong>
La instalación por red asume que usted cuenta con conexión a Internet. Puede ser
de varias maneras: con una conexión analógica PPP, ethernet, red inalámbrica (con algunas
restricciones), pero no por RDSI (¡lo sentimos!).</p>

<p>Las siguientes imágenes de CD mínimas de arranque están disponibles
para descarga:</p>

<ul>

 <li>Imágenes oficiales <q>netinst</q> para la publicación <q>estable</q> &mdash; <a
  href="#netinst-stable">mire más abajo</a></li>

  <li>Imágenes para la publicación <q>en pruebas</q>, tanto las construcciones
  diarias como las instantáneas de trabajo conocidas, mire la <a
  href="$(DEVEL)/debian-installer/">página del instalador de Debian</a>.</li>
 </ul>


<h2 id="netinst-stable">Imágenes oficiales <q>netinst</q> para la publicación <q>estable</q></h2>

<p>Hasta 300&nbsp;MB de tamaño, esta imagen contiene el instalador y 
y un pequeño conjunto de paquetes que le permiten la instalación de un sistema
(muy) básico.</p>

<div class="line">
<div class="item col50">
<p><strong>
Imagen de CD netinst (normalmente 150-300 MB, varía según arquitectura)
</strong></p>
	  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>Imagen de CD netinst (usando <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)</strong></p>
  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Para información sobre estos archivos y cómo usarlos, por favor vea
las <a href="../faq/">PF</a> (Preguntas Frecuentes).</p>

<p>Una vez se haya bajado las imágenes, asegúrese de leer la
<a href="$(HOME)/releases/stable/installmanual">información detallada
sobre el proceso de instalación</a>.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Imágenes no oficiales <q>netinst</q> con «firmware» no libre incluido</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Si algún componente hardware de su sistema <strong>requiere cargar «firmware»
no libre</strong> con el controlador de dispositivo, puede usar uno de los
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archivos comprimidos con paquetes de «firmware» común</a> o descargar una imagen <strong>no oficial</strong>
que incluya estos «firmwares» <strong>no libres</strong>. En la <a href="../../releases/stable/amd64/ch06s04">guía de instalación</a> puede encontrar
instrucciones sobre cómo usar los archivos comprimidos e información general sobre cómo cargar
el «firmware» durante la instalación.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">imágenes
no oficiales de instalación de <q>estable</q> con «firmware» incluido</a>
</p>
</div>
