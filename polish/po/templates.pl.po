# translation of templates.pl.po to polski
# Wojciech Zareba <wojtekz@comp.waw.pl>, 2007, 2008.
# Marcin Owsiany <porridge@debian.org>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: templates.pl\n"
"PO-Revision-Date: 2014-08-01 02:58+0200\n"
"Last-Translator: Marcin Owsiany <porridge@debian.org>\n"
"Language-Team: polski <debian-l10n-polish@lists.debian.org>\n"
"Language: polish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.6\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Witryna projektu"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Przeszukaj witrynę Debiana."

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "Przeszukiwanie witryny Debiana"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Tak"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Nie"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Projekt Debian"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian to system operacyjny i dystrybucja Wolnego Oprogramowania. Opiekuje "
"się nią wielu użytkowników, którzy poświęcają jej swój czas i wysiłek."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, otwarte źródła, wolne, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "Powrót do <a href=\"m4_HOME/\">strony głównej Debiana</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Strona Główna"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Pomiń szybką nawigację"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "O&nbsp;Debianie"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "O&nbsp;Debianie"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Skontaktuj się z nami"

#: ../../english/template/debian/common_translation.wml:37
#, fuzzy
msgid "Legal Info"
msgstr "Informacje o wydaniach"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr ""

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Dary"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Wydarzenia"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Wiadomości"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Dystrybucja"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Pomoc"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr ""

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Kącik deweloperów"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Dokumentacja"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Bezpieczeństwo"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Wyszukiwanie"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "żadne"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Idź"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "ogólnoświatowe"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Mapa serwisu"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Różne"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Jak zdobyć Debiana"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "Blog Debiana"

#: ../../english/template/debian/common_translation.wml:94
#, fuzzy
#| msgid "Debian Project News"
msgid "Debian Micronews"
msgstr "Wiadomości Projektu Debian"

#: ../../english/template/debian/common_translation.wml:97
#, fuzzy
#| msgid "Debian Project"
msgid "Debian Planet"
msgstr "Projekt Debian"

#: ../../english/template/debian/common_translation.wml:100
#, fuzzy
msgid "Last Updated"
msgstr "Ostatnia modyfikacja"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Prosimy przesyłać wszelkie komentarze, krytyczne uwagi i sugestie na temat "
"tych stron WWW na naszą <a href=\"mailto:debian-doc@lists.debian.org\">listę "
"dyskusyjną</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "niepotrzebny"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "niedostępne"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "nie dotyczy"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "w wersji 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "w wersji 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "w wersji 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "w wersji 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "w wersji 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:89
#, fuzzy
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"Aby zgłosić problem ze stroną WWW, prześlij wiadomość po angielsku na <a "
"href=\"mailto:debian-www@lists.debian.org\">debian-www@lists.debian.org</a> "
"lub po polsku na <a href=\"mailto:debian-l10n-polish@lists.debian.org"
"\">debian-l10n-polish@lists.debian.org</a>. Inne informacje kontaktowe "
"możesz uzyskać na <a href=\"m4_HOME/contact\">stronie kontaktowej</a> "
"Debiana. Dostępne są <a href=\"m4_HOME/devel/website/using_cvs\">źródła "
"strony WWW</a>."

#: ../../english/template/debian/footer.wml:92
msgid "Last Modified"
msgstr "Ostatnia modyfikacja"

#: ../../english/template/debian/footer.wml:95
msgid "Last Built"
msgstr ""

#: ../../english/template/debian/footer.wml:98
msgid "Copyright"
msgstr "Copyright"

#: ../../english/template/debian/footer.wml:101
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> i inni;"

#: ../../english/template/debian/footer.wml:104
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr "Zobacz <a href=\"m4_HOME/license\" rel=\"copyright\">warunki umowy</a>"

#: ../../english/template/debian/footer.wml:107
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian jest zarejestrowanym <a href=\"m4_HOME/trademark\">znakiem handlowym</"
"a> Software in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Ta strona jest również dostępna w następujących językach:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "Jak ustawić <a href=m4_HOME/intro/cn>domyślny język dokumentu</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr ""

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr ""

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Różne języki w Debianie"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Partnerzy"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Cotygodniowe wiadomości Debiana"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Wiadomości cotygodniowe"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Wiadomości Projektu Debian"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Wiadomości Projektu"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Informacje o wydaniach"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Pakiety Debiana"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Pobieranie"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;na&nbsp;płytach&nbsp;CD"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Książki o Debianie"

#: ../../english/template/debian/links.tags.wml:37
#, fuzzy
msgid "Debian Wiki"
msgstr "Debian"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Archiwa List Dyskusyjnych"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Listy Dyskusyjne"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Umowa Społeczna"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Kodeks Postępowania"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - Uniwersalny system operacyjny"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Mapa witryny Debiana"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Baza danych developerów"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "FAQ Debiana"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Podręcznik polityki Debiana"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Poradnik dla developerów"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Przewodnik dla początkujących opiekunów pakietów"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Błędy krytyczne dystrybucji"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Raporty lintiana"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Archiwa list dla użytkowników"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Archiwa list dla developerów"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Archiwa list związanych z obsługą różnych języków"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Archiwa list dyskusyjnych dotyczących adaptacji"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Archiwa list związanych z systemem śledzenia błędów"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Archiwa pozostałych list dyskusyjnych"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Wolne oprogramowanie"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Rozwój"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Jak pomóc Debianowi"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Zgłoszenia błędów"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Adaptacje/Architektury"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Podręcznik instalacji"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Sprzedawcy płyt"

#: ../../english/template/debian/links.tags.wml:125
#, fuzzy
msgid "CD/USB ISO images"
msgstr "Obrazy płyt"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Instalacja przez sieć"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Preinstalowany"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Projekt Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr "Alioth &ndash; Debian GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Zapewnienie jakości"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "System śledzenia pakietów"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Przegląd pakietów developerów Debiana"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Strona domowa Debiana"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Brak wydarzeń w tym roku."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "proponowane"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "dyskutowane"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "głosowanie otwarte"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "zakończone"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "wycofane"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Przyszłe wydarzenia"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Poprzednie wydarzenia"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(nowa wersja)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "Data Zgłoszenia"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr ""

#: ../../english/template/debian/redirect.wml:12
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s dla architektury %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Uwaga:</em> <a href=\"$link\">Oryginał</a> jest nowszy niż to "
"tłumaczenie."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Uwaga! To tłumaczenie jest przestarzałe, prosimy przejść do <a href=\"$link"
"\">oryginału</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>Uwaga:</em> Oryginalny dokument tego tłumaczenia już nie istnieje."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "Adres internetowy"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Powrót do <a href=\"../\">strony użytkowników Debiana</a>."

#~ msgid "Visit the site sponsor"
#~ msgstr "Odwiedź sponsora Strony"
