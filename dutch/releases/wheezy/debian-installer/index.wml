#use wml::debian::template title="Debian &ldquo;wheezy&rdquo; Installatie-informatie" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/wheezy/release.data"
#use wml::debian::translation-check translation="f36546f515e33fb0e590b3db17a516bf3d605f5f"

<h1>Debian installeren <current_release_wheezy></h1>

<if-stable-release release="jessie">
<p><strong>Debian 7 werd vervangen door
<a href="../../jessie/">Debian 8 (<q>jessie</q>)</a>. Sommige van de
installatie-images hieronder zijn mogelijk niet langer beschikbaar of werken
niet meer. Het wordt aanbevolen om in de plaats daarvan jessie te
installeren.
</strong></p>
</if-stable-release>

<p>
<strong>Voor het installeren van Debian</strong> <current_release_wheezy>
(<em>wheezy</em>) kunt u een van de volgende images downloaden (alle i386 en amd64
cd/dvd-images zijn ook bruikbaar op USB-stick):
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst cd-image (meestal 150-280 MB)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>volledige cd-sets</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>volledige dvd-sets</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>dvd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>andere images (netboot, flexibele usb-stick, enz.)</strong></p>
<other-images />
</div>
</div>

<div id="firmware_nonfree" class="warning">
<p>
Indien een hardwareonderdeel van uw systeem <strong>het laden van niet-vrije firmware
vereist</strong> voor het stuurprogramma van een apparaat, kunt u een van de
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/wheezy/current/">\
tar-archieven met gebruikelijke firmwarepakketten</a>  gebruiken of een <strong>niet-officieel</strong> image downloaden met deze <strong>niet-vrije</strong> firmware. Instructies over het gebruik van deze tar-archieven en algemene informatie
over het laden van firmware tijdens de installatie is te vinden in de
Installatiehandleiding (zie onder Documentatie hierna).
</p>
<div class="line">
<div class="item col50">
<p><strong>netinst (gewoonlijk 240-290 MB) <strong>niet-vrije</strong>
cd-images <strong>met firmware</strong></strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>Opmerkingen</strong>
</p>
<ul>
    <li>
	Voor het downloaden van volledige cd- of dvd-images wordt het gebruik van
	BitTorrent of jigdo aanbevolen.
    </li><li>
	Voor de minder gebruikelijke architecturen is enkel een beperkt aantal
	images uit de cd- of dvd-set beschikbaar als ISO-bestand of via BitTorrent.
	De volledige sets zijn enkel via jigdo beschikbaar.
    </li><li>
	De multi-arch <em>cd</em>-images zijn bedoeld voor i386/amd64; de
    installatie is vergelijkbaar met een installatie met een netinst-image
	voor één enkele architectuur.
    </li><li>
	Het multi-arch <em>dvd</em>-image is bedoeld voor i386/amd64; de
	installatie is vergelijkbaar met een installatie met een
	volledig cd-image voor één enkele architectuur. De dvd bevat ook
	al de broncode voor de opgenomen pakketten.
    </li><li>
	Voor de installatie-images zijn verificatiebestanden (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> en andere) te vinden in dezelfde map als de images.
    </li>
</ul>


<h1>Documentatie</h1>

<p>
<strong>Indien u slechts één document leest</strong> voor u met installeren
begint, lees dan onze
<a href="../i386/apa">Installatie-Howto</a> met een snel
overzicht van het installatieproces. Andere nuttige informatie is:
</p>

<ul>
<li><a href="../installmanual">Wheezy Installatiehandleiding</a><br />
met uitgebreide installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
en <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
met algemene vragen en antwoorden</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
met door de gemeenschap onderhouden documentatie</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Dit is een lijst met gekende problemen in het installatiesysteem van
Debian <current_release_wheezy>. Indien u bij het installeren van Debian op
een probleem gestoten bent en dit probleem hier niet vermeld vindt, stuur ons dan een
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">installatierapport</a>
waarin u de problemen beschrijft of
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">raadpleeg de wiki</a>
voor andere gekende problemen.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata voor release 7.0</h3>

<dl class="gloss">

	<dt>De installatie van het opstartprogramma GRUB kan mislukken als er meer dan één schijf beschikbaar is.</dt>
	<dd>Wanneer tijdens de installatie meer dan één schijf beschikbaar is
	(bijvoorbeeld een harde schijf en een USB-stick, zoals gewoonlijk het geval is
    wanneer het installatiesysteem vanaf een USB-stick opgestart wordt), dan kan
	<code>grub-install</code> in de problemen komen: er werd verschillende keren
    gemeld dat het opstartprogramma GRUB op de USB-stick geïnstalleerd werd
    in plaats van op de harde schijf met daarop het pas geïnstalleerde systeem.
	<br />
	Om te vermijden dat u in een dergelijke situatie terecht komt, moet u er voor
    zorgen <q>Nee/No</q> te antwoorden wanneer tijdens het installatieproces
    volgende vraag gesteld wordt: <q>Install the GRUB boot loader to the
	master boot record?</q> (De Grub-bootloader in het master boot record
    installeren?); bij de volgende stap is het dan mogelijk om het juiste
    apparaat op te geven: <q>Device for boot loader installation</q> (Apparaat
    waarop de bootloader geïnstalleerd moet worden).
	<br />
	Indien de installatie succesvol eindigde ondanks een foutieve configuratie
    van GRUB, zou het mogelijk moeten zijn om dit te herstellen met de
    herstelmodus van het installatiesysteem: chroot in het rootbestandssysteem,
    koppel de eventuele extra bootpartitie aan (zoals <code>/boot</code> en/of
	<code>/boot/efi</code> voor een EFI-systeem, zie <code>/etc/fstab</code>) en
	voer <code>grub-install</code> uit met de juiste schijf als parameter.
	<br />
	<b>Status:</b> Dit probleem zal hopelijk opgelost worden bij de tweede
    tussenrelease van Wheezy.
	</dd>

	<dt>Desktopinstallaties op i386 met enkel CD#1 functioneren niet</dt>
	<dd>Wegens plaatsgebrek op de eerste cd, passen niet alle verwachte GNOME
    desktoppakketten op CD#1. Voor een succesvolle installatie moet u extra
    pakketbronnen gebruiken (bijv. een tweede cd of een netwerkspiegelserver)
    of een dvd.
	<br />
	<b>Status:</b> Het is onwaarschijnlijk dat nog iets extra mogelijk is om
    meer pakketten op CD#1 te krijgen.
	</dd>

	<dt>Mogelijke problemen met UEFI-opstart op amd64</dt>
	<dd>We ontvingen enkele rapporten over problemen met het opstarten van het
    Debian installatiesysteem in UEFI-modus op amd64-systemen. Sommige systemen
    blijken niet goed op te starten met <code>grub-efi</code> en bij sommige
    andere gaat de grafische weergave fout bij het weergeven van het
    initiële welkomstscherm van de installatie.
	<br />
	Indien u een van deze problemen ervaart, stuur ons dan een bugrapport
    en geef ons zoveel mogelijk details, zowel over de symptomen als over
    uw hardware - dit zou het team moeten helpen deze fouten te repareren. Voorlopig
    kunt u dit probleem vermijden door UEFI uit te zetten en de installatie uit te voeren
    met het <q>oudere BIOS</q> of in <q>Fallback-modus</q> (noodmodus).
	<br />
	<b>Status:</b> Mogelijk komen er extra bugreparaties tijdens de verschillende
    tussenreleases van Wheezy.
	</dd>

	<dt>Foutieve prompt in verband met firmware voor sommige Wifi-kaarten van Intel</dt>

	<dd>Indien u een Intel Wireless 6005 of 6205 kaart heeft, zal het
    installatiesysteem naar het firmware-bestand <code>iwlwifi-6000g2a-6.ucode</code> vragen. Dit bestand zit niet in het pakket <code>firmware-iwlwifi</code>
    en is ook niet nodig. U moet <code>no/neen</code> antwoorden om de installatie
    verder te zetten.
	<br />
	<b>Status:</b> Dit probleem werd gerepareerd in de eerste tussenrelease van Wheezy.
	</dd>

	<dt>Geen pieptoon bij het opstarten van het installatiesysteem</dt>

	<dd>In het kader van toegankelijkheid moet er een pieptoon te horen
    zijn wanneer het opstartprogramma van het installatiesysteem op invoer
    wacht. Een bug deed deze jammer genoeg verdwijnen in de eerste Alpha release
    van Wheezy en dit werd pas opgemerkt net voor de derde Wheezy Releasekandidaat.
	<br />
	<b>Status:</b> Deze fout is rechtgezet in de eerste tussenrelease van Wheezy.
	</dd>

<!-- leaving this in for possible future use...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

</dl>
<if-stable-release release="wheezy">
<p>
Voor de volgende release van Debian wordt gewerkt aan een verbeterde versie
van het installatiesysteem, die u ook kunt gebruiken om wheezy te installeren.
Raadpleeg voor de concrete informatie
<a href="$(HOME)/devel/debian-installer/">de pagina van het Debian-Installer project</a>.
</p>
</if-stable-release>
