#use wml::debian::template title="Een kandidaat-lid voordragen"
#use wml::debian::translation-check translation="a0723d79bbfc5ed2266ada5ffd402b2d3ab47405"

<p>Voordat een kandidaat-lid voordraagt, moet u nagaan of het voldoet aan
alle zaken die vermeld staan op de <a href="./nm-checklist">NM-checklist</a>.
</p>

<p>U moet <em>alleen</em> iemand voordragen als u denkt dat die persoon klaar is
om lid van het project te worden. Dit betekent dat u deze persoon al enige tijd
kent en diens werk kunt beoordelen. Het is belangrijk dat kandidaat-leden al
enige tijd in het project meewerken. Enkele manieren waarop kandidaat-leden een
bijdrage kunnen leveren zijn:
</p>

<ul>
<li>een pakket (of meerdere) onderhouden,</li>
<li>gebruikers helpen,</li>
<li>publiciteitswerkzaamheden,</li>
<li>vertalingen,</li>
<li>organisatorische werkzaamheden voor debconf,</li>
<li>patches indienen,</li>
<li>bugrapporten insturen,</li>
<li>... en nog zoveel andere dingen!</li>
</ul>

<p>Deze manieren van helpen sluiten elkaar natuurlijk niet uit!  Veel
kandidaat-leden zullen werk hebben verricht in verschillende domeinen van het
project, en niet iedereen hoeft een verpakker van software te zijn. Denk eraan
dat <a href="https://www.debian.org/vote/2010/vote_002">Debian ook medewerkers
die geen pakketbeheerder zijn verwelkomt als lid van het project</a>.
Voor een ruimer zicht op een bepaalde medewerker bent u misschien
geïnteresseerd in <a href="http://ddportfolio.debian.net/">hulpmiddelen waarmee
enkele publiekelijk zichtbare delen van het project worden gebundeld</a>.
Ook uw persoonlijke kennis van de kandidaat is hier van belang.
</p>

<p>Het kandidaat-lid zou vertrouwd moeten zijn met de speciale manier van
werken van Debian en zou al actief en effectief moeten hebben bijgedragen aan
het project. Het belangrijkste is dat het toegewijd is aan de idealen en de
structuur van het Debian-project. Vraag uzelf af of u het in Debian wil zien
&ndash; als u denkt dat het een ontwikkelaar van Debian zou moeten zijn, ga er
dan voor en moedig het aan om zich kandidaat te stellen.</p>

<p>Voer vervolgens de volgende stappen uit: spreek met het kandidaat-lid af dat
u hem/haar zult aanbevelen en laat hem/haar zich
<a href="https://nm.debian.org/public/newnm">aanmelden</a>. Daarna moet u op
zijn/haar naam klikken in de <a href="https://nm.debian.org/process/">lijst van
kandidaten</a> en naar de pagina <q>Deze kandidatuur voordragen</q> gaan. Voer
uw Debian inloggegevens in en druk op de knop <q>Voordragen</q>. U ontvangt dan
een e-mail met een authenticatie-sleutel die u ondertekend met PGP/GPG moet
terugsturen. Hierna wordt de kandidaat toegevoegd aan de wachtrij voor de
toewijzing van een beheerder van kandidaat-leden met wie hij/zij alle stappen
van de controle van nieuwe leden zal doorlopen.
</p>

<p>
Raadpleeg ook de wikipagina met <a href="https://wiki.debian.org/FrontDesk/AdvocacyTips">Tips voor het voordragen van kandidaten</a>.
</p>
