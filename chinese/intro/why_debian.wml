#use wml::debian::template title="选择 Debian 的理由" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Vifly"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">面向用户的 Debian</a></li>
    <li><a href="#devel">面向开发者的 Debian</a></li>
    <li><a href="#enterprise">面向企业环境的 Debian</a></li>
  </ul>
</div>

<p>
有很多理由可以选择 Debian 作为您的操作系统——作为用户、作为\
开发者，甚至是在企业环境中。大多数\
用户称赞它的稳定性，以及软件包和发行版的平滑的升级过程。\
Debian 也被软件和硬件开发人员广泛使用，\
因为它能运行在众多架构和设备上，\
提供了一个公开的缺陷跟踪系统，以及面向开发人员的其他工具。\
如果您在专业环境中使用 Debian，\
您还可以享受到诸如 LTS 版本和云映像带来的额外好处。
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> 对我而言，使用 Debian 的原因是它完美的易用性和稳定性。这些年来，我使用了各种不同的发行版，但是 Debian 是唯一一个能够完美使用的发行版。<a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest 在 Reddit 上写道</a></p>
</aside>

<h2><a id="users">面向用户的 Debian</a></h2>

<dl>
  <dt><strong>Debian 是自由软件。</strong></dt>
  <dd>
    Debian 是由自由和开放源代码的软件组成的，并将始终\
    保持 100% <a href="free">自由</a>。\
    每个人都能自由使用、修改，以及分发。这是我们对<a href="../users">我们的用户</a>的主要\
    承诺。它也是免费的。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 稳定且安全。</strong></dt>
  <dd>
    Debian 是一个广泛用于各种设备的基于 Linux 的操作系统，其使用范围\
    包括[CN:笔记本电脑:][HK:手提電腦:][TW:筆記型電腦:]，台式机和服务器。
    我们为每个软件包提供合理的默认配置，\并在软件包的生命周期内提供常规的安全更新。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 具有广泛的硬件支持。</strong></dt>
  <dd>
    大多数硬件已获得 Linux 内核的支持。这\
    意味着 Debian 也会\
    支持它们。如有需要，也可使用专有的硬件驱动程序。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 提供灵活的安装程序。</strong></dt>
  <dd>
    希望在安装前尝试 Debian 的用户可以使用我们\
    的 <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live
    CD</a>。它同时包含了 Calamares 安装程序，\
    使得从 Live 系统安装 Debian 变得十分容易。\
    经验更加丰富的用户可以使用 Debian 安装程序，\
    它提供了更多可以微调的选项，\
    包括使用自动化的网络安装工具的功能。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 提供平滑的更新。</strong></dt>
  <dd>
    保持操作系统最新十分容易，不论您是想\
    升级到一个全新的发布版本，还是只想升级一个单独的软件包。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 是许多其他发行版的基础。</strong></dt>
  <dd>
    许多非常受欢迎的 Linux 发行版，例如 Ubuntu、Knoppix、PureOS \
    以及 Tails，都基于 Debian。我们提供了所需的所有工具，\
    使得每个人在有需要的时候都可以制作自己\
    的软件包，以补充 Debian 档案库里没有的软件包。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 项目是一个社区。</strong></dt>
  <dd>
    所有人都可以成为 Debian 社区的一员；您\
    不必是一名开发者或系统管理员。Debian 有\
    一个<a href="../devel/constitution">民主的治理架构</a>。\
    由于所有 Debian 项目的成员都享有平等的权利，\
    所以 Debian 不能被单个公司所控制。我们的\
    开发人员来自超过 60 个国家/地区，\
    并且 Debian 本身也已经被翻译为超过 80 种语言。
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Debian 作为开发者使用的操作系统享有盛誉的原因是丰富的软件包数量和软件支持，这对开发者而言十分重要。强烈建议专业的程序员和系统管理员使用。<a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma 在 Fossbytes 上写道</a></p>
</aside>

<h2><a id="devel">面向开发者的 Debian</a></h2>

<dl>
  <dt><strong>多种硬件架构</strong></dt>
  <dd>
    Debian 支持<a href="../ports">一长串</a>的CPU架构，包括 AMD64、i386，\
    ARM 和 MIPS 的多个版本、POWER7、POWER8、IBM System z 以及 RISC-V。Debian 还可以\
    用于一些特殊用途的架构。
  </dd>
</dl>

<dl>
  <dt><strong>物联网和嵌入式设备</strong></dt>
  <dd>
    Debian 可以在各种设备上运行，例如 Raspberry Pi、\
    QNAP 的各个变种、移动设备、家庭路由器以及大量单板计算机\
    （SBC）。 
  </dd>
</dl>

<dl>
  <dt><strong>大量的软件包</strong></dt>
  <dd>
    Debian 拥有大量的<a
    href="$(DISTRIB)/packages">软件包</a>（当前的\
    稳定版本：<packages_in_stable> 个软件包） ，使用 <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">deb
    格式</a>。
  </dd>
</dl>

<dl>
  <dt><strong>不同的发布版本</strong></dt>
  <dd>
    除了我们的稳定版本外，您还可以通过安装测试版或不稳定版本\
    来获得更新版本的软件。 
  </dd>
</dl>

<dl>
  <dt><strong>公开的错误跟踪系统</strong></dt>
  <dd>
    我们的 Debian <a href="../Bugs">错误跟踪系统</a>（BTS）向所有人公开，\
    任何人都可通过浏览器访问。我们不会隐藏我们的软件错误，\
    您可以轻松提交新的错误报告或参与讨论。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 政策和开发人员工具</strong></dt>
  <dd>
    Debian 提供高质量的软件包。欲了解我们的质量标准，\
    请阅读我们的<a href="../doc/debian-policy/">政策</a>，\
    它规定了每个被发行版接受的软件包所必须满足\
    的技术需求。我们的持续集成策略\
    包括 Autopkgtest（对软件包进行测试），Piuparts（\
    测试安装、升级和删除），以及 Lintian（检查软件包的不一致\
    和错误）。
  </dd>
</dl> 

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> 稳定性是 Debian 的同义词。 [...] 安全是 Debian 最重要的特性之一。<a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis 在 pontikis.net 上写道</a></p>
</aside>

<h2><a id="enterprise">面向企业环境的 Debian</a></h2>

<dl>
  <dt><strong>Debian 是可靠的。</strong></dt>
  <dd>
    Debian 在从单个用户的[CN:笔记本电脑:][HK:手提電腦:][TW:筆記型電腦:]到\
    超级对撞机、证劵交易所和汽车行业的数以千计的现实日常场景\
    中证明其可靠性。\
    它在学术界、科研机构和公共部门中也很流行。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 有很多专家。</strong></dt>
  <dd>
    我们的软件包维护者不仅仅只为 Debian 打包软件包\
    和整合新的上游版本。他们\
    常常是该应用程序的专家，因此可以\
    直接为上游开发做出贡献。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 是安全的。</strong></dt>
  <dd>
    Debian 对其稳定版本提供安全支持。许多其它\
    发行版的开发人员和安全研究人员都依赖 Debian 的安全跟踪器。 
  </dd>
</dl>

<dl>
  <dt><strong>长期支持</strong></dt>
  <dd>
    Debian 提供的免费的<a href="https://wiki.debian.org/LTS">长期支持</a>（LTS）\
    版本将所有 Debian 稳定版本\
    的生命周期延长到至少 5 年。除此以外，还有商业的\
    <a href="https://wiki.debian.org/LTS/Extended">扩展 LTS</a> 计划，\
    该计划将对有限的软件包的支持延长到了 5 年以上。 
  </dd>
</dl>

<dl>
  <dt><strong>云映像</strong></dt>
  <dd>
    官方云映像可用于所有的主流云端平台。我们还\
    提供了工具和配置，因此您可以构建自己的自定义\
    云映像。您还可以在桌面或容器里的虚拟机中\
    使用 Debian。 
  </dd>
</dl>
