#use wml::debian::template title="Debian bekommen"
#use wml::fmt::isolatin
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="0691e0b35ed0aa5df10a4b47799051f77e519d25"
# $Id$
# Translator: Hans-Peter Hinrichs <hp@hphinrichs.de>
# Updated by: Thimo Neubauer <thimo@debian.org>
# Updated by: Helge Kreutzmann <debian@helgefjell.de>
# Updated: Holger Wansing <linux@wansing-online.de>, 2011, 2012, 2018.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2019, 2020, 2021.

<p>Debian wird <a href="../intro/free">frei</a> über das
Internet verteilt. Sie können es komplett von jedem unserer
<a href="ftplist">Spiegel</a> herunterladen.
Die <a href="../releases/stable/installmanual">Installationsanleitung</a>
enthält ausführliche Anweisungen zur Installation.
Desweiteren sind <a href="../releases/stable/releasenotes">hier</a> die
Veröffentlichungshinweise zu finden.
</p>

<p>Auf dieser Seite finden Sie alles zur Installation von Debian Stable. Falls Sie an
Testing oder Unstable interessiert sind, besuchen Sie die <a href="../releases/">Seite
unserer Veröffentlichungen</a>.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Ein Installations-Image herunterladen</a></h2>
    <p>Abhängig von Ihrer Internet-Verbindung können Sie eines der folgenden
       Images herunterladen:</p>
    <ul>
      <li>Ein <a href="netinst"><strong>kleines Installations-Image</strong></a>:
          kann schnell heruntergeladen werden und sollte auf einen
          Wechseldatenträger aufgebracht werden. Um dies zu nutzen,
          benötigen Sie auf dem zu installierenden Rechner eine
          Internet-Verbindung.
	<ul class="quicklist downlist">
	  <li><a title="Installer für 64-Bit Intel- und AMD-PC herunterladen"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-Bit-PC Netinst-ISO</a></li>
	  <li><a title="Installer für normale 32-Bit Intel- und AMD-PC herunterladen"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-Bit-PC Netinst-ISO</a></li>
	</ul>
      </li>
      <li>Ein größeres <a href="../CD/"><strong>vollständiges Installations-Image</strong></a>:
          enthält mehr Pakete; dies macht es einfacher, einen Rechner ohne
          Internet-Verbindung zu installieren.
	<ul class="quicklist downlist">
	  <li><a title="DVD-Torrents für 64-Bit Intel- und AMD-PC herunterladen"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-Bit-PC DVD-Torrents</a></li>
	  <li><a title="DVD-Torrents für normale 32-Bit Intel- und AMD-PC herunterladen"
		 href="<stable-images-url/>/i386/bt-dvd/">32-Bit-PC DVD-Torrents</a></li>
	  <li><a title="CD-Torrents für 64-Bit Intel- und AMD-PC herunterladen"
	         href="<stable-images-url/>/amd64/bt-cd/">64-Bit-PC CD-Torrents</a></li>
	  <li><a title="CD-Torrents für normale 32-Bit Intel- und AMD-PC herunterladen"
		 href="<stable-images-url/>/i386/bt-cd/">32-Bit-PC CD-Torrents</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Ein Debian-Cloud-Image verwenden</a></h2>
    <p>Ein offizielles <a href="https://cloud.debian.org/images/cloud/"><strong>Cloud-Image</strong></a>,
       erstellt vom Cloud-Team, kann verwendet werden:</p>
    <ul>
      <li>bei Ihrem OpenStack-Anbieter, im qcow2- oder raw-Format.
      <ul class="quicklist downlist">
	   <li>64-Bit AMD/Intel (<a title="OpenStack-Image für 64-bit AMD/Intel - qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2">qcow2</a>,
               <a title="OpenStack-Image für 64-bit AMD/Intel - raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.raw">raw</a>)</li>
           <li>64-Bit ARM (<a title="OpenStack-Image für 64-Bit ARM - qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.qcow2">qcow2</a>,
               <a title="OpenStack-Image für 64-Bit ARM - raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.raw">raw</a>)</li>
	   <li>64-Bit Little-Endian PowerPC (<a title="OpenStack-Image für 64-Bit Little-Endian PowerPC - qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.qcow2">qcow2</a>,
               <a title="OpenStack-Image für 64-Bit Little-Endian PowerPC - raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>bei Amazon EC2, entweder als Maschinen-Image oder über den AWS Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Amazon Machinen-Images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon Machinen-Images</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	   </ul>
      </li>
      <li>bei Microsoft Azure, über den Azure Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 11 für Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 (<q>Bullseye</q>)</a></li>
        <li><a title="Debian 10 für Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-10?tab=PlansAndPrice">Debian 10 (<q>Buster</q>)</a></li>
	   </ul>
      </li>
    </ul>
 </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Kaufen Sie einen Satz CDs von einem der
    Händler, die Debian-CDs vertreiben</a></h2>

   <p>
      Viele der Händler verkaufen die Distribution für weniger als
      5,-&nbsp;€ plus Porto (sollten Sie im Ausland bestellen wollen,
      prüfen Sie deren Webseiten, um zu sehen, ob
      diese international versenden).
      <br />
      Einige der <a href="../doc/books">Bücher über Debian</a> enthalten
      ebenfalls CDs.
   </p>

   <p>Hier einige der grundsätzlichen Vorteile von CDs:</p>

   <ul>
     <li>Die Installation von einer CD verläuft geradliniger.</li>
     <li>Sie können auf Rechnern installieren, die keine Internetanbindung
         haben.</li>
	 <li>Sie können Debian (auf so vielen Maschinen, wie Sie möchten) 
             installieren, ohne alle Pakete selbst herunterzuladen.</li>
     <li>Die CD kann einfacher verwendet werden, um ein beschädigtes
         Debian-System zu retten.</li>
   </ul>

   <h2><a href="pre-installed">Kaufen Sie einen Computer, auf
        dem Debian vorinstalliert ist</a></h2>
   <p>Dies hat eine Reihe von Vorteilen:</p>
   <ul>
    <li>Sie müssen Debian nicht installieren.</li>
    <li>Die Installation ist vorkonfiguriert, um auf Ihrer Hardware
        zu laufen.</li>
    <li>Der Händler kann Ihnen technische Unterstützung bieten.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Probieren Sie Debian live aus, bevor Sie es installieren</a></h2>
    <p>Sie können Debian jetzt ausprobieren, indem Sie ein Live-System von
       einer CD, DVD oder einem USB-Stick booten, ohne dass Sie dazu irgendeine
       Datei auf den Computer installieren müssen. Wenn Sie dann dazu bereit
       sind, können Sie den enthaltenen Installer starten (seit Debian 10 Buster ist
       dies der benutzerfreundliche <a href="https://calamares.io">Calamares-Installer</a>).
       Vorausgesetzt, die Images
       erfüllen Ihre Anforderungen an Größe, Sprache und Paketauswahl, könnte diese
       Methode für Sie passend sein.
       Lesen Sie <a href="../CD/live#choose_live">weitere Informationen über diese
       Vorgehensweise</a>, um festzustellen, ob sie für Sie passend ist.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Live-Torrents für 64-Bit Intel- und AMD-PC herunterladen"
	     href="<live-images-url/>/amd64/bt-hybrid/">64-Bit-PC Live-Torrents</a></li>
      <li><a title="Live-Torrents für 32-Bit Intel- und AMD-PC herunterladen"
	     href="<live-images-url/>/i386/bt-hybrid/">32-Bit-PC Live-Torrents</a></li>
    </ul>
  </div>

</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Sollten Sie Hardware verwenden, deren Treiber <strong>das Laden nicht-freier
Firmware erfordert</strong>, können Sie einen der
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
Tarballs mit häufig verwendeten Firmware-Archiven</a> verwenden oder ein
<strong>inoffizielles</strong> Installations-Image herunterladen, das diese
<strong>nicht-freien</strong> Firmware-Dateien enthält. Eine
Anleitung zur Verwendung der Tarballs sowie allgemeine Informationen über
das Laden von Firmware während der Installation finden Sie auch in der
<a href="../releases/stable/amd64/ch06s04">Installationsanleitung</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">inoffizielle
Installations-Images für <q>Stable</q> (inklusive Firmware)</a>
</p>
</div>
