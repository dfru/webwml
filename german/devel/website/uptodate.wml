#use wml::debian::template title="Website-Übersetzungen aktuell halten"
#use wml::debian::translation-check translation="8f2dd37edbf6287df4029e3f234798efbcce2862"
# $Id$
# Translator: Gerfried Fuchs <alfie@debian.org> 2002-04-05
# Updated: Holger Wansing <hwansing@mailbox.org>, 2021.

<p>Da Webseiten nicht statisch sind, ist es eine gute Idee, zu verfolgen,
zu welcher Version des Originals eine Übersetzung gehört, und diese
Information zu verwenden, um zu überprüfen, welche Seiten sich seit der
letzten Übersetzung
geändert haben. Diese Information sollte sich im Kopf des Dokuments befinden
(jedoch nach allen anderen <q>use</q>-Zeilen), in dieser Form:</p>

<pre>
\#use wml::debian::translation-check translation="git_commit_hash"
</pre>

<p>wobei <var>git_commit_hash</var> der Commit-Hash-Wert der (englischen)
Originaldatei ist, von der ausgehend die Übersetzung erstellt wurde.
Details zu dem entsprechenden Commit bekommen Sie mit Hilfe des Werkzeugs
<code>git show</code>: <code>git show &lt;git_commit_hash&gt;</code>.
Wenn Sie das <kbd>copypage.pl</kbd>-Skript im webml-Verzeichnis verwenden,
wird die <code>translation-check</code>-Zeile automatisch zur neuen
Version Ihrer übersetzten Datei hinzugefügt, wobei der Hash-Wert auf
die Version der Originaldatei verweist, die zu der Zeit existiert.</p>

<p>Einige Übersetzungen werden möglicherweise für längere Zeit nicht aktualisiert,
auch wenn sich die (englische) Originaldatei ändert.
Wegen der Inhaltsaushandlung könnte der Leser der übersetzen Sprache dies nicht
bemerken und wichtige Informationen versäumen, die in neuen Versionen des
Originals hinzugekommen sind. Die <code>translation-check</code>-Vorlage
beinhaltet Code, der prüft, ob Ihre Übersetzung veraltet ist und eine
entsprechende Meldung ausgibt, die den Benutzer davor warnt.</p>

<p>Es gibt noch weitere Parameter, die Sie in der
<code>translation-check</code>-Zeile verwenden können:</p>

<dl>
 <dt><code>original="<var>Sprache</var>"</code></dt>
 <dd>wobei <var>Sprache</var> der Name der Sprache ist, von der Sie
 übersetzen, falls dies nicht Englisch ist. Der Name muss einem der
 Unterverzeichnisse im Stammverzeichnis des VCS entsprechen, und dem Namen in der
 <code>languages.wml</code>-Vorlage.</dd>

 <dt><code>mindelta="<var>Nummer</var>"</code></dt>
 <dd>was die maximal mögliche Differenz bei den git-Revisionen angibt, bei der
 die Übersetzung noch nicht als alt gilt. Der Standardwert ist <var>1</var>. Für
 weniger wichtige Seiten kann man dies auf <var>2</var> setzen, was bedeutet,
 dass mindestens zwei Änderungen gemacht werden müssten, bevor die Übersetzung
 als alt angesehen wird.</dd>

 <dt><code>maxdelta="<var>Nummer</var>"</code></dt>
 <dd>was die maximale Differenz bei den git-Revisionen angibt, ab der die
 Übersetzung dann als veraltet gilt. Der Standard-Wert ist <var>5</var>. Für sehr
 wichtige Seiten wird dies auf einen geringeren Wert gesetzt.
 Ein Wert von <var>1</var> bedeutet, dass durch jede Änderung am Original, die
 in der Übersetzung nicht nachgezogen ist, die Übersetzung den Status
 <code>veraltet</code> bekommt.</dd>
</dl>

<p>Die Aktualität der Übersetzungen zu verfolgen erlaubt es uns auch,
   <a href="stats/">Übersetzungsstatistiken</a> zu erstellen, sowie Berichte über
   veraltete Übersetzungen (zusammen mit hilfreichen Links bezüglich der Unterschiede
   zwischen den Dateien) und Listen von Seiten, die noch gar nicht übersetzt
   wurden. Dies ist dazu gedacht, den Übersetzern zu helfen und weitere Personen
   anzuregen, zu helfen.
</p>

<p>Übersetzungen, die innerhalb von sechs Monaten nach Änderung der
Original-Seite nicht aktualisiert wurden, werden automatisch restlos entfernt,
so dass unseren Benutzern keine allzu veralteten Informationen präsentiert
werden.
Schauen Sie auf die
<a href="https://www.debian.org/devel/website/stats/">Liste der veralteten
Übersetzungen</a>, um herauszufinden, welche Seiten von einer Löschung bedroht
sind.
</p>

<p>Zusätzlich ist das Skript <kbd>check_trans.pl</kbd> im webwml/-Verzeichnis
verfügbar, das Ihnen einen Bericht über die Seiten anzeigt, die
Aktualisierungen benötigen:</p>

<pre>
check_trans.pl <var>Sprache</var>
</pre>

<p>wobei <var>Sprache</var> der Verzeichnisname ist, der ihrer Übersetzung
entspricht, z.B. <q>german</q>.</p>

<p>Seiten, von denen eine Übersetzung fehlt, werden als <q><code>Missing
<var>Dateiname</var></code></q> angezeigt, und Seiten, die zum Original nicht
aktuell sind, werden als <q><code>NeedToUpdate <var>Dateiname</var> to version
<var>XXXXXX</var></code></q> angezeigt.</p>

<p>Wenn Sie die konkreten Änderungen sehen wollen, können Sie die Unterschiede
angezeigt bekommen, indem Sie die Kommandozeilen-Option <kbd>-d</kbd> zum oben
angegebenen Befehl hinzufügen.</p>

<p>Wenn Sie Warnungen über fehlende Übersetzungen ignorieren wollen (zum Beispiel
für alte Nachrichten-Seiten), können Sie eine Datei namens
<code>.transignore</code> in dem Verzeichnis erstellen, für das Sie die
Warnungen unterdrücken wollen; dabei sollte <code>.transignore</code> 
alle Dateien enthalten, die Sie nicht übersetzen werden, für jede Datei eine
Zeile.</p>

<p>Ein ähnliches Skript für die Überwachung der Übersetzungen der
Mailinglisten-Beschreibungen ist ebenfalls vorhanden. Lesen Sie dazu bitte
die Kommentare im <code>check_desc_trans.pl</code>-Skript als
Dokumentation.</p>
