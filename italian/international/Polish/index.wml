#use wml::debian::template title="Debian in polacco"
#use wml::debian::translation-check translation="d75ca5b8c7dcafd5019d828a62493ededba2b961" maintainer="Giuseppe Sacco"

#  Thanks for skotik87 and andrzejh for translation and check.

<p>
  Debian è disponibile anche in polacco. A partire dall'installazione
  del sistema ai messaggi di sistema e alle interfacce utente delle
  applicazioni più usate, fino alla documentazione. Ovviamente ci sono
  ancora molte cose da fare, ma probabilmente già ora si può usare
  Debian senza conoscere l'inglese.
</p>
<p>
  In Polonia c'è inoltre una forte comunità di utenti Debian, evidenziata
  da molteplici siti web, mailing list e forum; attualmente fanno parte
  del progetto 15 sviluppatori ufficiali (dei quali 11 attivi) &ndash;
  <a href="https://www.perrier.eu.org/weblog/">più informazioni nel blog di bubulle</a>.
</p>

<h2>Risorse Debian in polacco</h2>

<ul>
  <li>Manuali online
    <ul>
#      <li>
#	<strong>Debian FAQ</strong>
#	&nbsp;[<a
#	href="../../doc/manuals/debian-faq/index.pl.html">HTML</a>]&nbsp;[<a
#	href="../../doc/manuals/debian-faq/debian-faq.pl.txt">solo testo</a>]&nbsp;[<a
#	href="../../doc/manuals/debian-faq/debian-faq.pl.pdf">PDF</a>]&nbsp;[<a
#	href="../../doc/manuals/debian-faq/debian-faq.pl.ps">PS</a>] &mdash; Debian
#	in domande e risposte
#      </li>
      <li>
	<strong><a
	  href="http://damlab.pl/poradnik-debiana/handbook/index.php">Guida
	  per principianti</a></strong> &mdash; come iniziare l'avventura con Debian
      </li>
      <li>
	<strong>Installazione e configurazione iniziale di Debian GNU/Linux</strong>&nbsp;[<a
	href="https://dug.net.pl/~azhag/debiandoc/debian-instalacja-konfiguracja.html">HTML</a>]&nbsp;[<a
	href="https://dug.net.pl/~azhag/debiandoc/debian-instalacja-konfiguracja.txt">solo testo</a>]&nbsp;[<a
	href="https://dug.net.pl/~azhag/debiandoc/debian-instalacja-konfiguracja.pdf">PDF</a>]&nbsp;[<a
	href="https://dug.net.pl/~azhag/debiandoc/debian-instalacja-konfiguracja.ps">PS</a>]
      </li>
# Superseeded by Debian Reference
#      <li>
#	<strong>Debian Quick Reference</strong>&nbsp;[<a
#	href="../../doc/manuals/quick-reference/index.pl.html">HTML</a>]&nbsp;[<a
#	href="../../doc/manuals/quick-reference/quick-reference.pl.txt">plain text</a>]&nbsp;[<a
#	href="../../doc/manuals/quick-reference/quick-reference.pl.pdf">PDF</a>]&nbsp;[<a
#	href="../../doc/manuals/quick-reference/quick-reference.pl.ps">PS</a>] &mdash;
#	the concise user handbook
#      </li>
      <li>
	<strong>Debian Reference</strong>&nbsp;[<a
	href="../../doc/manuals/reference/index.pl.html">HTML</a>]&nbsp;[<a
	href="../../doc/manuals/reference/debian-reference.pl.txt">solo testo</a>]
	&mdash; il manuale più completo
      </li>
      <li><strong>APT HOWTO (vecchia documentazione)</strong>&nbsp;[<a
        href="../../doc/manuals/apt-howto/index.pl.html">HTML</a>]&nbsp;[<a
        href="../../doc/manuals/apt-howto/apt-howto.pl.txt">solo testo</a>]&nbsp;[<a
        href="../../doc/manuals/apt-howto/apt-howto.pl.pdf">PDF</a>]&nbsp;[<a
        href="../../doc/manuals/apt-howto/apt-howto.pl.ps">PS</a>] &mdash;
        il breve manuale su APT, l'utilità di gestione dei pacchetti Debian
      </li>
      <li><strong>Guida per il nuovo Maintainer Debian</strong>&nbsp;[<a
        href="../../doc/manuals/maint-guide/index.pl.html">HTML</a>]&nbsp;[<a
        href="../../doc/manuals/maint-guide/maint-guide.pl.txt">solo testo</a>]&nbsp;[<a
        href="../../doc/manuals/maint-guide/maint-guide.pl.pdf">PDF</a>]&nbsp;[<a
        href="../../doc/manuals/maint-guide/maint-guide.pl.ps">PS</a>] &mdash; come
	creare pacchetti nel formato <q>deb</q>
      </li>
    </ul>
  </li>
  <li>Libri
    <ul>
      <li><a
       href="http://helion.pl/ksiazki/debian_linux_system_operacyjny_dla_kazdego_pierwsze_starcie_sylwester_zdanowski,delips.htm">
       Debian Linux. Il sistema operativo universale. Primi passi</a>
      <li> <a href="http://helion.pl/ksiazki/debian_gnu_linux_3_1_biblia_benjamin_mako_hill_david_b_harris_jaldhar_vyas,de3bib.htm">Debian
      GNU/Linux 3.1. Bibbia</a></li>
      <li> <a href="http://helion.pl/ksiazki/debian_gnu_linux_bill_mccarty,dlindk.htm">Conoscere Debian
      GNU/Linux</a></li>
      <li><a href="http://helion.pl/ksiazki/debian_linux_ksiega_eksperta_mario_camou_john_goerzen_aaron_van_couwenberghe,dlinke.htm">Debian
      GNU/Linux 2.1 svelato</a></li>
      <li><a href="http://helion.pl/ksiazki/debian_linux_cwiczenia_lukasz_kolodziej,cwdlin.htm">Debian
      GNU/Linux. Esercizi</a></li>
    </ul>
  <li>Siti web
    <ul>
      <li><a href="https://dug.net.pl">Gruppo utenti Debian</a> &mdash; vasta
        raccolta di ottimi articoli, FAQ, collegamenti</li>
      <li><a href="http://debian.pl/">Portale polacco Debian</a> &mdash; 
	forum attivo, notizie, articoli, FAQ e blog</li>
      <li><a href="http://www.planetadebiana.pl/">Planet Debian in polacco</a> &mdash; 
	blog e altri siti in polacco su Debian</li>
      <li><a href="https://wiki.debian.org/FrontPagePolish">Pagine del
	wiki ufficiale</a> &mdash; si può partecipare all'ampliamento o
	leggere ciò che altri hanno scritto</li>
      <li><a href="https://pl.wikipedia.org/wiki/Debian">Wikipedia</a>
	&mdash; concise informazioni enciclopediche</li>
      <li><a href="http://www.linux.pl/">linux.pl</a>
	&mdash; sito Linux polacco, descrive le varie distribuzioni</li>
    </ul>
  </li>
</ul>

<h2>Dove cercare aiuto in polacco</h2>

<ul>
  <li>Forum
    <ul>
      <li><a href="http://debian.pl/">Il forum polacco degli utenti Debian</a>
	al momento è probabilmente il luogo di maggiore attivitò degli
        utenti Debian. Tra i forum si possono trovare molti consigli,
        documenti e discussioni riguardo le attuali edizioni di Debian</li>
      <li><a href="http://forum.dug.net.pl/">Gruppo utenti Debian</a>
	&mdash; è anch'esso un forum molto attivo per utenti più avanzati</li>
    </ul>
  </li>
  <li>Gruppi di discussione Usenet
    <ul>
      <li><a href="news://pl.comp.os.linux.debian">pl.comp.os.linux.debian</a></li>
      <li><a href="news://alt.pl.comp.os.linux.debian">alt.pl.comp.os.linux.debian</a>
	&mdash; meno traffico, che può essere considerato un vantaggio o
	uno svantaggio.</li>
    </ul>
  </li>
  <li>
    <a href="https://lists.debian.org/debian-user-polish/">Mailing list
    Debian per utenti polacchi</a> &mdash; ci si può iscrivere o inviare
    messaggi all'indirizzo <email debian-user-polish@lists.debian.org />.
    (È inoltre consultabile tramite un gateway mail2news, ad esempio
    <code>news.amu.edu.pl</code>, scrivendo attraverso esso come se si fosse
    iscritti alla lista <email linux-gate@lists.bofh.it />)
  </li>
  <li>Canali IRC
    <ul>
      <li><a href="irc://irc.pl/debian.pl">#debian.pl su IRCNet</a></li>
      <li><a href="irc://irc.pl/error">#error su IRCNet</a> &mdash;
      canale del gruppo utenti Debian (DUG)</li>
    </ul>
  </li>
</ul>

<p>	
  Ci sono molti altri siti polacchi riguardanti Debian, ma non vengono aggiornati da tempo.
</p>

<h2><a name="polonizacja">Tradurre Debian in polacco</a></h2>

<p>
  Le informazioni sui pacchetti e le pagine web tradotte sono disponibili
  <a href="https://www.debian.org/international/l10n">sulle pagine delle
  statistiche</a>.</p>  
<p>Al fine di mantenere il lessico coerente, si prega di utilizzare il<a
  href="https://wiki.debian.org/WordlistPolish">glossario</a>
  durante la traduzione delle risorse relative a Debian.</p>
<p>
  Se volete aiutare nella traduzione (messaggi d'installazione,
  descrizioni dei pacchetti, interfacce dei programmi, pagine del sito,
  documentazione, ecc.) o avete altre idee su qualcosa da inserire in questa o altre
  pagine Debian, inviare un'email in lista <email debian-l10n-polish@lists.debian.org>.
</p>
<p>
  In passato gran parte del lavoro fu inserito nella
  <a href="http://debian.linux.org.pl/">documentazione del progetto
  Debian in polacco (PDDP)</a>, ma ora non sembra più attivo.<br />
</p>

<h2>Progetti di traduzione</h2>
<p>
<small>La seguente descrizione fu preparata alla fine del 2013</small>
</p>

<strong>Installatore (debian-installer, d-i)</strong>
<ul>
 <li>Ambito: Debian e derivate (parzialmente)</li>
 <li>Formato della traduzione: gettext (file PO)</li>
 <li>Fare modifiche: direttamente su SVN/debian-l10n-polish@</li>
 <li>Statistiche: <a href="https://d-i.debian.org/l10n-stats/">https://d-i.debian.org/l10n-stats/</a></li>
 <li>Repository: <a href="https://salsa.debian.org/installer-team/d-i/tree/master/packages/po">https://salsa.debian.org/installer-team/d-i/tree/master/packages/po</a></li>
 <li>Documentazione: <a href="https://d-i.alioth.debian.org/doc/i18n/">https://d-i.alioth.debian.org/doc/i18n/</a></li>
</ul>

<p>
 L'installatore Debian è ormai completamente tradotto. Va controllato lo
 stile, ma è sostanzialmente in buono stato.
</p>
<p>
 Queste traduzioni vanno tenute d'occhio, con l'avvicinamento
 del rilascio della nuova Debian, perché i manutentori dei pacchetti
 tendono a cambiare i messaggi inglesi all'ultimo minuto.
</p>

<strong>Pagine WWW</strong>
<ul>
 <li>Ambito: Debian</li>
 <li>Formato della traduzione: WML</li>
 <li>FAre modifiche: direttamente su SVN/debian-l10n-polish@</li>
 <li>Statistiche: <a href="https://www.debian.org/devel/website/stats/">https://www.debian.org/devel/website/stats/</a></li>
 <li>Repository: <a href="https://salsa.debian.org/webmaster-team/webwml/tree/master/polish">https://salsa.debian.org/webmaster-team/webwml/tree/master/polish</a></li>
 <li>Documentazione: <a href="https://www.debian.org/devel/website/translating">https://www.debian.org/devel/website/translating</a></li>
</ul>
<p>
 Le principali pagine web Debian sono tradotte, alcune delle quali
 sono da aggiornare. La situazione ha iniziato a migliorare di recente.
 Non è male, anche se andrebbero tradotte ancora alcune pagine importanti.
</p>

<strong>Modelli Debconf</strong>
<ul>
 <li>Ambito: Debian e derivate (parzialmente)</li>
 <li>Formato della traduzione: gettext (file PO)</li>
 <li>Fare modifiche: bug report</li>
 <li>Statistiche: <a href="https://www.debian.org/international/l10n/po-debconf/pl">https://www.debian.org/international/l10n/po-debconf/pl</a></li>
</ul>
<p>
 Attualmente circa il 60% dei modelli debconf è stato tradotto. Nuovi
 modelli vengono tradotti man mano che appaiono, ma ci sono ancora
 molti vecchi modelli che sarebbe opportuno tradurre.
</p>
<p>
 I modelli debconf sono quelle finetre che appaio a volte
 mentre di installa o aggiorna un pacchetto, o se si esegue
 dpkg-reconfigure.Le descrizioni delle traduzioni sono generalmente
 molto brevi, anche se alcune sono difficili da tradurre senza conoscere
 le specificità del pacchetto. Le notiche su nuov versioni del modello
 vengono inviate sulla lista debian-i18n, mentre il modello tradotto
 va inviato come bug report con priorità «wishlist» sul pacchetto stesso.
</p>

<strong>Pagine di manuali</strong>
 <ul>
  <li>Debian:
   <ul>
    <li>Ambito: Debian</li>
    <li>Formato della traduzione: gettext (file PO)</li>
    <li>Fare modifiche: bug report</li>
    <li>Statistiche: <a href="https://www.debian.org/international/l10n/po4a/pl">https://www.debian.org/international/l10n/po4a/pl</a></li>
   </ul>
    Parecchie pagine di manuale sono tradotte (principalmente legate ai pacchetti
    dpkg, apt, aptitude, eccetera). Non ci sono gravi mancanze.
  </li>
  <li>altro (pacchetto manpages-pl):
   <ul>
    <li>Ambito: GNU/Linux</li>
    <li>Formato delle traduzioni: gettext (file PO)</li>
    <li>Fare modifiche: direttamente su git/manpages-pl-list@lists.sourceforge.net</li>
    <li>Repository:
     <ul>
      <li>tramite web: <a href="http://sourceforge.net/p/manpages-pl/code/ci/master/tree/">http://sourceforge.net/p/manpages-pl/code/ci/master/tree/</a></li>
      <li>tramite git: git://git.code.sf.net/p/manpages-pl/code</li>
     </ul>
    </li>
   </ul>
  </li>
</ul>

<p>
 Pagine di manuale di pacchetto quali coreutils (rm, cat, etc.), util-linux
 (dmesg, mkfs, etc.), manpages (proc, hosts, etc.) e altri
 (sed, bash, etc.) sono tradotte dal progetto manpages-pl project, che
 accetta chiunque voglia ridurre, anche solo di uno, l'elenco delle
 quasi 700 pagine di manuale vetuste che attendono volontari in cerca
 di gloria :-) Attualmente la traduzione di manpages-pl viene anche
 usata almeno da Fedora e Arch Linux (e, naturalmente da Ubuntu e altre
 derivate di Debian).
</p>

<h2>Programmi</h2>
<ul>
 <li>Debian:
  <ul>
   <li>Ambito: Debian e derivate (parzialmente)</li>
   <li>Formato traduzione: gettext (file PO)</li>
   <li>Fare modifiche: bug report</li>
   <li>Statistiche: <a href="https://www.debian.org/international/l10n/po/pl">https://www.debian.org/international/l10n/po/pl</a></li>
  </ul>
   Al momento molti programmi Debian sono tradotti (principalmente i
   pacchetti dpkg, apt, aptitude etc.). Non ci sono grosse mancanze.
 </li>
 <li>altro
  <ul>
   <li>Ambito: *nix</li>
   <li>Formato traduzione: normalmente gettext (file PO)</li>
   <li>Fare modifiche: vario (progetti sorgente)</li>
   <li>Statistiche: <a href="https://www.debian.org/international/l10n/po/pl">https://www.debian.org/international/l10n/po/pl</a></li>
  </ul>
   I programmi dei progetti esterni a Debian (inclusi KDE, Gnome, ecc.)
   sono tradotti nel progetto sorgente, dove la situazione è varia.
   Anche se questi pacchetti appaiono nelle statistiche delle traduzioni
   da fare, queste non vengono fatte in Debian!
 </li>
</ul>

<h2>Documentazione varia, guide, ITP HOW-TO, etc. (DDP)</h2>
<ul>
 <li>Ambito: Debian</li>
 <li>Formato traduzione: gettext (file PO) / docbook</li>
 <li>Fare modifiche: direttamente su SVN/bug report</li>
 <li>Repository: <a href="https://anonscm.debian.org/viewvc/ddp/manuals/trunk/">https://anonscm.debian.org/viewvc/ddp/manuals/trunk/</a></li>
 <li>Documentazione: <a href="https://www.debian.org/doc/ddp">https://www.debian.org/doc/ddp</a></li>
</ul>
<p>
 Al momento la traduzione delle note di rilascio e altre piccole
 cose è aggiornata. Ciononostante molte sono in uno stato disdicevole.
 Questi documenti da tradurre sono molto consistenti, quindi lo loro
 traduzione (ad esempio) della guida di installazione, richiede il
 coinvolgimenti di almeno 3-5 persone.
</p>
<p>
 Va notato che anche la versione originale dei manuali tende ad essere un
 po' lasciata per ultima.
</p>

<h2>Descrizioni dei pacchetti (DDTP)</h2>
<ul>
 <li>Ambito: Debian e Ubuntu</li>
 <li>Formato traduzione: HTML</li>
 <li>Fare modifiche: <a href="https://ddtp.debian.org/ddtss/index.cgi/pl">https://ddtp.debian.org/ddtss/index.cgi/pl</a></li>
 <li>Statistiche: <a href="https://ddtp.debian.org/stats">https://ddtp.debian.org/stats</a></li>
 <li>Documentazione:
  <ul>
   <li><a href="http://ddtp.dug.net.pl/forum/">http://ddtp.dug.net.pl/forum/</a></li>
   <li><a href="https://www.debian.org/international/l10n/ddtp">https://www.debian.org/international/l10n/ddtp</a></li>
  </ul>
 </li>
</ul>
<p>
 Le descrizioni dei pacchetti più importanti sono oramai tradotte, ma altre
 stanno arrivando, quindi vanno controllate periodicamente. Anche attività
 sporadiche sono utili, perché ogni descrizione va approvata da altre due persone.
</p>
<p>
 La traduzione delle descrizioni dei pacchetti è il modo più semplice
 di tradurre in Debian (almeno, se non si ha esperienza con la traduzione
 dei file PO). Sfortunatamente, la saga infinita del server dislocato in
 Spagna, nell'Estremadura, causa spesso problemi dell'intera infrastruttura
 che a volte durano per settimane.
</p>
<p>
 Le descrizioni dei pacchetti task-* richiedono particolare attenzione perché
 sono usati anche dall'installatore Debian.
</p>

<h2>Notizie</h2>
<ul>
 <li>Ambito: Debian</li>
 <li>Formato traduzione: WML/Markdown</li>
 <li>Fare modifiche: direttamente su SVN/debian-publicity@</li>
 <li>Repository: <a href="https://salsa.debian.org/publicity-team/">https://salsa.debian.org/publicity-team/</a></li>
</ul>
<p>
 La sezione «News» non è attualmente tradotta in polacco, e forse a ragione
 perché va oltre le nostre attuali capacità. Di quando in quando
 appare una traduzione generalmente relativa ai rilasci delle nuove
 Debian. Attualmente questa sezione è quella meno importante.
</p>

<h2>Spiegazioni</h2>
<p>
 Spiegazioni: con &quot;fare modifiche&quot; intendiamo un posto
 nel quale si possono inviare i cambiamenti, o una lista alla quale
 inviare il file tradotto. Tutte le liste Debian sono accessibili su
 lists.debian.org, quindi se scriviamo &quot;debian-l10n-polish@&quot;
 intendiamo debian-l10n-polish@lists.debian.org. &quot;Bug report&quot;
 indica che va mandata un email contestualizzata all'indirizzo
 submit@bugs.debian.org (per magiori informazioni vedere
 <a href="https://www.debian.org/Bugs/">https://www.debian.org/Bugs/</a>).
</p>
<p>
 <strong>Nota:</strong> alcuni collegamenti vanno alla pagina
 &quot;Principali dati statistici sulle traduzioni in Debian&quot; at
 <a href="https://www.debian.org/international/l10n/">https://www.debian.org/international/l10n/</a>
</p>

