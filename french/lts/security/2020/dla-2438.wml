#use wml::debian::translation-check translation="43b404ec07f0db216f7402af3c2791e9e46830bb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait deux vulnérabilités de dépassement de tas
dans raptor2, un ensemble d’analyseurs pour des fichiers RDF qui sont, parmi
d’autres, utilisés dans LibreOffice.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18926">CVE-2017-18926</a>

<p>raptor_xml_writer_start_element_common dans raptor_xml_writer.c dans la
bibliothèque de syntaxe de Raptor RDF, version 2.0.15 calculait de façon
erronée les déclarations maximales de <q>nspace</q> pour le scripteur XML,
conduisant à un dépassement de tampon de tas (parfois aperçu dans
raptor_qname_format_as_xml).</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.0.14-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets raptor2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2438.data"
# $Id: $
