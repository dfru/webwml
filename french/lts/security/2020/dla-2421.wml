#use wml::debian::translation-check translation="3fa8ba9db3f0d11ac49c23662494d8490f46a695" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans cimg, une bibliothèque puissante
de traitement d’image.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010174">CVE-2019-1010174</a>

<p>Ce CVE est relatif à un assainissement manquant de chaîne d’URL,
qui pourrait aboutir à une injection de commande lors du chargement d’image
spécialement contrefaite.</p>

<p>Les autres CVE concernent des lectures hors limites de tampon de tas ou des
doubles libérations de zone de mémoire lors du chargement d’image contrefaite.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.7.9+dfsg-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cimg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cimg, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cimg">https://security-tracker.debian.org/tracker/cimg</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2421.data"
# $Id: $
