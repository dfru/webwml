#use wml::debian::translation-check translation="039b6907a524d7a57988750fb538b3aaf3352133" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une possibilité d’attaque par déni de
service dans libproxy, une bibliothèque pour avertir les applications d’un
mandataire HTTP. Un serveur distant pourrait provoquer une récursion infinie
de pile.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25219">CVE-2020-25219</a>

<p>url::recvline dans url.cpp dans libproxy, versions 0.4.x jusqu’à 0.4.15,
permet à un serveur HTTP distant de déclencher une récursion incontrôlée à 
l'aide d'une réponse composée d’un flux infini sans caractère de nouvelle ligne.
Cela amène à un épuisement de pile.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.4.14-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets  mandataire li.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2372.data"
# $Id: $
