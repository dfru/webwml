#use wml::debian::translation-check translation="6720d074c1b7a1b706ad2a534d3f529f6af91f8c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été signalé que l'implémentation des profils HID et HOGP de BlueZ
n'exige pas particulièrement de lien entre le périphérique et l'hôte. Des
périphériques malveillants peuvent tirer avantage de ce défaut pour se connecter
à un hôte cible et se faire passer pour un périphérique HID existant sans
sécurité ou produire une découverte de services SDP ou GATT, ce qui
permettrait l'injection de rapports HID dans le sous-système d'entrée à partir
d'une source non liée.</p>

<p>Une nouvelle option de configuration (ClassicBondedOnly) pour le profil HID
a été introduite pour s'assurer que les connexions d'entrées ne viennent que
de connexions avec des périphériques liés. Les options sont par défaut
à « false » pour maximiser la compatibilité avec les périphériques.</p>

<p>Remarquez qu’en raison des modifications importantes entre la version
précédente dans Jessie (basée sur la publication 5.23 de l’amont) et les
correctifs disponibles pour corriger cette vulnérabilité, il a été décidé qu’un
rétroportage du paquet bluez de Debian 9 <q>Stretch</q> était la seule façon
possible de corriger la vulnérabilité référencée.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 5.43-2+deb9u2~deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bluez.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2240.data"
# $Id: $
