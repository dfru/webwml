#use wml::debian::translation-check translation="0c0cb6446638d777667ad4103d8d0e781ce6a28f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que node-xmldom, une implémentation standard de DOM
XML (Level2 CORE) en pur javascript, traitait du XML mal formé, pouvant aboutir
à des bogues ou des trous de sécurité dans les applications en aval.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21366">CVE-2021-21366</a>

<p>xmldom, versions 0.4.0 et suivantes, ne préservait pas correctement les
identificateurs du système, les FPI ou les espaces de nommage lors de l’analyse
et la sérialisation répétitive de documents malveillants. Cela pouvait conduire
à des modifications inattendues lors du traitement d’XML dans certaines
applications en aval.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39353">CVE-2022-39353</a>

<p>Mark Gollnick a découvert que xmldom analysait du XML non correct car
contenant plusieurs éléments de premier niveau, et ajoutait tous les nœuds
racine à la collection de <code>childNodes</code> du <code>Document</code>, sans
rapporter ou rejeter n’importe quel erreur. Cela brisait la supposition qu’il
y avait un seul nœud racine dans l’arbre et pouvait creuser des trous de
sécurité tels que le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-39299">CVE-2022-39299</a>
dans les applications en aval.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.1.27+ds-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-xmldom.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-xmldom,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-xmldom">\
https://security-tracker.debian.org/tracker/node-xmldom</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3260.data"
# $Id: $
