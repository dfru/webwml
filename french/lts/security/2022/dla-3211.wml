#use wml::debian::translation-check translation="e625d75e10724cacde14ae8ba392b2b20e4d5fc2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une lecture hors limites potentielle dans
le démon BGP de frr, un ensemble d’outils pour le routage du trafic Internet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37032">CVE-2022-37032</a>

<p>Une lecture hors limites dans le démon BGP de FRRouting FRR avant la
version 8.4 pourrait conduire à une erreur de segmentation et un déni de
service. Cela se produit dans bgp_capability_msg_parse dans
bgpd/bgp_packet.c.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 6.0.2-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets frr.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3211.data"
# $Id: $
