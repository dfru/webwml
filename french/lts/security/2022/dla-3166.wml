#use wml::debian::translation-check translation="77a44307ac82fb9029ad7c0fdd1b1776eb17b225" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de traversée de fichier a été découverte dans
src:ruby-sinatra, un serveur web répandu souvent utilisé avec Ruby on
Rails. Le serveur vérifie désormais que les chemins développés
correspondent au <q>public_dir</q> autorisé lors du service de fichiers
statiques.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
2.0.5-4+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-sinatra.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-sinatra,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-sinatra">\
https://security-tracker.debian.org/tracker/ruby-sinatra</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3166.data"
# $Id: $
