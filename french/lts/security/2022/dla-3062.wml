#use wml::debian::translation-check translation="f9e9402066eecfcb07a81111b40acb712d004d3a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>uBlock, un greffon de Firefox de blocage de publicités, de logiciels
malveillants et de pisteurs, prenait en charge une profondeur arbitraire
d'imbrication de paramètres pour un blocage strict. Cela permet à des sites
web contrefaits de provoquer un déni de service (récursion sans limite qui
peut déclencher une consommation excessive de mémoire et une perte de toutes
les fonctions de blocage).</p>

<p>Veuillez noter que webext-ublock-origin a été remplacé par
webext-ublock-originfirefox.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.42.0+dfsg-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ublock-origin.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ublock-origin,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ublock-origin">\
https://security-tracker.debian.org/tracker/ublock-origin</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3062.data"
# $Id: $
