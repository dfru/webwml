#use wml::debian::translation-check translation="fa4669b567758b23e3f886c6c40464a214ea8bcd" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans puma, un
serveur web pour les applications Ruby/Rack. Ces défauts pouvaient conduire
à une fuite d'information due à la fermeture non systématique du corps des
réponses, permettant une entrée non fiable dans un en-tête de réponse
(découpage de réponse HTTP) et facilitant ainsi éventuellement plusieurs
autres attaques tel qu'un script intersite. Un client au mauvais
comportement pouvait aussi utiliser des requêtes persistantes pour
monopoliser le réacteur de Puma et créer une attaque par déni de service.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la version
3.6.0-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets puma.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de puma, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/puma">\
https://security-tracker.debian.org/tracker/puma</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3023.data"
# $Id: $
