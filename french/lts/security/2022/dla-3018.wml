#use wml::debian::translation-check translation="d69454ea19d65f05c16894f110d9c22a7c7f67f5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que libpgjava, le pilote JDBC officiel de PostgreSQL,
pouvait être vulnérable si un attaquant contrôlait l'URL ou les propriétés
de JDBC. Le pilote JDBC ne vérifiait pas si certaines classes
implémentaient l'interface attendue avant d'instancier la classe. Cela peut
conduire à l'exécution de code à l'aide de classes arbitraires.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
9.4.1212-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libpgjava.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libpgjava, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libpgjava">\
https://security-tracker.debian.org/tracker/libpgjava</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3018.data"
# $Id: $
