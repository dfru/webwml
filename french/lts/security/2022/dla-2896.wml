#use wml::debian::translation-check translation="79e10d0d2b41423d13d3273b651d25c62ad4531e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une potentielle vulnérabilité d'exécution de code arbitraire
dans IPython, l'interpréteur de commande interactif de Python.</p>

<p>Ce problème résulte de l'exécution par IPython de fichiers non fiables
dans le répertoire de travail en cours. Selon les développeurs amont :</p>

<blockquote>
<p>
Presque toutes les versions d'IPython recherchent les fichiers de
configuration et les profils dans le répertoire de travail en cours. Dans
la mesure où IPython a été développé avant que pip et les environnements
existent, cela était utilisé de façon pratique pour charger du code ou des
paquets d'une manière qui dépendait du projet.
</p>
<p>
En 2022, cela n'est plus nécessaire et peut conduire à un comportement
confus où, par exemple, le clonage d'un dépôt et le démarrage d'IPython ou
le chargement d'un « notebook » à partir de n'importe quelle interface
compatible avec Jupyter ayant défini ipython comme noyau peut conduire à
l'exécution de code.
</p>
</blockquote>

<p>Pour traiter ce problème, les profils ou les fichiers de configuration
ne sont plus recherchés dans le répertoire de travail en cours.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21699">CVE-2022-21699</a>

<p>IPython (Interactive Python) est un interpréteur de commande pour une
utilisation interactive dans plusieurs langages de programmation, développé
à l'origine pour le langage Python. Les versions affectées sont sujettes à
une vulnérabilité d'exécution de code arbitraire réalisée par une gestion
incorrecte de fichiers temporaires entre les utilisateurs. Cette
vulnérabilité permet à un utilisateur d'exécuter du code en tant qu'un
autre sur la même machine. Il est conseillé à tous les utilisateurs de
mettre à niveau leurs paquets.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 5.1.0-3+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ipython.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2896.data"
# $Id: $
