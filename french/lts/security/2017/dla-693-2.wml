#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La version 4.0.2-6+deb7u7 introduisait des modifications qui avaient
pour conséquence que libtiff n'était pas capable d'écrire des fichiers tiff
lorsque le schéma de compression utilisé reposait sur des étiquettes de
TIFF, spécifiques au codec, incorporées à l'image.</p>

<p>Ce problème se manifestait par des erreurs comme celles-ci :<br>
$ tiffcp -r 16 -c jpeg sample.tif out.tif<br>
_TIFFVGetField: out.tif: Invalid tag <q>Predictor</q> (not supported by codec).<br>
_TIFFVGetField: out.tif: Invalid tag <q>BadFaxLines</q> (not supported by codec).<br>
tiffcp: tif_dirwrite.c:687: TIFFWriteDirectorySec: Assertion `0' failed.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.0.2-6+deb7u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-693-2.data"
# $Id: $
