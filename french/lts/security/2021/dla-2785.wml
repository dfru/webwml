#use wml::debian::translation-check translation="6b71064de108ba1d76bcc868ac6ecc4a6647b665" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation de privilèges, un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3702">CVE-2020-3702</a>

<p>Un défaut a été découvert dans le pilote pour la famille de puces
Atheros IEEE 802.11n (ath9k) permettant une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16119">CVE-2020-16119</a>

<p>Hadar Manor a signalé une utilisation de mémoire après libération dans
l'implémentation du protocole DCCP dans le noyau Linux. Un attaquant local
peut tirer avantage de ce défaut pour provoquer un déni de service ou
éventuellement pour exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3444">CVE-2021-3444</a>,

<p><a href="https://security-tracker.debian.org/tracker/CVE-2021-3600">CVE-2021-3600</a></p>

<p>Deux défauts ont été découverts dans le vérificateur d'Extended BPF
(eBPF). Un utilisateur local pourrait les exploiter pour lire et écrire des
portions arbitraires de mémoire dans le noyau, ce qui pourrait être utilisé
pour une élévation de privilèges.</p>

<p>Cela peut être atténué avec le réglage de sysctl
kernel.unprivileged_bpf_disabled=1 qui désactive l'utilisation d'eBPF par
les utilisateurs non privilégiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3612">CVE-2021-3612</a>

<p>Murray McAllister a signalé un défaut dans le sous-système d'entrée de
manette de jeu. Un utilisateur local autorisé à accéder à un périphérique
de manette de jeu pourrait exploiter cela pour une lecture et écriture hors
limites dans le noyau, ce qui pourrait être utilisé pour une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3653">CVE-2021-3653</a>

 <p>Maxim Levitsky a découvert une vulnérabilité dans l'implémentation de
l'hyperviseur KVM pour les processeurs AMD dans le noyau Linux : l'absence
de validation du champ « int_ctl » de VMCB pourrait permettre à un client
L1 malveillant d'activer la prise en charge d'AVIC (Advanced Virtual
Interrupt Controller) pour le client L2. Le client L2 peut tirer avantage
de ce défaut pour écrire dans un sous-ensemble limité mais néanmoins
relativement grand de la mémoire physique de l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3655">CVE-2021-3655</a>

<p>Ilja Van Sprundel et Marcelo Ricardo Leitner ont découvert plusieurs
défauts dans l'implémentation de SCTP où l'absence de validation pourrait
conduire à une lecture hors limites. Sur un système utilisant SCTP, un
attaquant réseau pourrait les exploiter pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3656">CVE-2021-3656</a>

<p>Maxim Levitsky et Paolo Bonzini ont découvert un défaut dans
l'implémentation de l'hyperviseur KVM pour les processeurs AMD dans le
noyau Linux. L'absence de validation du champ « virt_ext » de VMCB pourrait
permettre à un client L1 malveillant de désactiver à la fois les
interceptions des instructions VMLOAD/VMSAVE et VLS (VMLOAD/VMSAVE
virtuels) pour le client L2. Dans ces circonstances, le client L2 peut
exécuter des instructions VMLOAD/VMSAVE non interceptées et donc lire et
écrire des portions de la mémoire physique de l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3679">CVE-2021-3679</a>

<p>Un défaut dans la fonctionnalité du module de traçage du noyau Linux
pourrait permettre à un utilisateur local privilégié (doté de la capacité
CAP_SYS_ADMIN) de provoquer un déni de service (saturation des ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3732">CVE-2021-3732</a>

<p>Alois Wohlschlager a signalé un défaut dans l'implémentation du
sous-système overlayfs, permettant à un attaquant local, doté des
privilèges pour monter un système de fichiers, de révéler des fichiers
cachés dans le montage d'origine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3743">CVE-2021-3743</a>

<p>Une lecture de mémoire hors limites a été découverte dans
l'implémentation du protocole du routeur IPC Qualcomm, permettant de
provoquer un déni de service ou une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3753">CVE-2021-3753</a>

<p>Minh Yuan a signalé une situation de compétition dans vt_k_ioctl dans
rivers/tty/vt/vt_ioctl.c, qui peut provoquer une lecture hors limites dans
le terminal virtuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22543">CVE-2021-22543</a>

<p>David Stevens a découvert un défaut dans la manière dont l'hyperviseur
KVM met en correspondance la mémoire de l'hôte dans un client. Un
utilisateur local autorisé à accéder à /dev/kvm pourrait utiliser cela pour
provoquer la libération de certaines pages alors qu'elles ne devraient pas
l'être, menant à une utilisation de mémoire après libération. Cela pourrait
être utilisé pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33624">CVE-2021-33624</a>,

<p><a href="https://security-tracker.debian.org/tracker/CVE-2021-34556">CVE-2021-34556</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-35477">CVE-2021-35477</a></p>

<p>Plusieurs chercheurs ont découvert des défauts dans les protections du
vérificateur d'Extended BPF (eBPF) contre les fuites d'informations au
moyen de l'exécution spéculative. Un utilisateur local pourrait exploiter
cela pour lire des informations sensibles.</p>

<p>Cela peut être atténué avec le réglage de sysctl
kernel.unprivileged_bpf_disabled=1 qui désactive l'utilisation d'eBPF par
les utilisateurs non privilégiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-35039">CVE-2021-35039</a>

<p>Un défaut a été découvert dans l'application de la signature des
modules. Un noyau personnalisé avec IMA activé pourrait avoir permis de
charger des modules non signés du noyau alors qu'il n'aurait pas dû.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37159">CVE-2021-37159</a>

<p>Un défaut a été découvert dans le pilote hso pour les modems mobiles
haut débit Option. Une erreur pendant l'initialisation pourrait conduire à
une double libération de zone de mémoire ou à une utilisation de mémoire
après libération. Un attaquant capable de se connecter à des périphériques
USB pourrait utiliser cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou éventuellement pour exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38160">CVE-2021-38160</a>

<p>Un défaut a été découvert dans virtio_console permettant la corruption
ou la perte de données par un périphérique non fiable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38198">CVE-2021-38198</a>

<p>Un défaut a été découvert dans l'implémentation de KVM pour les
processeurs x86, ce qui pourrait avoir pour conséquence que la protection
de la mémoire virtuelle dans un client n'est pas correctement appliquée.
Lorsque des tables de page « shadow » sont utilisées, par exemple pour la
virtualisation imbriquée ou sur les processeurs qui ne disposent pas des
fonctions EPT ou NPT, un utilisateur du système exploitation client
pourrait exploiter cela pour un déni de service ou une élévation de
privilèges dans le client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38199">CVE-2021-38199</a>

<p>Michael Wakabayashi a signalé un défaut dans l'implémentation du client
NFSv4, où l'ordre incorrect des réglages de connexion permet aux opérations
d'un serveur NFSv4 distant de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38205">CVE-2021-38205</a>

<p>Une fuite d'informations a été découverte dans le pilote réseau
xilinx_emaclite. Sur un noyau personnalisé où ce pilote est activé et
utilisé, cela pourrait faciliter l'exploitation d'autres bogues du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40490">CVE-2021-40490</a>

<p>Une situation de compétition a été découverte dans le sous-système ext4
lors de l'écriture dans un fichier inline_data alors que ses attributs
étendus sont modifiés. Cela pourrait avoir pour conséquence un déni de
service.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.19.208-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.19.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-4.19, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-4.19">\
https://security-tracker.debian.org/tracker/linux-4.19</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2785.data"
# $Id: $
