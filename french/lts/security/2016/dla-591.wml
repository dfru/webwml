#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un fichier « OpenDocument Presentation » .ODP ou « Presentation
Template » .OTP peut contenir des éléments de présentation non valables qui
mènent à une corruption de mémoire quand le document est chargé dans
LibreOffice Impress. Ce défaut peut faire que le document apparaît comme
corrompu et LibreOffice peut planter dans un mode de récupération bloqué
nécessitant une intervention manuelle. Une exploitation contrefaite de ce
défaut peut permettre à un attaquant distant de provoquer un déni de
service (corruption de mémoire et plantage de l'application) et une
possible exécution de code arbitraire.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans la
version 3.5.4+dfsg2-0+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libreoffice.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-591.data"
# $Id: $
