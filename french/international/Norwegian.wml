#use wml::debian::template title="Le coin norvégien de Debian"
#use wml::debian::translation-check translation="7ff1e58fbad65b9683e41dcd4c9d35fe7c22ecef" maintainer="Jean-Pierre Giraud"

# Translators:
# Christian Couder, 2001-2004.
# Thomas Huriaux, 2005.
# Jean-Edouard Babin, 2008.
# David Prévot, 2010, 2013.
# Jean-Pierre Giraud, 2021.

    <p>
    Cette page présente des informations pour les utilisateurs norvégiens
    de Debian. Si vous connaissez quelque chose qui mériterait d'être
    ajouté ici, veuillez écrire à l'un des <a
    href="#translators">traducteurs</a> norvégiens.</p>

    <h2>Listes de diffusion</h2>

    <p>Debian n'a pas pour l'instant de liste de diffusion officielle
    en norvégien. Si l'intérêt se fait sentir, nous pouvons en créer
    une ou plusieurs, pour les utilisateurs ou les développeurs.</p>

    <h2>Liens</h2>

    <p>Quelques liens pouvant intéresser les utilisateurs norvégiens de
    Debian :</p>

   <ul>
    <!-- 
      L’organization Linux Norge ne semble plus exister. Le domaine linux.no 
      existe, mais est complètement obsolète.
    -->
    <!-- li><a href="http://www.linux.no/">Linux Norway</a><br>
        <em>« Une organisation de bénévoles qui a pour but de diffuser
        des informations sur le système d'exploitation Linux en Norvège »</em>
        qui héberge, entre autres, le 
    </li> -->

    <li><a href="https://nuug.no/">Groupe norvégien d'utilisateurs d’UNIX</a>.
        Consultez la page de leur wiki sur les
        <a href="http://wiki.nuug.no/likesinnede-oversikt">organisations similaires</a>
        qui signale la plupart des groupes actifs d'utilisateurs de Linux en
        Norvège.
    </li>

    </ul>

    <h2>Contributeurs norvégiens au projet Debian</h2>

    <h3>Développeurs Debian norvégiens actifs actuellement :</h3>

    <ul>
      <li>Lars Bahner &lt;<email bahner@debian.org>&gt;</li>
      <li>Morten Werner Forsbring &lt;<email werner@debian.org>&gt;</li>
      <li>Ove Kåven &lt;<email ovek@debian.org>&gt;</li>
      <li>Petter Reinholdtsen &lt;<email pere@debian.org>&gt;</li>
      <li>Ruben Undheim &lt;<email rubund@debian.org>&gt;</li>
      <li>Steinar H. Gunderson &lt;<email sesse@debian.org>&gt;</li>
      <li>Stein Magnus Jodal &lt;<email jodal@debian.org>&gt;</li>
      <li>Stig Sandbeck Mathisen &lt;<email ssm@debian.org>&gt;</li>
      <li>Tollef Fog Heen &lt;<email tfheen@debian.org>&gt;</li>
      <li>Øystein Gisnås &lt;<email shaka@debian.org>&gt;</li>
    </ul>

<p>Cette liste n'est pas fréquemment mise à jour, vous pouvez consulter la
<a href="https://db.debian.org/">base des Développeurs Debian</a> pour avoir une
liste à jour en sélectionnant Norvège comme pays.</p>

    <h3>Anciens développeurs Debian norvégiens :</h3>

    <ul>
      <li>Morten Hustvei</li>
      <li>Ole J. Tetlie</li>
      <li>Peter Krefting</li>
      <li>Tom Cato Amundsen</li>
      <li>Tore Anderson</li>
      <li>Tor Slettnes</li>
     </ul>

    <a id="translators"></a>
    <h3>Traducteurs :</h3>

    <p>Les pages web Debian sont traduites en norvégien par :</p>

    <ul>
      <li>Hans F. Nordhaug &lt;<email hansfn@gmail.com>&gt;</li>
    </ul>

    <p>
      <!-- Debian, en ce moment, n'a personne pour traduire ses pages web en
      norvégien. -->
      Si vous êtes intéressé, merci de commencer par lire les
      <a href="$(DEVEL)/website/translating">informations pour les
      traducteurs</a>.
    </p>

    <p>
      Auparavant, les pages web Debian étaient traduites par&nbsp;:
    </p>

    <ul>
      <li>Cato Auestad</li>
      <li>Stig Sandbeck Mathisen</li>
      <li>Tollef Fog Heen</li>
      <li>Tor Slettnes</li>
    </ul>

    <p>Si vous voulez aider, ou si vous connaissez quelqu'un d'autre
    qui d'une façon ou d'une autre est impliqué dans le projet Debian,
    merci de contacter l'un d'entre nous.</p>

