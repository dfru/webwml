#use wml::debian::translation-check translation="fe5c1a6aa317aa3b586fd77b4bfca281e8bd5883" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42852">CVE-2022-42852</a>

<p>hazbinhotel a découvert que le traitement d'un contenu web contrefait
pouvait avoir pour conséquence la divulgation de la mémoire du processus.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42856">CVE-2022-42856</a>

<p>Clement Lecigne a découvert que le traitement d'un contenu web
contrefait pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42867">CVE-2022-42867</a>

<p>Maddie Stone a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46692">CVE-2022-46692</a>

<p>KirtiKumar Anandrao Ramchandani a découvert que le traitement d'un
contenu web contrefait pouvait contourner la politique de même origine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46698">CVE-2022-46698</a>

<p>Dohyun Lee et Ryan Shin ont découvert que le traitement d'un contenu web
contrefait pouvait divulguer des informations sensibles de l'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46699">CVE-2022-46699</a>

<p>Samuel Gross a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-46700">CVE-2022-46700</a>

<p>Samuel Gross a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.38.3-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5308.data"
# $Id: $
