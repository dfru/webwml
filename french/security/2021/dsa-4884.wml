#use wml::debian::translation-check translation="8841a09ee24923f86538151f0ba51430b8414c1a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans ldb, une base de
données embarquée similaire à LDAP construite sur TDB. </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10730">CVE-2020-10730</a>

<p>Andrew Bartlett a découvert un déréférencement de pointeur NULL et un
défaut d'utilisation de mémoire après libération lors du traitement des
contrôles <q>ASQ</q> et <q>VLV</q> de LDAP en combinaison avec la fonction
paged_results de LDAP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27840">CVE-2020-27840</a>

<p>Douglas Bagnall a découvert un défaut de corruption de tas au moyen de
chaînes DN contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20277">CVE-2021-20277</a>

<p>Douglas Bagnall a découvert une vulnérabilité de lecture hors limites
dans le traitement des attributs de LDAP qui contiennent plusieurs espaces
en début consécutifs.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2:1.5.1+really1.4.6-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ldb.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ldb, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ldb">\
https://security-tracker.debian.org/tracker/ldb</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4884.data"
# $Id: $
