#use wml::debian::translation-check translation="8d9ce2ed95e0dd5428bb16b32cf4a2c1634c4815" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Simon McVittie a signalé un défaut dans ibus, le bus d'entrée
intelligent (« Intelligent Input Bus »). Du fait d'une mauvaise
configuration durant l'installation de DBus, n'importe quel utilisateur non
privilégié pourrait surveiller et envoyer des appels de méthode au bus ibus
d'un autre utilisateur, s'il peut découvrir la socket UNIX utilisée par
l'autre utilisateur connecté dans un environnement graphique. L'attaquant
peut profiter de ce défaut pour intercepter les frappes de l'utilisateur
victime ou pour modifier des configurations relatives aux entrées grâce
à des appels de méthode de DBus.</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 1.5.14-3+deb9u2.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 1.5.19-4+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ibus.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ibus, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ibus">\
https://security-tracker.debian.org/tracker/ibus</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4525.data"
# $Id: $
