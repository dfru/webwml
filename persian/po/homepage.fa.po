msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "سیستم عامل همگانی"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr "عکس گروهی از ۱۹‌امین کنفرانس دبیان"

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr "عکس گروهی از دب‌کنف۱۹"

#: ../../english/index.def:19
msgid "Mini DebConf Hamburg 2018"
msgstr "دب‌کنف کوچیک در هامبورگ، سال ۲۰۱۸"

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Hamburg 2018"
msgstr "عکس گروهی از دب‌کنف کوچک در هامبورگ، سال ۲۰۱۸"

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr "عکس از صفحه نصب کننده کالامارس"

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr "عکس از صفحه نصب کننده کالامارس"

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "دبیان مثل چاقوی سوئیسیه"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr "مردم با دبیان سرکیفن!"

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "بر و بچ دبیان در دب‌کنف۱۸ در هسینچو حسابی برقرارن!"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "بیت‌هایی از دبیان"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "وبلاگ"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "اخبار کوتاه"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "اخبار کوتاه از دبیان"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "سیاره"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "سیاره‌ی دبیان"
